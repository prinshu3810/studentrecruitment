/****** Object:  Table [dbo].[CollegeInfo]    Script Date: 06-05-2021 18:08:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CollegeInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CollegeName] [varchar](max) NOT NULL,
	[ContactPerson] [varchar](150) NOT NULL,
	[ContactPersonPhoneNo] [varchar](10) NOT NULL,
	[CollegeAddress] [varchar](max) NOT NULL,
	[State] [varchar](100) NOT NULL,
 CONSTRAINT [PK__CollegeI__3214EC07D35CB7EE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Course]    Script Date: 06-05-2021 18:08:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Course](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CourseType] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EndUser]    Script Date: 06-05-2021 18:08:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EndUser](
	[PhoneNo] [varchar](10) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[RoleId] [int] NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[Name] [varchar](150) NOT NULL,
	[otp] [varchar](8) NULL,
	[Id] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_EndUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Question]    Script Date: 06-05-2021 18:08:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[QuestionContent] [varchar](max) NOT NULL,
	[Marks] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[SectionId] [int] NOT NULL,
 CONSTRAINT [PK__Question__3214EC0720B79BB4] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuestionOption]    Script Date: 06-05-2021 18:08:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuestionOption](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[QuestionId] [int] NOT NULL,
	[QuestionOption] [varchar](max) NOT NULL,
	[IsActive] [bit] NULL,
	[IsCorrect] [bit] NULL,
 CONSTRAINT [PK__Question__3214EC07381AB637] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 06-05-2021 18:08:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleType] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Section]    Script Date: 06-05-2021 18:08:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Section](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SectionName] [varchar](100) NOT NULL,
	[TestId] [int] NOT NULL,
	[QuestionCount] [int] NOT NULL,
 CONSTRAINT [PK__Section__3214EC07B05AD2FE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student]    Script Date: 06-05-2021 18:08:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student](
	[AlternatePhoneNo] [varchar](10) NULL,
	[RollNo] [varchar](20) NOT NULL,
	[Semester] [int] NOT NULL,
	[CollegeId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Test]    Script Date: 06-05-2021 18:08:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Test](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Duration] [int] NOT NULL,
	[TestName] [varchar](100) NOT NULL,
	[MaxMarks] [int] NOT NULL,
	[CutOff] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserQuestionAnswer]    Script Date: 06-05-2021 18:08:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserQuestionAnswer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserTestId] [int] NOT NULL,
	[QuestionId] [int] NOT NULL,
	[OptionId] [int] NULL,
	[TextResponse] [varchar](max) NULL,
 CONSTRAINT [PK__UserQues__3214EC07A0B4CB10] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserTest]    Script Date: 06-05-2021 18:08:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTest](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[TestId] [int] NOT NULL,
	[Marks] [int] NOT NULL,
	[DateGiven] [date] NOT NULL,
	[Pass] [bit] NULL,
 CONSTRAINT [PK__UserTest__3214EC07E7CF22C1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CollegeInfo] ON 
GO
INSERT [dbo].[CollegeInfo] ([Id], [CollegeName], [ContactPerson], [ContactPersonPhoneNo], [CollegeAddress], [State]) VALUES (1, N'JSS Academy Of Technical Education Noida', N'Yogendra Singh', N'9819852325', N'C-42 sector-62 Noida', N'UP')
GO
INSERT [dbo].[CollegeInfo] ([Id], [CollegeName], [ContactPerson], [ContactPersonPhoneNo], [CollegeAddress], [State]) VALUES (2, N'Jaypee Institute Of Engineering', N'Sachin Dalal', N'9865475966', N'A-142 sector-62 Noida', N'UP')
GO
INSERT [dbo].[CollegeInfo] ([Id], [CollegeName], [ContactPerson], [ContactPersonPhoneNo], [CollegeAddress], [State]) VALUES (3, N'AKG Institute of Engineering', N'Divya Sinha', N'9648569584', N'A-193 sector-123 Noida', N'UP')
GO
INSERT [dbo].[CollegeInfo] ([Id], [CollegeName], [ContactPerson], [ContactPersonPhoneNo], [CollegeAddress], [State]) VALUES (4, N'Amity University', N'Yash Kherwal', N'8800896521', N'B-93 sector-127 Noida', N'UP')
GO
SET IDENTITY_INSERT [dbo].[CollegeInfo] OFF
GO
SET IDENTITY_INSERT [dbo].[Course] ON 
GO
INSERT [dbo].[Course] ([Id], [CourseType]) VALUES (1, N'B.Tech')
GO
INSERT [dbo].[Course] ([Id], [CourseType]) VALUES (2, N'B.E')
GO
INSERT [dbo].[Course] ([Id], [CourseType]) VALUES (3, N'BCA')
GO
INSERT [dbo].[Course] ([Id], [CourseType]) VALUES (4, N'MCA')
GO
SET IDENTITY_INSERT [dbo].[Course] OFF
GO
INSERT [dbo].[EndUser] ([PhoneNo], [Email], [RoleId], [Password], [Name], [otp], [Id]) VALUES (N'8446522152', N'pulkijatav@gmail.com', 1, N'Pulkit@1234', N'pulkit', NULL, N'efc07485-98e4-44bd-b704-02a1661cecf4')
GO
INSERT [dbo].[EndUser] ([PhoneNo], [Email], [RoleId], [Password], [Name], [otp], [Id]) VALUES (N'9319757051', N'satyam8934@gmail.com', 2, N'Satyam@1234', N'satyam', NULL, N'a9fbe64a-efcf-4027-ad17-577c0e4c3e79')
GO
INSERT [dbo].[EndUser] ([PhoneNo], [Email], [RoleId], [Password], [Name], [otp], [Id]) VALUES (N'9365445652', N'dhruvatiwari9@gmail.com', 1, N'Satish@123', N'satish', N'319816', N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0')
GO
INSERT [dbo].[EndUser] ([PhoneNo], [Email], [RoleId], [Password], [Name], [otp], [Id]) VALUES (N'9580061885', N'prashantsh7227@gmail.com', 1, N'Priy@1234', N'Prashant', N'861283', N'64025662-fc68-4cbb-89e0-79caa404e5e8')
GO
INSERT [dbo].[EndUser] ([PhoneNo], [Email], [RoleId], [Password], [Name], [otp], [Id]) VALUES (N'9648995829', N'msdianpriyanshu@gmail.com', 1, N'Priy@1234', N'Priyanshu Tiwari', N'615521', N'a0770a41-20b9-4d84-834f-83590bfeafd1')
GO
INSERT [dbo].[EndUser] ([PhoneNo], [Email], [RoleId], [Password], [Name], [otp], [Id]) VALUES (N'8800861697', N'shekharanant29@gmail.com', 1, N'Priy@1234', N'Priyanshu Tiwari', N'574100', N'f5df147a-d1d8-4410-9083-8c6763af5c90')
GO
INSERT [dbo].[EndUser] ([PhoneNo], [Email], [RoleId], [Password], [Name], [otp], [Id]) VALUES (N'9865656565', N'manseemishra315@gmail.com', 1, N'Mansee@1234', N'Mansee mishra', N'280066', N'f09f26da-0d40-46d4-97da-93172216e89a')
GO
INSERT [dbo].[EndUser] ([PhoneNo], [Email], [RoleId], [Password], [Name], [otp], [Id]) VALUES (N'9818566552', N'priyanshu28798@gmail.com', 1, N'Himanshu@1234', N'Himanshu Tiwari', N'890065', N'6b1fd50d-6960-4e11-9fe2-dcfa35c9709a')
GO
INSERT [dbo].[EndUser] ([PhoneNo], [Email], [RoleId], [Password], [Name], [otp], [Id]) VALUES (N'8548545663', N'priyanshu.tiwari@grapecity.com', 1, N'Priy@1234', N'Priyanshu', N'956283', N'2af85e85-82e5-4769-ba1c-f03cb395e421')
GO
SET IDENTITY_INSERT [dbo].[Question] ON 
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (1, N'Which of the following language was developed as the first purely object programming language?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (2, N'Who developed object-oriented programming?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (3, N'Which of the following is not an OOPS concept?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (4, N'Which feature of OOPS described the reusability of code?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (5, N'Which of the following language supports polymorphism but not the classes?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (6, N'Which among the following feature is not in the general definition of OOPS?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (7, N'Which feature of OOPS derives the class from another class?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (8, N' Define the programming language, which does not support all four types of inheritance?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (9, N' A single program of OOPS contains _______ classes?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (10, N' Which operator from the following can be used to illustrate the feature of polymorphism?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (11, N'Write a program to find the factorial of a natural number less than 1000', 50, 1, 4)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (12, N'Given an array A[] of size n. The task is to find the largest element in it.', 50, 1, 4)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (13, N'Which of the following is not OOPS concept in Java?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (14, N'Which of the following is a type of polymorphism in Java?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (15, N'When does method overloading is determined?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (16, N'When Overloading does not occur?', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (17, N'Look at this series: 2, 1, (1/2), (1/4), ... What number should come next?', 2, 1, 2)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (18, N'Look at this series: 7, 10, 8, 11, 9, 12, ... What number should come next?', 2, 1, 2)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (19, N'Look at this series: 36, 34, 30, 28, 24, ... What number should come next?', 2, 1, 2)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (20, N'Look at this series: 22, 21, 23, 22, 24, 23, ... What number should come next?', 2, 1, 2)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (21, N'Look at this series: 53, 53, 40, 40, 27, 27, ... What number should come next?', 2, 1, 2)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (22, N'Which word does NOT belong with the others?', 2, 1, 2)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (23, N'Odometer is to mileage as compass is to', 2, 1, 2)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (24, N'Marathon is to race as hibernation is to', 2, 1, 2)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (25, N'Window is to pane as book is to', 2, 1, 2)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (26, N'Cup is to coffee as bowl is to', 2, 1, 2)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (27, N'I ..... tennis every Sunday morning.', 2, 1, 3)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (28, N'Don''t make so much noise. Noriko ... to study for her ESL test!', 2, 1, 3)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (29, N'Jun-Sik ..... his teeth before breakfast every morning.', 2, 1, 3)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (30, N'Sorry, she can''t come to the phone. She ..... a bath!', 2, 1, 3)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (31, N'..... many times every winter in Frankfurt.', 2, 1, 3)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (32, N'How many students in your class ..... from Korea?', 2, 1, 3)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (33, N'Weather report: "It''s seven o''clock in Frankfurt and ..... ."', 2, 1, 3)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (34, N'Babies ..... when they are hungry.', 2, 1, 3)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (35, N'	Jane: "What ..... in the evenings?"
Mary: "Usually I watch TV or read a book."', 2, 1, 3)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (36, N'	Jane: "What ..... ?"
Mary: "I''m trying to fix my calculator."', 2, 1, 3)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (37, N'new question', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (38, N'question 2', 2, 1, 1)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (39, N'new logical question 1', 2, 1, 2)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (40, N'another logical question', 2, 1, 2)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (41, N'new language question', 2, 1, 3)
GO
INSERT [dbo].[Question] ([Id], [QuestionContent], [Marks], [IsActive], [SectionId]) VALUES (42, N'what is your name?', 2, 1, 2)
GO
SET IDENTITY_INSERT [dbo].[Question] OFF
GO
SET IDENTITY_INSERT [dbo].[QuestionOption] ON 
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1, 1, N'SmallTalk', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (2, 1, N'C++', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (3, 1, N'Kotlin', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (4, 1, N'Java', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (5, 2, N'Adele Goldberg', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (6, 2, N'Dennis Ritchie', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (7, 2, N'Alan Kay', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (8, 2, N'Andrea Ferro', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (9, 3, N'Encapsulation', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (10, 3, N'Polymorphism', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (11, 3, N'Exception', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (12, 3, N'Abstraction', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (13, 4, N'Abstraction', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (14, 4, N'Encapsulation', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (15, 4, N'Polymorphism', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (16, 4, N'Inheritance', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (17, 5, N'C++ programming language', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (18, 5, N'Java programming language', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (19, 5, N'Ada programming language', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (20, 5, N'C# programming language', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (21, 6, N'Modularity', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (22, 6, N'Efficient Code', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (23, 6, N'Code reusability', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (24, 6, N'Duplicate or Redundant Data', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (25, 7, N'Inheritance', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (26, 7, N'Data hiding', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (27, 7, N'Encapsulation', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (28, 7, N'Polymorphism', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (29, 8, N'Smalltalk', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (30, 8, N'Kotlin', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (31, 8, N'Java', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (32, 8, N'C++', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (33, 9, N'Only 1', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (34, 9, N'Only 999', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (35, 9, N'Only 100', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (36, 9, N'Any number', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (37, 10, N'Overloading <<', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (38, 10, N'Overloading &&', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (39, 10, N'Overloading | |', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (40, 10, N'Overloading +=', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (41, 13, N' Inheritance', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (42, 13, N' Encapsulation', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (43, 13, N' Polymorphism', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (44, 13, N' Compilation', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (45, 14, N' Compile time polymorphism', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (46, 14, N'Execution time polymorphism', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (47, 14, N'Multiple polymorphism', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (48, 14, N'Multilevel polymorphism', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (49, 15, N'At run time', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (50, 15, N'At compile time', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (51, 15, N'At coding time', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (52, 15, N'At execution time', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (53, 16, N'More than one method with same name but different method signature and different number or type of parameters', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (54, 16, N'More than one method with same name, same signature but different number of signature', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (55, 16, N'More than one method with same name, same signature, same number of parameters but different type', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (56, 16, N'More than one method with same name, same number of parameters and type but different signature
', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1041, 17, N'(1/3)', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1042, 17, N'(1/8)', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1043, 17, N'(2/8)', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1044, 17, N'(1/16)', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1045, 18, N'7', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1046, 18, N'10', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1047, 18, N'12', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1048, 18, N'13', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1049, 19, N'20', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1050, 19, N'22', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1051, 19, N'23', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1052, 19, N'26', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1056, 20, N'22', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1057, 20, N'24', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1058, 20, N'25', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1059, 20, N'26', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1060, 21, N'12', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1061, 21, N'14', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1062, 21, N'27', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1063, 21, N'53', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1065, 22, N'parsley', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1066, 22, N'parsley', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1067, 22, N'dill', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1068, 22, N'mayonnaise', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1069, 23, N'speed', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1070, 23, N'hiking', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1071, 23, N'needle', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1072, 23, N'direction', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1073, 24, N'winter', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1074, 24, N'bear', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1075, 24, N'dream', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1076, 24, N'sleep', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1077, 25, N'novel', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1078, 25, N'glass', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1079, 25, N'cover', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1080, 25, N'page', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1082, 26, N'dish', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1083, 26, N'soup', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1084, 26, N'spoon', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1085, 26, N'food', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1086, 27, N'play', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1087, 27, N'am playing', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1088, 27, N'am play', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1089, 28, N'try', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1090, 28, N'tries', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1091, 28, N'tried', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1092, 28, N'is trying', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1093, 29, N'will cleaned', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1094, 29, N'	is cleaning', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1095, 29, N'cleans', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1096, 29, N'clean', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1097, 30, N'is having', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1098, 30, N'having', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1099, 30, N'have', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1100, 30, N'has', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1101, 31, N'It snows', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1102, 31, N'	It snowed', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1103, 31, N'It is snowing', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1104, 31, N'It is snow', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1105, 32, N'comes', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1106, 32, N'come', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1107, 32, N'came', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1108, 32, N'are coming', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1109, 33, N'there is snow', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1110, 33, N'it`s snowing', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1111, 33, N'it snows', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1112, 33, N'it snowed', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1113, 34, N'cry', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1114, 34, N'cries', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1115, 34, N'cried', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1116, 34, N'are crying', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1117, 35, N'you doing', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1118, 35, N'you do', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1119, 35, N'do you do', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1120, 35, N'are you doing', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1121, 36, N'you doing', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1122, 36, N'you do', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1123, 36, N'do you do', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1124, 36, N'are you doing', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1125, 27, N'played', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1126, 37, N'option1', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1127, 37, N'option2', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1128, 37, N'option3', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1129, 37, N'option4', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1130, 38, N'option 1', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1131, 38, N'option 2', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1132, 38, N'option 3', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1133, 38, N'option 4', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1134, 39, N'option 1', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1135, 39, N'opotion 2', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1136, 39, N'option 3', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1137, 39, N'option 4', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1138, 40, N'option 1', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1139, 40, N'option 2', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1140, 40, N'option 3', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1141, 40, N'option 4', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1142, 41, N'option 1', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1143, 41, N'option 2', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1144, 41, N'option 3', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1145, 41, N'option 4', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1146, 42, N'priyanshu', 1, 1)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1147, 42, N'sachin', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1148, 42, N'ramesh', 1, 0)
GO
INSERT [dbo].[QuestionOption] ([Id], [QuestionId], [QuestionOption], [IsActive], [IsCorrect]) VALUES (1149, 42, N'suresh', 1, 0)
GO
SET IDENTITY_INSERT [dbo].[QuestionOption] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 
GO
INSERT [dbo].[Role] ([Id], [RoleType]) VALUES (1, N'Student')
GO
INSERT [dbo].[Role] ([Id], [RoleType]) VALUES (2, N'Admin')
GO
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Section] ON 
GO
INSERT [dbo].[Section] ([Id], [SectionName], [TestId], [QuestionCount]) VALUES (1, N'OOPS', 1, 10)
GO
INSERT [dbo].[Section] ([Id], [SectionName], [TestId], [QuestionCount]) VALUES (2, N'Logical', 1, 10)
GO
INSERT [dbo].[Section] ([Id], [SectionName], [TestId], [QuestionCount]) VALUES (3, N'Language', 1, 10)
GO
INSERT [dbo].[Section] ([Id], [SectionName], [TestId], [QuestionCount]) VALUES (4, N'Programming', 2, 2)
GO
SET IDENTITY_INSERT [dbo].[Section] OFF
GO
INSERT [dbo].[Student] ([AlternatePhoneNo], [RollNo], [Semester], [CollegeId], [CourseId], [Id]) VALUES (N'9648995829', N'1709110115', 8, 1, 1, N'efc07485-98e4-44bd-b704-02a1661cecf4')
GO
INSERT [dbo].[Student] ([AlternatePhoneNo], [RollNo], [Semester], [CollegeId], [CourseId], [Id]) VALUES (N'', N'244652854', 8, 3, 2, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0')
GO
INSERT [dbo].[Student] ([AlternatePhoneNo], [RollNo], [Semester], [CollegeId], [CourseId], [Id]) VALUES (N'', N'201010025', 8, 3, 3, N'64025662-fc68-4cbb-89e0-79caa404e5e8')
GO
INSERT [dbo].[Student] ([AlternatePhoneNo], [RollNo], [Semester], [CollegeId], [CourseId], [Id]) VALUES (N'8855855858', N'1709446558', 7, 1, 1, N'a0770a41-20b9-4d84-834f-83590bfeafd1')
GO
INSERT [dbo].[Student] ([AlternatePhoneNo], [RollNo], [Semester], [CollegeId], [CourseId], [Id]) VALUES (N'9648995829', N'1709110114', 8, 1, 1, N'f5df147a-d1d8-4410-9083-8c6763af5c90')
GO
INSERT [dbo].[Student] ([AlternatePhoneNo], [RollNo], [Semester], [CollegeId], [CourseId], [Id]) VALUES (N'', N'1809110114', 8, 4, 4, N'f09f26da-0d40-46d4-97da-93172216e89a')
GO
INSERT [dbo].[Student] ([AlternatePhoneNo], [RollNo], [Semester], [CollegeId], [CourseId], [Id]) VALUES (N'', N'1706986569', 8, 3, 2, N'6b1fd50d-6960-4e11-9fe2-dcfa35c9709a')
GO
INSERT [dbo].[Student] ([AlternatePhoneNo], [RollNo], [Semester], [CollegeId], [CourseId], [Id]) VALUES (N'', N'1709110116', 8, 3, 3, N'2af85e85-82e5-4769-ba1c-f03cb395e421')
GO
SET IDENTITY_INSERT [dbo].[Test] ON 
GO
INSERT [dbo].[Test] ([Id], [Duration], [TestName], [MaxMarks], [CutOff]) VALUES (1, 30, N'Aptitude Test', 40, 10)
GO
INSERT [dbo].[Test] ([Id], [Duration], [TestName], [MaxMarks], [CutOff]) VALUES (2, 45, N'Coding Test', 100, 60)
GO
SET IDENTITY_INSERT [dbo].[Test] OFF
GO
SET IDENTITY_INSERT [dbo].[UserQuestionAnswer] ON 
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1, 1, 1, 1, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (2, 1, 2, 7, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3, 1, 3, 12, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4, 1, 4, 16, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5, 1, 5, 20, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6, 1, 6, 23, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (7, 1, 7, 28, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8, 1, 8, 29, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (9, 1, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (10, 1, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (11, 2, 11, NULL, N'')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (12, 2, 12, NULL, N' 2  ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1023, 1006, 1, 1, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1024, 1006, 2, 7, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1025, 1006, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1026, 1006, 4, 16, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1027, 1006, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1028, 1006, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1029, 1006, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1030, 1006, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1031, 1006, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1032, 1006, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1033, 1007, 11, NULL, N'1 ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1034, 1007, 12, NULL, N' 2  ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1035, 1008, 1, 1, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1036, 1008, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1037, 1008, 3, 12, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1038, 1008, 4, 16, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1039, 1008, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1040, 1008, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1041, 1008, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1042, 1008, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1043, 1008, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1044, 1008, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1045, 1009, 11, NULL, N'satish code1 ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (1046, 1009, 12, NULL, N' satish code 2')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (2001, 2001, 1, 1, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (2002, 2001, 2, 7, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (2003, 2001, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (2004, 2001, 4, 16, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (2005, 2001, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (2006, 2001, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (2007, 2001, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (2008, 2001, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (2009, 2001, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (2010, 2001, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (2011, 2002, 11, NULL, N'Prashant code 1')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (2012, 2002, 12, NULL, N' Prashant 2')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3001, 3001, 1, 1, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3002, 3001, 2, 7, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3003, 3001, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3004, 3001, 4, 16, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3005, 3001, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3006, 3001, 6, 23, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3007, 3001, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3008, 3001, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3009, 3001, 9, 35, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3010, 3001, 10, 39, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3011, 3002, 11, NULL, N'#include <iostream>
using namespace std;

int main()
{
    int firstNumber, secondNumber, sumOfTwoNumbers;
    
    cout << "Enter two integers: ";
    cin >> firstNumber >> secondNumber;

    // sum of two numbers in stored in variable sumOfTwoNumbers
    sumOfTwoNumbers = firstNumber + secondNumber;

    // Prints sum 
    cout << firstNumber << " + " <<  secondNumber << " = " << sumOfTwoNumbers;     

    return 0;
}')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3012, 3003, 1, 1, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3013, 3003, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3014, 3003, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3015, 3003, 4, 14, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3016, 3003, 5, 17, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3017, 3003, 6, 22, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3018, 3003, 7, 26, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3019, 3003, 8, 30, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3020, 3003, 9, 34, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (3021, 3003, 10, 38, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4012, 4003, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4013, 4003, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4014, 4003, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4015, 4003, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4016, 4003, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4017, 4003, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4018, 4003, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4019, 4003, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4020, 4003, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4021, 4003, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4022, 4004, 11, NULL, N'#include <iostream>

int main() {
    std::cout << "Hello World!";
    return 0;
}    ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4023, 4004, 12, NULL, N'     #include <iostream>

int main() {
    std::cout << "Hello World!";
    return 0;
}')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4024, 4005, 16, 53, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4025, 4005, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4026, 4005, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4027, 4005, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4028, 4005, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4029, 4005, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4030, 4005, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4031, 4005, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4032, 4005, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4033, 4005, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4034, 4007, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4035, 4007, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4036, 4007, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4037, 4007, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4038, 4007, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4039, 4007, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4040, 4007, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4041, 4007, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4042, 4007, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4043, 4007, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4044, 4008, 11, NULL, N'#include<bits/stdc++.h>
using namespace std;
int main{
    cout<<"Written for Question 1"<<endl;
}  ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (4045, 4008, 12, NULL, N' #include<bits/stdc++.h>
using namespace std;
int main{
    cout<<"Written for Question 1"<<endl;
} ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5012, 5003, 9, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5013, 5003, 4, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5014, 5003, 2, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5015, 5003, 14, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5016, 5003, 10, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5017, 5003, 5, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5018, 5003, 3, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5019, 5003, 15, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5020, 5003, 8, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5021, 5003, 7, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5022, 5004, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5023, 5004, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5024, 5004, 16, 53, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5025, 5004, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5026, 5004, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5027, 5004, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5028, 5004, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5029, 5004, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5030, 5004, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5031, 5004, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5032, 5005, 11, NULL, N'1')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (5033, 5005, 12, NULL, N' 2')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6012, 6003, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6013, 6003, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6014, 6003, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6015, 6003, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6016, 6003, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6017, 6003, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6018, 6003, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6019, 6003, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6020, 6003, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6021, 6003, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6022, 6003, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6023, 6003, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6024, 6003, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6025, 6003, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6026, 6003, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6027, 6003, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6028, 6003, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6029, 6003, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6030, 6003, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6031, 6003, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6032, 6005, 11, NULL, N' new Code')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (6033, 6005, 12, NULL, N'  new Code')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (7033, 6006, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (7034, 6006, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (7035, 6006, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (7036, 6006, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (7037, 6006, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (7038, 6006, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (7039, 6006, 16, 56, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (7040, 6006, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (7041, 6006, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (7042, 6006, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8033, 8006, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8034, 8006, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8035, 8006, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8036, 8006, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8037, 8006, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8038, 8006, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8039, 8006, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8040, 8006, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8041, 8006, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8042, 8006, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8043, 8006, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8044, 8006, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8045, 8006, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8046, 8006, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8047, 8006, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8048, 8006, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8049, 8006, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8050, 8006, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8051, 8006, 16, 55, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8052, 8006, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8053, 8008, 11, NULL, N'#include<bits/stdc++.h>
using namespace std;
int main()
{
     
} ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (8054, 8008, 12, NULL, N' ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (9033, 9006, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (9034, 9006, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (9035, 9006, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (9036, 9006, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (9037, 9006, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (9038, 9006, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (9039, 9006, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (9040, 9006, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (9041, 9006, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (9042, 9006, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (9043, 9007, 11, NULL, N'nice coding platform ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (9044, 9007, 12, NULL, N' even nicer coding platform')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (13033, 14006, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (13034, 14006, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (13035, 14006, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (13036, 14006, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (13037, 14006, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (13038, 14006, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (13039, 14006, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (13040, 14006, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (13041, 14006, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (13042, 14006, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (13043, 14007, 11, NULL, N'#include <iostream>
using namespace std;

int main()
{
    int firstNumber, secondNumber, sumOfTwoNumbers;
    
    cout << "Enter two integers: ";
    cin >> firstNumber >> secondNumber;

    // sum of two numbers in stored in variable sumOfTwoNumbers
    sumOfTwoNumbers = firstNumber + secondNumber;

    // Prints sum 
    cout << firstNumber << " + " <<  secondNumber << " = " << sumOfTwoNumbers;     

    return 0;
}  ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (13044, 14007, 12, NULL, N' #include <iostream>
using namespace std;

int main()
{
    int firstNumber, secondNumber, sumOfTwoNumbers;
    
    cout << "Enter two integers: ";
    cin >> firstNumber >> secondNumber;

    // sum of two numbers in stored in variable sumOfTwoNumbers
    sumOfTwoNumbers = firstNumber + secondNumber;

    // Prints sum 
    cout << firstNumber << " + " <<  secondNumber << " = " << sumOfTwoNumbers;     

    return 0;
} ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14033, 15006, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14034, 15006, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14035, 15006, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14036, 15006, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14037, 15006, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14038, 15006, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14039, 15006, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14040, 15006, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14041, 15006, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14042, 15006, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14043, 15007, 11, NULL, N'  klrgekkergre ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14044, 15007, 12, NULL, N'   ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14045, 17008, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14046, 17008, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14047, 17008, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14048, 17008, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14049, 17008, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14050, 17008, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14051, 17008, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14052, 17008, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14053, 17008, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14054, 17008, 16, 55, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14055, 17009, 11, NULL, N'  ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14056, 17009, 12, NULL, N'  ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14091, 17017, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14092, 17017, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14093, 17017, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14094, 17017, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14095, 17017, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14096, 17017, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14097, 17017, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14098, 17017, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14099, 17017, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14100, 17017, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14101, 17018, 11, NULL, N'')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14102, 17019, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14103, 17019, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14104, 17019, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14105, 17019, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14106, 17019, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14107, 17019, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14108, 17019, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14109, 17019, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14110, 17019, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14111, 17019, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14112, 17020, 11, NULL, N'    ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14113, 17020, 12, NULL, N'     ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14114, 17021, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14115, 17021, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14116, 17021, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14117, 17021, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14118, 17021, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14119, 17021, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14120, 17021, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14121, 17021, 10, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14122, 17021, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14123, 17021, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14124, 17022, 11, NULL, N'  ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14125, 17022, 12, NULL, N'   ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14126, 17023, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14127, 17023, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14128, 17023, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14129, 17023, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14130, 17023, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14131, 17023, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14132, 17023, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14133, 17023, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14134, 17023, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14135, 17023, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14136, 17024, 11, NULL, N' ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14137, 17024, 12, NULL, N' ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14138, 17025, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14139, 17025, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14140, 17025, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14141, 17025, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14142, 17025, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14143, 17025, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14144, 17025, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14145, 17025, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14146, 17025, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14147, 17025, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (14148, 17026, 11, NULL, N'')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (15102, 19019, 13, 41, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (15103, 19019, 16, 53, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (15104, 19019, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (15105, 19019, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (15106, 19019, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (15107, 19019, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (15108, 19019, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (15109, 19019, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (15110, 19019, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (15111, 19019, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (15112, 19020, 11, NULL, N'   ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (15113, 19020, 12, NULL, N'   ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (16102, 20019, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (16103, 20019, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (16104, 20019, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (16105, 20019, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (16106, 20019, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (16107, 20019, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (16108, 20019, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (16109, 20019, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (16110, 20019, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (16111, 20019, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (16112, 20020, 11, NULL, N' ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (16113, 20020, 12, NULL, N' ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18102, 24019, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18103, 24019, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18104, 24019, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18105, 24019, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18106, 24019, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18107, 24019, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18108, 24019, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18109, 24019, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18110, 24019, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18111, 24019, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18112, 24020, 11, NULL, N'')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18113, 24022, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18114, 24022, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18115, 24022, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18116, 24022, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18117, 24022, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18118, 24022, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18119, 24022, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18120, 24022, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18121, 24022, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18122, 24022, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18123, 24023, 11, NULL, N' ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18124, 24023, 12, NULL, N'  ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18125, 24024, 2, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18126, 24024, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18127, 24024, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18128, 24024, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18129, 24024, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18130, 24024, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18131, 24024, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18132, 24024, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18133, 24024, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18134, 24024, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18135, 24025, 11, NULL, N'#include')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18136, 24029, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18137, 24029, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18138, 24029, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18139, 24029, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18140, 24029, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18141, 24029, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18142, 24029, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18143, 24029, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18144, 24029, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18145, 24029, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18146, 24029, 17, 1042, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18147, 24029, 18, 1046, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18148, 24029, 19, 1050, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18149, 24029, 20, 1058, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18150, 24029, 21, 1061, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18151, 24029, 22, 1067, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18152, 24029, 23, 1072, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18153, 24029, 24, 1076, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18154, 24029, 25, 1079, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18155, 24029, 26, 1083, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18156, 24029, 27, 1086, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18157, 24029, 28, 1092, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18158, 24029, 29, 1093, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18159, 24029, 30, 1098, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18160, 24029, 31, 1102, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18161, 24029, 32, 1106, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18162, 24029, 33, 1111, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18163, 24029, 34, 1114, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18164, 24029, 35, 1119, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18165, 24029, 36, 1122, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18166, 24030, 11, NULL, N'   ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18167, 24030, 12, NULL, N'  fhfjjfh ')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18168, 24031, 6, 21, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18169, 24031, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18170, 24031, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18171, 24031, 16, 53, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18172, 24031, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18173, 24031, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18174, 24031, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18175, 24031, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18176, 24031, 2, 6, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18177, 24031, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18178, 24031, 17, 1042, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18179, 24031, 18, 1046, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18180, 24031, 19, 1049, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18181, 24031, 20, 1058, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18182, 24031, 21, 1061, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18183, 24031, 22, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18184, 24031, 23, 1072, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18185, 24031, 24, 1076, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18186, 24031, 25, 1079, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18187, 24031, 26, 1083, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18188, 24031, 27, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18189, 24031, 28, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18190, 24031, 29, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18191, 24031, 30, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18192, 24031, 31, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18193, 24031, 32, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18194, 24031, 33, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18195, 24031, 34, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18196, 24031, 35, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18197, 24031, 36, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18198, 24032, 11, NULL, N'jhhhhhhkjjjjjjjjjjyhjhhg')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18199, 24033, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18200, 24033, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18201, 24033, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18202, 24033, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18203, 24033, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18204, 24033, 2, 7, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18205, 24033, 6, 24, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18206, 24033, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18207, 24033, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18208, 24033, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18209, 24033, 17, 1042, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18210, 24033, 18, 1046, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18211, 24033, 19, 1050, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18212, 24033, 20, 1058, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18213, 24033, 21, 1061, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18214, 24033, 22, 1066, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18215, 24033, 23, 1072, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18216, 24033, 24, 1076, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18217, 24033, 25, 1079, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18218, 24033, 26, 1083, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18219, 24033, 27, 1086, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18220, 24033, 28, 1092, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18221, 24033, 29, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18222, 24033, 30, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18223, 24033, 31, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18224, 24033, 32, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18225, 24033, 33, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18226, 24033, 34, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18227, 24033, 35, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18228, 24033, 36, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18229, 24034, 11, NULL, N'ggjfjfg')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18230, 24035, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18231, 24035, 38, 1131, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18232, 24035, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18233, 24035, 8, 31, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18234, 24035, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18235, 24035, 5, 19, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18236, 24035, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18237, 24035, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18238, 24035, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18239, 24035, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18240, 24035, 17, 1042, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18241, 24035, 18, 1046, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18242, 24035, 19, 1050, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18243, 24035, 20, 1058, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18244, 24035, 21, 1061, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18245, 24035, 22, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18246, 24035, 23, 1072, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18247, 24035, 24, 1076, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18248, 24035, 25, 1079, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18249, 24035, 26, 1083, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18250, 24035, 39, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18251, 24035, 40, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18252, 24035, 42, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18253, 24035, 27, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18254, 24035, 28, 1091, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18255, 24035, 29, 1094, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18256, 24035, 30, 1097, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18257, 24035, 31, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18258, 24035, 32, 1106, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18259, 24035, 33, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18260, 24035, 34, 1113, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18261, 24035, 35, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18262, 24035, 36, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18263, 24035, 41, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18264, 24036, 11, NULL, N' #incl')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18265, 24036, 12, NULL, N'  codingkwklk')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18266, 24038, 15, 50, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18267, 24038, 38, 1131, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18268, 24038, 13, 44, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18269, 24038, 14, 45, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18270, 24038, 9, 36, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18271, 24038, 10, 37, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18272, 24038, 3, 11, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18273, 24038, 7, 25, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18274, 24038, 16, 54, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18275, 24038, 4, 15, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18276, 24038, 39, 1135, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18277, 24038, 21, 1061, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18278, 24038, 25, 1079, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18279, 24038, 24, 1076, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18280, 24038, 18, 1046, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18281, 24038, 40, 1140, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18282, 24038, 20, 1058, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18283, 24038, 42, 1146, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18284, 24038, 19, 1050, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18285, 24038, 22, 1066, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18286, 24038, 31, 1103, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18287, 24038, 36, 1121, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18288, 24038, 29, 1094, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18289, 24038, 41, 1144, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18290, 24038, 34, 1113, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18291, 24038, 33, 1110, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18292, 24038, 35, 1119, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18293, 24038, 30, 1097, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18294, 24038, 28, 1090, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18295, 24038, 32, NULL, NULL)
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18296, 24039, 11, NULL, N' include <iostream>
using namespace std;

int main()
{
    int firstNumber, secondNumber, sumOfTwoNumbers;
    
    cout << "Enter two integers: ";
    cin >> firstNumber >> secondNumber;

    // sum of two numbers in stored in variable sumOfTwoNumbers
    sumOfTwoNumbers = firstNumber + secondNumber;

    // Prints sum 
    cout << firstNumber << " + " <<  secondNumber << " = " << sumOfTwoNumbers;     

    return 0;
}')
GO
INSERT [dbo].[UserQuestionAnswer] ([Id], [UserTestId], [QuestionId], [OptionId], [TextResponse]) VALUES (18297, 24039, 12, NULL, N'  include <iostream>
using namespace std;

int main()
{
    int firstNumber, secondNumber, sumOfTwoNumbers;
    
    cout << "Enter two integers: ";
    cin >> firstNumber >> secondNumber;

    // sum of two numbers in stored in variable sumOfTwoNumbers
    sumOfTwoNumbers = firstNumber + secondNumber;

    // Prints sum 
    cout << firstNumber << " + " <<  secondNumber << " = " << sumOfTwoNumbers;     

    return 0;
}')
GO
SET IDENTITY_INSERT [dbo].[UserQuestionAnswer] OFF
GO
SET IDENTITY_INSERT [dbo].[UserTest] ON 
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (1, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 1, 10, CAST(N'2021-04-15' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (2, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 2, 22, CAST(N'2021-04-15' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (1006, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 1, 20, CAST(N'2021-04-16' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (1007, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 2, 20, CAST(N'2021-04-16' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (1008, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 1, 16, CAST(N'2021-04-16' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (1009, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 2, 25, CAST(N'2021-04-16' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (2001, N'64025662-fc68-4cbb-89e0-79caa404e5e8', 1, 20, CAST(N'2021-04-17' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (2002, N'64025662-fc68-4cbb-89e0-79caa404e5e8', 2, 35, CAST(N'2021-04-17' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (3001, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 1, 14, CAST(N'2021-04-18' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (3002, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 2, 40, CAST(N'2021-04-18' AS Date), 0)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (3003, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 1, 4, CAST(N'2021-04-18' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (4003, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 1, 14, CAST(N'2021-04-18' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (4004, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 2, 80, CAST(N'2021-04-18' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (4005, N'f09f26da-0d40-46d4-97da-93172216e89a', 1, 14, CAST(N'2021-04-19' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (4006, N'f09f26da-0d40-46d4-97da-93172216e89a', 2, 0, CAST(N'2021-04-19' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (4007, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 1, 16, CAST(N'2021-04-19' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (4008, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 2, 46, CAST(N'2021-04-19' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (5003, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 1, 0, CAST(N'2021-04-19' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (5004, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 1, 14, CAST(N'2021-04-19' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (5005, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 2, 0, CAST(N'2021-04-19' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (6003, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 1, 18, CAST(N'2021-04-20' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (6004, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 1, 18, CAST(N'2021-04-20' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (6005, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 2, 0, CAST(N'2021-04-20' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (6006, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 1, 20, CAST(N'2021-04-20' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (7006, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 2, 0, CAST(N'2021-04-20' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (8006, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 1, 16, CAST(N'2021-04-20' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (8007, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 1, 14, CAST(N'2021-04-20' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (8008, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 2, 0, CAST(N'2021-04-20' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (9006, N'f09f26da-0d40-46d4-97da-93172216e89a', 1, 16, CAST(N'2021-04-20' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (9007, N'f09f26da-0d40-46d4-97da-93172216e89a', 2, 0, CAST(N'2021-04-20' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (14006, N'2af85e85-82e5-4769-ba1c-f03cb395e421', 1, 16, CAST(N'2021-04-20' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (14007, N'2af85e85-82e5-4769-ba1c-f03cb395e421', 2, 25, CAST(N'2021-04-20' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (15006, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 1, 14, CAST(N'2021-04-23' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (15007, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 2, 0, CAST(N'2021-04-23' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (15008, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 1, 0, CAST(N'2021-04-27' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (15009, N'6b1fd50d-6960-4e11-9fe2-dcfa35c9709a', 1, 0, CAST(N'2021-04-27' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (16008, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 1, 0, CAST(N'2021-04-27' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (17008, N'f09f26da-0d40-46d4-97da-93172216e89a', 1, 14, CAST(N'2021-04-27' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (17009, N'f09f26da-0d40-46d4-97da-93172216e89a', 2, 0, CAST(N'2021-04-27' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (17016, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 1, 0, CAST(N'2021-04-27' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (17017, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 1, 18, CAST(N'2021-04-28' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (17018, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 2, 0, CAST(N'2021-04-28' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (17019, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 1, 16, CAST(N'2021-04-28' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (17020, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 2, 0, CAST(N'2021-04-28' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (17021, N'6b1fd50d-6960-4e11-9fe2-dcfa35c9709a', 1, 12, CAST(N'2021-04-28' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (17022, N'6b1fd50d-6960-4e11-9fe2-dcfa35c9709a', 2, 0, CAST(N'2021-04-28' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (17023, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 1, 16, CAST(N'2021-04-28' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (17024, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 2, 0, CAST(N'2021-04-28' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (17025, N'f09f26da-0d40-46d4-97da-93172216e89a', 1, 14, CAST(N'2021-04-28' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (17026, N'f09f26da-0d40-46d4-97da-93172216e89a', 2, 0, CAST(N'2021-04-28' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (18019, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 1, 0, CAST(N'2021-04-29' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (18020, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 1, 0, CAST(N'2021-04-29' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (19019, N'6b1fd50d-6960-4e11-9fe2-dcfa35c9709a', 1, 12, CAST(N'2021-04-29' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (19020, N'6b1fd50d-6960-4e11-9fe2-dcfa35c9709a', 2, 0, CAST(N'2021-04-29' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (20019, N'f09f26da-0d40-46d4-97da-93172216e89a', 1, 16, CAST(N'2021-04-29' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (20020, N'f09f26da-0d40-46d4-97da-93172216e89a', 2, 0, CAST(N'2021-04-29' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (23019, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 1, 0, CAST(N'2021-04-29' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24019, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 1, 20, CAST(N'2021-04-30' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24020, N'f5df147a-d1d8-4410-9083-8c6763af5c90', 2, 0, CAST(N'2021-04-30' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24021, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 1, 0, CAST(N'2021-04-30' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24022, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 1, 16, CAST(N'2021-04-30' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24023, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 2, 0, CAST(N'2021-04-30' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24024, N'6b1fd50d-6960-4e11-9fe2-dcfa35c9709a', 1, 16, CAST(N'2021-04-30' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24025, N'6b1fd50d-6960-4e11-9fe2-dcfa35c9709a', 2, 0, CAST(N'2021-04-30' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24026, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 1, 0, CAST(N'2021-05-03' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24027, N'b3fc450a-00d8-4efd-8276-6e2f5f0532c0', 1, 0, CAST(N'2021-05-03' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24028, N'6b1fd50d-6960-4e11-9fe2-dcfa35c9709a', 1, 0, CAST(N'2021-05-03' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24029, N'f09f26da-0d40-46d4-97da-93172216e89a', 1, 40, CAST(N'2021-05-03' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24030, N'f09f26da-0d40-46d4-97da-93172216e89a', 2, 70, CAST(N'2021-05-03' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24031, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 1, 24, CAST(N'2021-05-04' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24032, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 2, 20, CAST(N'2021-05-04' AS Date), 0)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24033, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 1, 34, CAST(N'2021-05-05' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24034, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 2, 20, CAST(N'2021-05-05' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24035, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 1, 34, CAST(N'2021-05-06' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24036, N'a0770a41-20b9-4d84-834f-83590bfeafd1', 2, 45, CAST(N'2021-05-06' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24037, N'f09f26da-0d40-46d4-97da-93172216e89a', 1, 0, CAST(N'2021-05-06' AS Date), NULL)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24038, N'2af85e85-82e5-4769-ba1c-f03cb395e421', 1, 32, CAST(N'2021-05-06' AS Date), 1)
GO
INSERT [dbo].[UserTest] ([Id], [UserId], [TestId], [Marks], [DateGiven], [Pass]) VALUES (24039, N'2af85e85-82e5-4769-ba1c-f03cb395e421', 2, 70, CAST(N'2021-05-06' AS Date), 1)
GO
SET IDENTITY_INSERT [dbo].[UserTest] OFF
GO
ALTER TABLE [dbo].[EndUser]  WITH CHECK ADD  CONSTRAINT [FK__EndUser__RoleId__276EDEB3] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[EndUser] CHECK CONSTRAINT [FK__EndUser__RoleId__276EDEB3]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Question] FOREIGN KEY([Id])
REFERENCES [dbo].[Question] ([Id])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Question]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Question1] FOREIGN KEY([Id])
REFERENCES [dbo].[Question] ([Id])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Question1]
GO
ALTER TABLE [dbo].[QuestionOption]  WITH CHECK ADD  CONSTRAINT [FK__QuestionO__Quest__3E52440B] FOREIGN KEY([QuestionId])
REFERENCES [dbo].[Question] ([Id])
GO
ALTER TABLE [dbo].[QuestionOption] CHECK CONSTRAINT [FK__QuestionO__Quest__3E52440B]
GO
ALTER TABLE [dbo].[Section]  WITH CHECK ADD  CONSTRAINT [FK__Section__TestId__160F4887] FOREIGN KEY([TestId])
REFERENCES [dbo].[Test] ([Id])
GO
ALTER TABLE [dbo].[Section] CHECK CONSTRAINT [FK__Section__TestId__160F4887]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [FK__Student__College__32E0915F] FOREIGN KEY([CollegeId])
REFERENCES [dbo].[CollegeInfo] ([Id])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [FK__Student__College__32E0915F]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [FK__Student__CourseI__300424B4] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Course] ([Id])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [FK__Student__CourseI__300424B4]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [FK_EnddUser] FOREIGN KEY([Id])
REFERENCES [dbo].[EndUser] ([Id])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [FK_EnddUser]
GO
ALTER TABLE [dbo].[UserQuestionAnswer]  WITH CHECK ADD  CONSTRAINT [FK__UserQuest__Optio__4316F928] FOREIGN KEY([OptionId])
REFERENCES [dbo].[QuestionOption] ([Id])
GO
ALTER TABLE [dbo].[UserQuestionAnswer] CHECK CONSTRAINT [FK__UserQuest__Optio__4316F928]
GO
ALTER TABLE [dbo].[UserQuestionAnswer]  WITH CHECK ADD  CONSTRAINT [FK__UserQuest__Quest__4222D4EF] FOREIGN KEY([QuestionId])
REFERENCES [dbo].[Question] ([Id])
GO
ALTER TABLE [dbo].[UserQuestionAnswer] CHECK CONSTRAINT [FK__UserQuest__Quest__4222D4EF]
GO
ALTER TABLE [dbo].[UserQuestionAnswer]  WITH CHECK ADD  CONSTRAINT [FK__UserQuest__UserT__412EB0B6] FOREIGN KEY([UserTestId])
REFERENCES [dbo].[UserTest] ([Id])
GO
ALTER TABLE [dbo].[UserQuestionAnswer] CHECK CONSTRAINT [FK__UserQuest__UserT__412EB0B6]
GO
ALTER TABLE [dbo].[UserTest]  WITH CHECK ADD  CONSTRAINT [FK__UserTest__TestId__38996AB5] FOREIGN KEY([TestId])
REFERENCES [dbo].[Test] ([Id])
GO
ALTER TABLE [dbo].[UserTest] CHECK CONSTRAINT [FK__UserTest__TestId__38996AB5]
GO
ALTER TABLE [dbo].[UserTest]  WITH CHECK ADD  CONSTRAINT [FK_UserTests] FOREIGN KEY([UserId])
REFERENCES [dbo].[EndUser] ([Id])
GO
ALTER TABLE [dbo].[UserTest] CHECK CONSTRAINT [FK_UserTests]
GO

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Recruitment.Repository.Shared.Contracts;
using Recruitment.Repository.Shared.Entities;
using Microsoft.EntityFrameworkCore;

namespace Recruitment.Repository.Implementation.Concretes
{
    public class CollegeInfoRepository:ICollegeInfoRepository
    {
        StudentRecruitmentContext _context;
        public CollegeInfoRepository(StudentRecruitmentContext context)
        {
            _context = context;
        }

        public void Add(CollegeInfo college)
        {
            _context.Entry(college).State = EntityState.Added;
            _context.SaveChanges();
        }

        public List<CollegeInfo> GetAllColleges(bool flag)
        {
            if (flag)
            {
                return _context.CollegeInfo.Select(a =>a).ToList();
            }
            return _context.CollegeInfo.Where(a=>a.IsActive).ToList();
        }

        public CollegeInfo GetById(int id)
        {
            return _context.CollegeInfo.FirstOrDefault(col => col.Id == id);
        }

        public CollegeInfo GetByName(string name)
        {
            string upperCaseName = name.ToUpper();
            return _context.CollegeInfo.FirstOrDefault(col => col.CollegeName.ToUpper() == upperCaseName);
        }

        public void Update(CollegeInfo college)
        {
            _context.Entry(college).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}

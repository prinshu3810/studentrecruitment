﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Recruitment.Repository.Shared.Contracts;
using Recruitment.Repository.Shared.Entities;
using System.Threading.Tasks;

namespace Recruitment.Repository.Implementation.Concretes
{
    public class EndUserRepository : IEndUserRepository
    {
        StudentRecruitmentContext _context;
        public EndUserRepository(StudentRecruitmentContext context)
        {
            _context = context;
        }
        public EndUser GetByPhonenumber(string phoneNumber)
        {
            return _context.EndUser.FirstOrDefault(a => a.PhoneNo == phoneNumber);
        }
        public void Update(EndUser user)
        {
            _context.Entry(user).State = EntityState.Modified;
            _context.SaveChanges();
        }
        public void AddUser(EndUser user)
        {
            _context.Entry(user).State = EntityState.Added;
            _context.SaveChanges();
        }
        public EndUser GetById(Guid id)
        {
            return _context.EndUser.SingleOrDefault(a => a.Id == id);

        }
        public EndUser GetDetailsById(Guid id)
        {
            return (_context.EndUser
                .Include(s => s.Student)
               .Include(i => i.UserTest).ThenInclude(ti => ti.UserQuestionAnswer)
               .FirstOrDefault(e => e.Id == id));

        }

        public EndUser GetByEmail(string email)
        {
            return _context.EndUser.SingleOrDefault(a => a.Email == email);
        }
        public EndUser GetByUsername(string username)
        {
            return _context.EndUser.SingleOrDefault(a => a.Name == username);
        }

        public async Task DeleteAsync(EndUser enduser)
        {
            var tests = enduser.UserTest.ToList();
            var questionAnswers = enduser.UserTest.SelectMany(u => u.UserQuestionAnswer).ToList();
            var transaction = await _context.Database.BeginTransactionAsync();
            {
                try
                {
                    _context.UserQuestionAnswer.RemoveRange(questionAnswers);
                    _context.UserTest.RemoveRange(tests);
                    _context.Student.Remove(enduser.Student);
                    _context.EndUser.Remove(enduser);

                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                }
                catch (Exception e)
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
        }
    }
}

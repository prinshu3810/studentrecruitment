﻿using Recruitment.Repository.Shared.Contracts;
using Recruitment.Repository.Shared.Entities;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Recruitment.Repository.Implementation.Concretes
{
    public class SectionRepository : ISectionRepository
    {
        StudentRecruitmentContext _context;
        public SectionRepository(StudentRecruitmentContext context)
        {
            _context = context;
        }

        public void Add(Section section)
        {
            _context.Entry(section).State = EntityState.Added;
            _context.SaveChanges();
        }

        public Section GetById(int Id)
        {
            return _context.Section.FirstOrDefault(sec => sec.Id == Id);
        }

        public List<Section> GetByTestId(int testId)
        {
            return _context.Section.Where(sec => sec.TestId == testId).ToList();
        }
        public List<Section> GetDetailsByTestId(int testId)
        {
            return _context.Section.Where(sec => sec.TestId == testId)
                .Include(sec => sec.Questions)
                .ThenInclude(q => q.QuestionOption)
                .ToList();
        }

        public void Update(Section section)
        {
            _context.Entry(section).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Recruitment.Foundation.Common;
using Recruitment.Repository.Shared.Contracts;
using Recruitment.Repository.Shared.Dto;
using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Repository.Implementation.Concretes
{
    public class StudentRepository : IStudentRepository
    {
        StudentRecruitmentContext _context;
        public StudentRepository(StudentRecruitmentContext context)
        {
            _context = context;
        }
        public void Add(Student student)
        {
            _context.Entry(student).State = EntityState.Added;
            _context.SaveChanges();
        }

        public Student GetById(Guid userId)
        {
            return _context.Student.FirstOrDefault(stu => stu.Id == userId);
        }

        public Student GetDetailsById(Guid userId)
        {
            return (_context.Student
                .Include(nameof(Student.College))
                .Include(i => i.IdNavigation).ThenInclude(ti => ti.UserTest)
                .FirstOrDefault(st => st.Id == userId));
        }

        public List<StudentInfoRepoModel> GetInfo()
        {
            var query = (from s in _context.Student
                         join e in _context.EndUser
                         on s.Id equals e.Id
                         join u in _context.UserTest
                         on e.Id equals u.UserId
                         join col in _context.CollegeInfo
                         on s.CollegeId equals col.Id
                         join cor in _context.Course
                        on s.CourseId equals cor.Id
                         select new
                         {
                             Id = e.Id,
                             Name = e.Name,
                             Marks = u.Marks,
                             Pass = u.Pass,
                             CollegeName = col.CollegeName,
                             CourseName = cor.CourseType,
                             Date = u.DateGiven,
                             Test = u.TestId,
                             CollegeId = col.Id,
                             CourseId = cor.Id,
                             PhoneNumber = e.PhoneNo,
                             Email = e.Email
                         }).ToList();
            return query.GroupBy(a => new { a.Id, a.Date })
                .Select(z => new StudentInfoRepoModel()
                {
                    Id = z.Key.Id,
                    Date = z.Key.Date,
                    Name = z.FirstOrDefault()?.Name,
                    College = z.FirstOrDefault()?.CollegeName,
                    Course = z.FirstOrDefault()?.CourseName,
                    AptiMarks = z.FirstOrDefault(a => a.Test == (int)TestTypes.AptitudeTestId) != null && z.FirstOrDefault(a => a.Test == (int)TestTypes.AptitudeTestId).Marks != null ? z.FirstOrDefault(a => a.Test == (int)TestTypes.AptitudeTestId).Marks : 0,
                    CodingMarks = z.FirstOrDefault(a => a.Test == (int)TestTypes.CodingTestId) != null && z.FirstOrDefault(a => a.Test == (int)TestTypes.CodingTestId).Marks != null ? z.FirstOrDefault(a => a.Test == (int)TestTypes.CodingTestId).Marks : 0,
                    CollegeId = z.FirstOrDefault()?.CollegeId,
                    CourseId = z.FirstOrDefault()?.CourseId,
                    PhoneNumber = z.FirstOrDefault()?.PhoneNumber,
                    AptiStatus = z.FirstOrDefault(t => t.Test == (int)TestTypes.AptitudeTestId)?.Pass,
                    CodingStatus = z.FirstOrDefault(t => t.Test == (int)TestTypes.CodingTestId)?.Pass,
                    Email = z.FirstOrDefault()?.Email
                }).OrderBy(o => o.Date).ToList();


        }

        public StudentPersonalInfoRepoModel GetPersonalDetails(Guid userId)
        {
            var response = (from e in _context.EndUser
                            join s in _context.Student on e.Id equals s.Id
                            join col in _context.CollegeInfo on s.CollegeId equals col.Id
                            join cor in _context.Course on s.CourseId equals cor.Id
                            select new
                            {
                                Id = e.Id,
                                Name = e.Name,
                                College = col.CollegeName,
                                Course = cor.CourseType,
                                PhoneNumber = e.PhoneNo,
                                AlternatePhoneNumber = s.AlternatePhoneNo,
                            }).ToList();
            var studentInfo = response.FirstOrDefault(a => a.Id == userId);
            return new StudentPersonalInfoRepoModel()
            {
                Name = studentInfo.Name,
                College = studentInfo.College,
                Course = studentInfo.Course,
                PhoneNumber = studentInfo.PhoneNumber,
                AlternatePhoneNumber = studentInfo.AlternatePhoneNumber
            };

        }
    }
}

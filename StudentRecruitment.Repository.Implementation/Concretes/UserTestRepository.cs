﻿using Microsoft.EntityFrameworkCore;
using Recruitment.Repository.Shared.Contracts;
using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Repository.Implementation.Concretes
{
    public class UserTestRepository:IUserTestRepository
    {
        StudentRecruitmentContext _context;
        public UserTestRepository(StudentRecruitmentContext context)
        {
            _context = context;
        }

        public void Add(UserTest userTest)
        {
            _context.Entry(userTest).State = EntityState.Added;
            _context.SaveChanges();
        }

        public UserTest Get(Guid id,int testId,DateTime date)
        {
           return  _context.UserTest.FirstOrDefault(a => a.UserId == id && a.DateGiven == date && a.TestId==testId);
        }

        public List<UserTest> GetById(Guid userId)
        {
            return _context.UserTest.Where(u => u.UserId == userId).ToList();
        }

        public void Update(UserTest userTest)
        {
            _context.Entry(userTest).State = EntityState.Modified;
            _context.SaveChanges();
        }
        public void UpdateWithoutSave(UserTest userTest)
        {
            _context.Entry(userTest).State = EntityState.Modified;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using IRepository.Shared.Contracts;
using Recruitment.Repository.Shared.Entities;

namespace Recruitment.Repository.Implementation.Concretes
{
    public class CourseRepository:ICourseRepository
    {
        StudentRecruitmentContext _context;
        public CourseRepository(StudentRecruitmentContext context)
        {
            _context = context;
        }

        public List<Course> GetAllCourses()
        {
            return _context.Course.Select(a => a).ToList();
        }
    }
}

﻿using Recruitment.Repository.Shared.Contracts;
using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Recruitment.Repository.Implementation.Concretes
{
    public class QuestionOptionRepository : IQuestionOptionRepository
    {
        StudentRecruitmentContext _context;
        public QuestionOptionRepository(StudentRecruitmentContext context)
        {
            _context = context;
        }

        public void Add(QuestionOption option)
        {
            _context.Entry(option).State = EntityState.Added;
            _context.SaveChanges();
        }

        public List<QuestionOption> GetByQuestionId(int id)
        {
            return _context.QuestionOption.Where(o => o.QuestionId == id && o.IsActive==true).ToList();
        }

        public void Update(QuestionOption option)
        {
            _context.Entry(option).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}

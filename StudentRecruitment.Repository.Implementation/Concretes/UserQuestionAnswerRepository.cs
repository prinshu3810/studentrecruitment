﻿using Microsoft.EntityFrameworkCore;
using Recruitment.Repository.Shared.Contracts;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Recruitment.Repository.Shared.Entities;
using System.Threading.Tasks;

namespace Recruitment.Repository.Implementation.Concretes
{
    public class UserQuestionAnswerRepository : IUserQuestionAnswerRepository
    {
        StudentRecruitmentContext _context;
        public UserQuestionAnswerRepository(StudentRecruitmentContext context)
        {
            _context = context;
        }

        public void Add(UserQuestionAnswer userQuestionAnswer)
        {
            _context.Entry(userQuestionAnswer).State = EntityState.Added;
            _context.SaveChanges();
        }
        public void AddMultiple(List<UserQuestionAnswer> userQuestionAnswer)
        {
            userQuestionAnswer.ForEach(u =>
            {
                _context.Entry(u).State = EntityState.Added;
            });
            _context.SaveChanges();
        }

        public List<UserQuestionAnswer> GetByQuestionId(int id)
        {
            return _context.UserQuestionAnswer.Where(a => a.QuestionId == id).ToList();
        }

        public List<UserQuestionAnswer> GetByUserTestId(int userTestId)
        {
            return _context.UserQuestionAnswer.Where(a => a.UserTestId == userTestId).ToList();
        }

        public void Update(UserQuestionAnswer userQuestionAnswer)
        {
            _context.Entry(userQuestionAnswer).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}

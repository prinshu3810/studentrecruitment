﻿using Recruitment.Repository.Shared.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Recruitment.Repository.Shared.Entities;

namespace Recruitment.Repository.Implementation.Concretes
{
    public class TestRepository:ITestRepository
    {
        StudentRecruitmentContext _context;
        public TestRepository(StudentRecruitmentContext context)
        {
            _context = context;
        }

       
        public Test GetById(int id)
        {
            return _context.Test.FirstOrDefault(a => a.Id == id);
        }
    }
}

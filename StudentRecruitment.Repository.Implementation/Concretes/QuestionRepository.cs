﻿using Recruitment.Repository.Shared.Contracts;
using Recruitment.Repository.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Recruitment.Repository.Shared.Entities;
using Recruitment.Foundation.Common;
using Microsoft.EntityFrameworkCore;

namespace Recruitment.Repository.Implementation.Concretes
{
    public class QuestionRepository:IQuestionRepository
    {
        StudentRecruitmentContext _context;
        public QuestionRepository(StudentRecruitmentContext context)
        {
            _context = context;
        }

        public void Add(Question question)
        {
            _context.Entry(question).State = EntityState.Added;
            _context.SaveChanges();
        }

        public List<AptitudeQuestionAnswerRepo> GetAptiQuestionAnswer()
        {
            var correctAnswersQuery = from q in _context.Question
                                      join o in _context.QuestionOption
                                      on q.Id equals o.QuestionId
                                      where o.IsCorrect == true && q.IsActive == true && o.IsActive == true
                                      select new
                                      {
                                          questionId = o.QuestionId,
                                          questionMarks = q.Marks,
                                          correctOptionId = o.Id,

                                      };
            var result= correctAnswersQuery.ToList();
             var answers=result.Select(a =>
             {
                 return new AptitudeQuestionAnswerRepo()
                 {
                     QuestionId = a.questionId,
                     QuestionMarks = a.questionMarks,
                     CorrectOptionId = a.correctOptionId

                 };
 
             }).ToList();
            return answers;
        }

        public List<AptitudeQuestionRepo> GetAptitudeQuestion()
        {
            var query = from r in _context.Question
                        join o in _context.QuestionOption
                        on r.Id equals o.QuestionId
                        where r.SectionId!=(int)SectionId.Programming
                        select new
                        {
                            questionId = r.Id,
                            questionContent = r.QuestionContent,
                            optionContent = o.QuestionOption1,
                            optionId = o.Id,
                            SectionId=r.SectionId
                        }
                        ;
            var result = query.ToList();
            var questions = result.GroupBy(a => a.questionId).Select(s =>
            {
                return new AptitudeQuestionRepo()
                {
                    QuestionId = s.Key,
                    QuestionContent = result.FirstOrDefault(f => f.questionId == s.Key)?.questionContent,
                    Options = s.Select(a =>
                    {
                        return new AptitudeOptionRepo()
                        {
                            OptionId = a.optionId,
                            Option = a.optionContent
                        };
                    }).ToList(),
                    SectionId=result.FirstOrDefault(z=> z.questionId==s.Key).SectionId,
                };
            }).ToList();
            return questions;
        }

        public Question GetById(int id)
        {
            return _context.Question.FirstOrDefault(a => a.Id == id);
        }

        public Question GetByQuestionContent(string questionContent)
        {
            return _context.Question.FirstOrDefault(ques => ques.QuestionContent == questionContent);
        }

        public List<Question> GetBySectionId(int sectionId)
        {
            return _context.Question.Where(q => q.SectionId == sectionId).ToList();
        }

        public List<Question> GetCodingQuestion(int sectionId)
        {
            return _context.Question.Where(a => a.SectionId==sectionId).ToList();
        }

        public void Update(Question question)
        {
            _context.Entry(question).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Request
{
    public class AdminSectionAddRequestModel
    {
        public string SectionName { get; set; }
    }
}

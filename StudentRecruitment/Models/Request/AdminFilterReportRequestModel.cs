﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Request
{
    public class AdminFilterReportRequestModel
    {
        public int Course { get; set; }
        public int College { get; set; }
    }
}

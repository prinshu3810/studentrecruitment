﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Request
{
    public class OtpRequestModel
    {
        public string Otp { get; set; }
    }
}

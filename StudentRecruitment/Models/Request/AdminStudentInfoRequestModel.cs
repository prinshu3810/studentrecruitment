﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Request
{
    public class AdminStudentInfoRequestModel
    {
        public string Id { get; set; }
        public string Date { get; set; }
    }
}

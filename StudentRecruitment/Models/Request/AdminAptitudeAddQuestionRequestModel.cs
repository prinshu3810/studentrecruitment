﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Request
{
    public class AdminAptitudeAddQuestionRequestModel
    {
        public int SectionId { get; set; }
        public List<string> Options { get; set; }
        public string QuestionContent { get; set; }
        public string CorrectOption { get; set; }
    }
}

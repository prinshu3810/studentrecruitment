﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Request
{
    public class CodingQuestionChangeRequestModel
    {
        public int QuestionId { get; set; }
        public int SelectedQuestion { get; set; }
        public string Usercode { get; set; }
        public int UserTestId { get; set; }
    }
}

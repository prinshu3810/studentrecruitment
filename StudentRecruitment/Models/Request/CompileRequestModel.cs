﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Request
{
    public class CompileRequestModel
    {
        public string Language { get; set; }
        public string Script { get; set; }
    }
}

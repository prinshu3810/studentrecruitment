﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Request
{
    public class RegistrationRequestModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }
        public int  Semester { get; set; }
        public int  CourseId { get; set; }
        public string RollNo { get; set; }
        public int  CollegeId { get; set; }
        public int AuthenticateVia { get; set; }
    }
}

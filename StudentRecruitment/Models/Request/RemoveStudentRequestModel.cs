﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Request
{
    public class RemoveStudentRequestModel
    {
        public string UserId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Request
{
    public class AdminLoginRequestModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Request
{
    public class LoginRequestModel
    {
        public string Input { get; set; }
        public int flag { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Request
{
    public class AdminUpdateMarksRequestModel
    {
        public string Id { get; set; }
        public int Marks { get; set; }
        public string Date { get; set; }
        public int Result { get; set; }
    }
}

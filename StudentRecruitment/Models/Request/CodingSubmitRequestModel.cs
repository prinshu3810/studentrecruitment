﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Request
{
    public class CodingSubmitRequestModel
    {
        public string Usercode { get; set; }
        public int QuestionId { get; set; }
        public int UserTestId { get; set; }
    }
}

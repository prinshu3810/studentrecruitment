﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Response
{
    public class OtpVerificationResponseModel
    {
        public bool IsVerified { get; set; }
        public bool IsExpired { get; set; }
    }
}

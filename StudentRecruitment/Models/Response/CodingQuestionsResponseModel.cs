﻿using Recruitment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Response
{
    public class CodingQuestionsResponseModel
    {
        public CodingTestQuestionResponseModel CodingQuestion { get; set; }
        public int SelectedQuestion { get; set; }
        public int QuestionId { get; set; }
        public string UserCode { get; set; }
       
    }
}

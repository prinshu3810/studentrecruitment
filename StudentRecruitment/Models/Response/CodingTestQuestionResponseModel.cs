﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Response
{
    public class CodingTestQuestionResponseModel
    {
        public string QuestionContent { get; set; }
        public int QuestionId { get; set; }
        public string QuestionType { get; set; }
        public string CodeSubmitted { get; set; }
        public int UserTestId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Response
{
    public class AptitudeQuestionResponseModel
    {
        public int QuestionId { get; set; }
        public string QuestionContent { get; set; }
        public string QuestionType { get; set; }
        public List<AptitudeOptionResponseModel> Options { get; set; } = new List<AptitudeOptionResponseModel>();
    }

}

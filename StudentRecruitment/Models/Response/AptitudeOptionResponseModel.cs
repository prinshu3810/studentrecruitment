﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Response
{
    public class AptitudeOptionResponseModel
    {
        public int OptionId { get; set; }
        public string Option { get; set; }
    }
}

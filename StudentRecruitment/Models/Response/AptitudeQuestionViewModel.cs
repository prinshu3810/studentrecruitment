﻿using Recruitment.Domain.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Response
{
    public class AptitudeQuestionViewModel
    {
        public List<AptitudeQuestion> OOPS { get; set; }
        public List<AptitudeQuestion> Logical { get; set; }
        public List<AptitudeQuestion> Language { get; set; }
    }
}

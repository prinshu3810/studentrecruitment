﻿using Recruitment.Domain.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Response
{
    public class StudentReportModel
    {
        public List<RegisterCollegesResponseModel> Colleges = new List<RegisterCollegesResponseModel>();
        public List<RegisterCoursesResponseModel> Courses = new List<RegisterCoursesResponseModel>();
        public List<StudentInfoModel> StudentInfo = new List<StudentInfoModel>();
    }
}

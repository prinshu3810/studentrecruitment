﻿$(document).ready(function () {
    $("#warningRefreshModal").modal({
        backdrop: 'static',
        keyboard: false
    });
    $("#warningRefreshModal").modal("show");
    $("#clock").removeClass("d-none");
    $("#questionBankLink").addClass("d-none");
    $.ajax({
        type: "GET",
        url: "/Aptitude/TestDuration",
        headers: {
            'content-type': 'application/json'
        },
        success: function (result) {
            $("#clock").innerHTML = `Time Left ${result}:00`;
            testInterval(result*60*1000);
        },
        error: function (result) {
            alert('Failed to get Test Duration');
        }
    });
    $("#logoutbtn").addClass("d-none");
    const form = $("#aptitudeTest");
    let autoSubmit = false;
    let awayCountdown = 30 * 1000;
    var inter = setInterval(warningInterval,1000);
    var blurInter;
    let timesMinimized = 0;
    const permissibleMinimiseLimit = 3;

    $(document).on("click", ".rotate", function () {
        $(this).toggleClass("down");
    });
    $("#warningModalConfirm").on("click", function () {
        var el = document.documentElement,
            rfs = el.requestFullscreen
                || el.webkitRequestFullScreen
                || el.mozRequestFullScreen
                || el.msRequestFullscreen
            ;

        rfs.call(el);
        clearInterval(inter);
        clearInterval(blurInter);
        awayCountdown = 30 * 1000;
    });

    $(window).blur(function () {
        clearInterval(inter);
        timesMinimized++;
        if (timesMinimized === permissibleMinimiseLimit) {
            autoSubmit = true;
            $("#aptitudeSubmit").trigger("click");
            return;
        }
        clearInterval(blurInter);
        document.getElementById("warningsLeft").innerHTML = `Warnings Left:${permissibleMinimiseLimit - timesMinimized}`;
        awayCountdown = 30 * 1000;
        $("#warningRefreshModal").modal("show");
        blurInter = setInterval(warningInterval, 1000);

    });

    document.addEventListener('contextmenu', event => event.preventDefault());

    function warningInterval() {
        awayCountdown -= 1000;
        var sec = Math.floor(awayCountdown / 1000);

        if (awayCountdown < 0) {
            clearInterval(inter);
            clearInterval(blurInter);
            $("#warningRefreshModal").modal('hide');
            autoSubmit = true;
            $("#aptitudeSubmit").trigger("click");
        }
        else {
            if (sec < 10) {
                sec = `0${sec}`;
            }

            document.getElementById("timeLeft").innerHTML = `Time left:${sec} seconds`;
        }
    }

    document.addEventListener('fullscreenchange', (event) => {

        if (document.fullscreenElement) {
            clearInterval(inter);
            awayCountdown = 30 * 1000;

        } else {
            timesMinimized++;
            if (timesMinimized === permissibleMinimiseLimit) {
                autoSubmit = true;
                $("#aptitudeSubmit").trigger("click");
                return;
            }
            document.getElementById("warningsLeft").innerHTML = `Warnings Left:${permissibleMinimiseLimit - timesMinimized}`;
            $("#warningRefreshModal").modal("show");
            inter = setInterval(warningInterval, 1000);
        }
    });

    function testInterval(countdown) {
        let timerId = setInterval(function () {
            countdown -= 1000;
            var min = Math.floor(countdown / (60 * 1000));
            var sec = Math.floor((countdown - (min * 60 * 1000)) / 1000);

            if (countdown < 0) {
                clearInterval(timerId);
                $("#aptitudeSubmit").trigger("click");
            }
            else {
                if (min < 10) {
                    min = `0${min}`;
                }
                if (sec < 10) {
                    sec = `0${sec}`;
                }

                document.getElementById("clock").innerHTML = `Time Left ${min}:${sec}`;
            }

        }, 1000);
    }
   

    function getQuestions() {
        let data = [];
        let optionIds = [];
        let questionIds = $(".questionId");
        for (let i = 0; i < questionIds.length; i++) {
            let optionId = $(`input[name=${questionIds[i].value}]:checked`).val();
            optionIds.push(optionId);
        }
        for (let i = 0; i < questionIds.length; i++) {
            if (optionIds[i] != null) {
                optionIds[i] = parseInt(optionIds[i]);
            }
            data.push({
                QuestionId: parseInt(questionIds[i].value),
                OptionId: optionIds[i]

            })
        }
        $.ajax({
            type: "POST",
            url: "/Aptitude/Questions",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify(data),
            success: function (result) {

                if (result === true) {
                    console.log(result)
                    window.location.href = "/Coding/TestDetails";
                }
                else {
                    $("#clock").addClass("d-none");
                    if (autoSubmit) {
                        $("#test").replaceWith(`<div class="container"> <p class="mt-3 text-danger text-center">Your test has been submitted and you could not qualify for the next round.</p></div>`)
                    } else {
                        $("#test").replaceWith(result);
                    }
                }
            },
            error: function (result) {
                alert('failed to submit questions');
            }
        });
    }

    $("#aptitudeSubmit").on("click", function (e) {
        setTimeout(function () {
            getQuestions();
        }, 1000)

    });

});
﻿$(document).ready(function () {
    let resendTime = 5 * 60 * 1000;
    let inter = setInterval(resendInterval, 1000);
    $("#otpForm").on("submit", function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/Login/OtpAuthentication",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({

                otp: $("#Otp").val()
            }),
            success: function (result) {
                $("#rensendMessage").addClass("d-none");
                if (result.isExpired) {
                    $("#expiredOtp").removeClass("d-none")
                }
                else if (!result.isVerified && !result.isExpired) {
                    $("#invalidOtp").removeClass("d-none")
                }
                else {
                                      
                    window.location.href = "/Aptitude/TestDetails";
                }
            },
            error: function (result) {
                alert('unable to process otp');
            }
        });
    });
    function resendInterval() {
        resendTime -= 1000;
        var min = Math.floor(resendTime / (60 * 1000));
        var sec = Math.floor((resendTime - (min * 60 * 1000)) / 1000);

        if (resendTime < 0) {
            clearInterval(inter);
            $('#resendBtn').prop("disabled", false);
            $('#resendBtn').addClass("ac-span");
        }
        else {
            if (min < 10) {
                min = `0${min}`;
            }
            if (sec < 10) {
                sec = `0${sec}`;
            }

        }
    }
    $("#resendBtn").on("click", function () {
        $("#expiredOtp").addClass("d-none");
        $("#invalidOtp").addClass("d-none");
        $('#resendBtn').prop("disabled", true);
        $('#resendBtn').removeClass("ac-span");
        $.ajax({
            type: "GET",
            url: "/Login/ResendOtp",
            headers: {
                'content-type': 'application/json'
            },
            success: function (result) {
                $("#rensendMessage").removeClass("d-none");
            },
            error: function (result) {
                alert('unable to resend otp');
            }
        });
    });
});
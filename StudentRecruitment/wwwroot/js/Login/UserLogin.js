﻿$(document).ready(function () {
    $("#adminLogin").removeClass("d-none");
    $("#registerButton").removeClass("d-none");
    $("#loginButton").addClass("d-none");
    let isvalid = false;
    $("#loginForm").on("submit", function (e) {
        e.preventDefault();
        if (isvalid) {
            localStorage.setItem("Register", "No");
            let input = $("#input").val();
            let flag = input.includes("@") ? 0 : 1;
            $.ajax({
                type: "POST",
                url: "/Login/UserLogin",
                headers: {
                    'content-type': 'application/json'
                },
                data: JSON.stringify({

                    Input: input,
                    Flag: flag

                }),
                success: function (result) {
                    if (result === false) {
                        $("#invalidUser").removeClass("d-none")
                    }
                    else {
                        $("#loginForm").replaceWith(result);
                    }
                },
                error: function (result) {
                    alert('Login failed');
                }
            });
        }
       
    });
    $("#adminLogin").on("click", function (e) {

        e.preventDefault();
        $.ajax({
            type: "GET",
            url: "/Login/AdminLogin",
            success: function (result) {
                $("#loginForm").replaceWith(result);
                $("#loginButton").removeClass("d-none");
                $("#adminLogin").addClass("d-none");
            },
            error: function (result) {
                alert('Login failed');
            }
        });
    });

    $("#input").blur(function (e) {
        var ep_emailval = $('#input').val();
        var intRegex = /[0-9 -()+]+$/;
        if (intRegex.test(ep_emailval)) {

            if ((ep_emailval.length < 9) || (ep_emailval.length > 10) || (!intRegex.test(ep_emailval))) {

                isvalid = false;
                $("#inputError").html("Please enter a valid phone number")
            }
            else {
                isvalid = true;
                $("#inputError").html("")
            }

        }
        else {
            var eml = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

            if (eml.test(ep_emailval) == false) {
                isvalid = false;
                $("#inputError").html("Please enter a valid email address")
            }
            else {

                isvalid = true;
                $("#inputError").html("")
            }

        }

    });
    $(document).on("click","#adminLoginbtn", function () {
        //e.preventDefault();
        $("#invalidPassword").addClass("d-none");
        $("#invalidAdmin").addClass("d-none")
        let username = $("#username").val();
        let password = $("#password").val();
        if (username.length == 0) {
            $("#invalidAdmin").removeClass("d-none");
            return;
        }
        if (password.length == 0) {
            $("#invalidPassword").removeClass("d-none")
            return;
        }
        $.ajax({
            type: "POST",
            url: "/Login/AdminLogin",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({

                Username: username,
                Password: password

            }),
            success: function (result) {
                if (result.isPresent == false) {
                    $("#invalidAdmin").removeClass("d-none");
                }
                else {
                    if (result.isPresent == true && result.isPasswordCorrect == false) {
                        $("#invalidAdmin").addClass("d-none");
                        $("#invalidPassword").removeClass("d-none")
                    }
                    else {
                        window.location.href = "/Admin/Report"
                    }
                }
            },
            error: function (result) {
                alert('Login failed');
            }
        });
    });
});
﻿$(document).ready(function () {
    $("#warningRefreshModal").modal({
        backdrop: 'static',
        keyboard: false
    });
    $("#warningRefreshModal").modal("show");
    $("#clock").removeClass("d-none");
    $.ajax({
        type: "GET",
        url: "/Coding/TestDuration",
        headers: {
            'content-type': 'application/json'
        },
        success: function (result) {
            $("#clock").innerHTML = `Time Left ${result}:00`;
            testInterval(result * 60 * 1000);
        },
        error: function (result) {
            alert('Failed to get Test Duration');
        }
    });
    $("#logoutbtn").addClass("d-none");
    $("#questionBankLink").addClass("d-none");
    $("#baseContainer").removeClass("container");

    let autoSubmit = false;
    let awayCountdown = 30 * 1000;
    var inter = setInterval(warningInterval, 1000);
    var blurInter;
    let timesMinimized = 0;
    const permissibleMinimiseLimit = 3;

    $(window).blur(function () {
        clearInterval(inter);
        timesMinimized++;
        if (timesMinimized === permissibleMinimiseLimit) {
            autoSubmit = true;
            $("#confirmSubmit").trigger("click");
            return;
        }
        clearInterval(blurInter);
        awayCountdown = 30 * 1000;
        document.getElementById("warningsLeft").innerHTML = `Warnings Left:${permissibleMinimiseLimit - timesMinimized}`;
        $("#warningRefreshModal").modal("show");
        blurInter = setInterval(warningInterval, 1000);

    });

    $(".change").on("click", function (e) {
        const selectedQuestion = parseInt(e.target.value);
        const questionId = parseInt($("#questionId").val());
        const userCode = $("#userCode").val();
        const userTestId = parseInt($("#userTestId").val());
        $.ajax({
            type: "POST",
            url: "/Coding/CodingSection",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({

                selectedQuestion: selectedQuestion,
                questionId: questionId,
                userCode: userCode,
                userTestId: userTestId
            }),
            success: function (result) {
                $("#codingSection").replaceWith(result);
                $("#error").addClass("d-none");
                $(".change").removeClass("bg");
                e.currentTarget.className += " bg";
            },
            error: function (result) {
                alert('Login failed');
            }
        });
    });

    $("#warningModalConfirm").on("click", function () {
        var el = document.documentElement,
            rfs = el.requestFullscreen
                || el.webkitRequestFullScreen
                || el.mozRequestFullScreen
                || el.msRequestFullscreen
            ;

        rfs.call(el);
        clearInterval(inter);
        clearInterval(blurInter);
        awayCountdown = 30 * 1000;
    });

    document.addEventListener('contextmenu', event => event.preventDefault());

    function warningInterval() {
        awayCountdown -= 1000;
        var sec = Math.floor(awayCountdown / 1000);

        if (awayCountdown < 0) {
            clearInterval(inter);
            clearInterval(blurInter);
            $("#warningRefreshModal").modal('hide');
            autoSubmit = true;
            $("#confirmSubmit").trigger("click");
        }
        else {
            if (sec < 10) {
                sec = `0${sec}`;
            }

            document.getElementById("timeLeft").innerHTML = `Time left:${sec} seconds`;
        }
    }

    document.addEventListener('fullscreenchange', (event) => {

        if (document.fullscreenElement) {
            clearInterval(inter);
            awayCountdown = 30 * 1000;

        } else {
            timesMinimized++;
            if (timesMinimized === permissibleMinimiseLimit) {
                autoSubmit = true;
                $("#confirmSubmit").trigger("click");
                return;
            }
            document.getElementById("warningsLeft").innerHTML = `Warnings Left:${permissibleMinimiseLimit - timesMinimized}`;
            $("#warningRefreshModal").modal("show");
            inter = setInterval(warningInterval, 1000);
        }
    });

    function testInterval(countdown) {
        let timerId = setInterval(function () {
            countdown -= 1000;
            var min = Math.floor(countdown / (60 * 1000));
            var sec = Math.floor((countdown - (min * 60 * 1000)) / 1000);

            if (countdown <= 0) {
                clearInterval(timerId);
                $("#codingForm").submit();
            }
            else {
                if (min < 10) {
                    min = `0${min}`;
                }
                if (sec < 10) {
                    sec = `0${sec}`;
                }
                document.getElementById("clock").innerHTML = `Time left ${min}:${sec}`;
            }

        }, 1000);
    }
    $("#compile").on("click", function (e) {
        const language = $("#language").val();
        const script = $("#userCode").val();
        $.ajax({
            type: "POST",
            url: "/Coding/CompileCode",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                language: language,
                script: script
            }),
            success: function (result) {
                $("#error").removeClass("d-none");
                $("#error").html(`Output:${result.output}`)
            },
            error: function (result) {
                alert('Login failed');
            }
        });
    });
    function saveCode(flag) {
        const code = $("#userCode").val();
        const questionId = parseInt($("#questionId").val());
        const userTestId = parseInt($("#userTestId").val());
        $.ajax({
            type: "POST",
            url: "/Coding/Test",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                userCode: code,
                questionId: questionId,
                userTestId: userTestId
            }),
            success: function (result) {
                if (flag == 1) {
                    if (autoSubmit) {
                        clearInterval(intervalId);
                        $("#codingTest").replaceWith(`<div class="container"> <p class="mt-3 text-danger text-center">Your test has been submitted and your responses have been saved.</p></div>`);
                    } else {
                        window.location.href = "/Coding/TestCompleted"
                    }
                }

            },
            error: function (result) {
                alert('Unable to submit');
            }
        });
    }
    //$("#codingForm").on("submit", function (e) {
    //    e.preventDefault();
    //    saveCode(1);

    //});
    $("#confirmSubmit").on("click", function (e) {
        //e.preventDefault();
        //clearInterval(intervalId);
        //window.location.href = "/Coding/TestCompleted";
        saveCode(1);

    })
    var intervalId = window.setInterval(function () { saveCode(0); }, 10000);
});
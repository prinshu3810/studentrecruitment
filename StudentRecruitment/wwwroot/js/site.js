﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.
$(document).ready(function () {
    $(document).on("click", "#logoutbtn", function () {
        $.ajax({
            type: "POST",
            url: "/Login/LoggedOut",
            headers: {
                'content-type': 'application/json'
            },
            success: function (result) {
                window.location.href = "/Login/UserLogin"
            },
            error: function (result) {
                alert('unable to logout');
            }
        });
    })
});

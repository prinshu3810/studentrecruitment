﻿$(document).ready(function () {
    $("#adminLogin").addClass("d-none");
    $("#registerButton").addClass("d-none");
    $("#loginButton").removeClass("d-none");
    const errorEmail = $("#errorEmail")
    $("#continue").on("click", function () {
        hideErrors();
        let name = $("#name").val();
        let email = $("#email").val();
        let rollNo = $("#rollNo").val();
        let phoneNumber = $("#phoneNumber").val();
        let alternatePhoneNumber = $("#alternatePhoneNumber").val();
        let semester = $("#semester").val();
        let courseId = $("#course").val();
        let collegeId = $("#college").val();

        if (name.length == 0) {
            $("#nameError").removeClass("d-none");
            return;
        }
        if (!validateEmail(email)) {
            $("#emailError").removeClass("d-none");
            return;
        }
        if (collegeId.length == 0) {
            $("#collegeError").removeClass("d-none");
            return;
        }
        if (rollNo.length == 0 || !validateRollNumber(rollNo)) {
            $("#rollnumberError").removeClass("d-none");
            return;
        }
        if (!validateMobile(phoneNumber)) {
            $("#phonenumberError").removeClass("d-none");
            return;
        }
        if (alternatePhoneNumber.length>0) {
            if (!validateMobile(alternatePhoneNumber)) {
                $("#altphoneError").removeClass("d-none");
                return;
            }           
        }
        if (semester.length==0) {
            $("#semesterError").removeClass("d-none");
            return;
        }
        if (courseId.length == 0) {
            $("#courseError").removeClass("d-none");
            return;
        }
      
        localStorage.setItem("Register", "yes");
        $.ajax({
            type: "POST",
            url: "/Register/Registration",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({

                name: name,
                email: email,
                rollNo: rollNo,
                phoneNumber: phoneNumber,
                alternatePhoneNumber: alternatePhoneNumber,
                semester: parseInt(semester),
                courseId: parseInt(courseId),
                collegeId: parseInt(collegeId),
                authenticatevia:0

            }),
            success: function (result) {
                if (result=== true) {
                    errorEmail.removeClass("d-none");
                    email.addClass("border-danger");
                }
                else  {
                    $("#registrationForm").replaceWith(result);
                    
                }
            },
            error: function (result) {
                alert('Unable to Register');
            }
        });
    });

    function hideErrors() {
        $("#nameError").addClass("d-none");
        $("#emailError").addClass("d-none");
        $("#rollnumberError").addClass("d-none");
        $("#phonenumberError").addClass("d-none");
        $("#altphoneError").addClass("d-none");
        $("#semesterError").addClass("d-none");
        $("#courseError").addClass("d-none");
        $("#collegeError").addClass("d-none");
    }

    function validateMobile(number) {
        var filter = /^\d*(?:\.\d{1,2})?$/;

        if (filter.test(number)) {
            if (number.length == 10) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    function validateEmail(email) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(email);;
    }
    function validateRollNumber(rollNumber) {
            var pattern = new RegExp("^[0-9]*$");
            return pattern.test(rollNumber);
    }
});

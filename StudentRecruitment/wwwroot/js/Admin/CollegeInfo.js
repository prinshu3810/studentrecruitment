﻿$(document).ready(function () {
    $("#clock").addClass("d-none");
    $(".link").removeClass("d-none");
    $("#collegeLink").addClass("activeTab");
    let editedCollegeId = 0;

    $(document).on("click", ".college", function () {
        editedCollegeId = $(this).data().value;
        $.ajax({
            type: "POST",
            url: "/Admin/CollegeEdit",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                collegeId: parseInt(editedCollegeId)
            }),
            success: function (result) {
                $("#editCollege").replaceWith(result);
            },
            error: function (result) {
                alert('failed to get college');
            }
        });
    });

    $(document).on("click", "#addBtn", function () {
        $.ajax({
            type: "GET",
            url: "/Admin/AddCollege",
            headers: {
                'content-type': 'application/json'
            },
            
            success: function (result) {
                $("#addCollege").replaceWith(result);
            },
            error: function (result) {
                alert('failed to add college');
            }
        });
    });
    $(document).on("click", "#editCollegeSubmit", function () {
        let name = $("#editedCollegeName").val();
        let cutOff = parseInt($("#editedCutoff").val());
        let isActive =  $("#editedIsActive").val()==="true";
        let contactPerson = $("#editedContactPerson").val();
        let contactNumber = $("#editedContactNumber").val();
        let address = $("#editedAddress").val();
        let state = $("#editedState").val();

        if (name.length == 0) {
            hideEditError();
            $("#editedCollegeNameError").removeClass("d-none");
            return;
        }
        if (contactPerson.length == 0) {
            hideEditError();
            $("#editedContactPersonError").removeClass("d-none");
            return;
        }
        if (!validateMobile(contactNumber)) {
            hideEditError();
            $("#editedContactError").removeClass("d-none");
            return;
        }
        if (address.length == 0) {
            hideEditError();
            $("#editedAddressError").removeClass("d-none");
            return;
        }
        if (state.length == 0) {
            hideEditError();
            $("#editedStateError").removeClass("d-none");
            return;
        }
        $.ajax({
            type: "POST",
            url: "/Admin/CollegeEditSubmit",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                id: parseInt(editedCollegeId),
                name: name,
                cutOff: cutOff,
                isActive: isActive,
                contactPerson: contactPerson,
                contactPersonNumber: contactNumber,
                address: address,
                state: state
            }),
            success: function (result) {
                if (result) {
                    $("#editCollegeModal").modal("hide");
                    updateCollegeList();
                } else {
                    $("#existError").removeClass("d-none");
                }
            },
            error: function (result) {
                alert('failed to update college details');
            }
        });
    });
    function hideError() {
        $("#collegeValidation").addClass("d-none");
        $("#cutoffError").addClass("d-none");
        $("#personError").addClass("d-none");
        $("#contactError").addClass("d-none");
        $("#addressError").addClass("d-none");
        $("#stateError").addClass("d-none");
        $("collegeNameError").addClass("d-none");
    }

    function hideEditError() {
        $("#editedContactPersonError").addClass("d-none");
        $("#editedContactError").addClass("d-none");
    }
    $(document).on("click", "#collegeClose", function () {
        hideError();
    });

    $(document).on("click", "#addCollegeSubmit", function () {
        let name = $("#newCollegeName").val();
        let cutOff = $("#newCutoff").val();
        let isActive = $("#newIsActive").val()==="true";
        let contactPerson = $("#newContactPerson").val();
        let contactNumber = $("#newContactNumber").val();
        let address = $("#address").val();
        let state = $("#state").val();

        if (name.length == 0) {
            hideError();
            $("#collegeValidation").removeClass("d-none");
            return;
        }
        if (cutOff.length == 0) {
            hideError();
            $("#cutoffError").removeClass("d-none");
            return;
        }

        if (contactPerson.length == 0) {
            hideError();
            $("#personError").removeClass("d-none");
            return;
        }
        if (!validateMobile(contactNumber)) {
            hideError();
            $("#contactError").removeClass("d-none")
            return;
        }
        if (address.length == 0) {
            hideError();
            $("#addressError").removeClass("d-none")
            return;
        }
        if (state.length == 0) {
            hideError();
            $("#stateError").removeClass("d-none")
            return;
        }

        $.ajax({
            type: "POST",
            url: "/Admin/CollegeAddSubmit",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                name: name,
                cutOff: parseInt(cutOff),
                isActive: isActive,
                contactPerson: contactPerson,
                contactPersonNumber: contactNumber,
                address: address,
                state: state
            }),
            success: function (result) {
                if (result === true) {
                    hideError();
                    updateCollegeList();
                    $("#addCollegeModal").modal("hide");
                } else {
                    $("#collegeNameError").removeClass("d-none");
                }             
            },
            error: function (result) {
                alert('failed to update college details');
            }
        });
    });

    function updateCollegeList() {
        $.ajax({
            type: "Get",
            url: "/Admin/UpdateCollegeList",
            headers: {
                'content-type': 'application/json'
            },
            success: function (result) {
                $("#collegeInfo").replaceWith(result);
            },
            error: function (result) {
                alert('failed to update college List');
            }
        });
    }

    function validateMobile(number) {
        var filter = /^\d*(?:\.\d{1,2})?$/;

        if (filter.test(number)) {
            if (number.length == 10) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
});
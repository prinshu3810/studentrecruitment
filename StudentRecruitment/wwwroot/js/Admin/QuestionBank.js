﻿$(document).ready(function () {
    let i = 1;
    let active = 0;
    let editedSectionId = "";
    let aciveSectionId = "";
    $(".link").removeClass("d-none");
    $("#questionBankLink").addClass("activeTab");
    updateSection("/Admin/UpdateAptitudeSection");

    $("#codingQuestionBankBtn").on("click", function () {
        setTimeout(function () {
            active = 1;
            $("#aptitudeQuestionBankBtn").removeClass("activeTest");
            $("#codingQuestionBankBtn").addClass("activeTest");
            updateSection("/Admin/UpdateCodingSection");
        }, 1000)

    });

    $("#aptitudeQuestionBankBtn").on("click", function () {
        setTimeout(function () {
            active = 0;
            $("#aptitudeQuestionBankBtn").addClass("activeTest");
            $("#codingQuestionBankBtn").removeClass("activeTest");
            updateSection("/Admin/UpdateAptitudeSection");
        }, 1000);

    });

    $(document).on("click", "#addButton", function () {
        if (active == 0) {
            aciveSectionId = $(this).closest("div.operation").find("input[name='aptitudeSectionId']").val();
            $("#aptitudeNewQuestionError").addClass("d-none");
            $("#aptitudeNewQuestionOptionError").addClass("d-none");
            openModel("/Admin/AptitudeQuestionAdd");
        }
        else {
            activeSectionId = $(this).closest("div.operation").find("input[name='codingSectionId']").val();
            $("#codingNewQuestionError").addClass("d-none");
            openModel("/Admin/CodingQuestionAdd");
        }
    });

    $(document).on("click", ".rotate", function () {
        $(this).toggleClass("down");
    });
    $("#addSectionClose").on("click", function () {
        $("#sectionNameError").addClass("d-none");
    });

    $(document).on("click", "#addCodingSubmit", function () {
        $("#codingNewQuestionError").addClass("d-none");
        $("#codingNewMarksError").addClass("d-none");
        let questionContent = $("#questionContent").val();
        let questionMarks = $("#questionMarks").val();
        let sectionId = parseInt(activeSectionId);
        if (questionContent.length == 0) {
            $("#codingNewQuestionError").removeClass("d-none");
            return;
        }
        if (questionMarks.length == 0) {
            $("#codingNewMarksError").removeClass("d-none");
            return;
        }
        $.ajax({
            type: "POST",
            url: "/Admin/CodingAddSubmit",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                questionContent: questionContent,
                questionMarks: parseInt(questionMarks),
                sectionId: parseInt(sectionId)
            }),
            success: function (result) {
                $("#addQuestionModel").modal("hide");
            },
            error: function (result) {
                alert('User not found');
            },
        });

    });

    $(document).on("click", "#sectionAddSubmit", function () {
        let sectionName = $("#sectionName").val();
        if (sectionName === "") {
            $("#sectionNameError").removeClass("d-none");
            return;
        }
        if (active == 0) {
            addSection("/Admin/AddAptitudeSection", sectionName);
        }
        else
            addSection("/Admin/AddCodingSection", sectionName);
    });
    function addSection(url, sectionName) {
        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                sectionName: sectionName
            }),
            success: function (result) {
                $("#addSectionModel").modal("hide");
            },
            error: function (result) {
                alert('Section not found');
            }
        });
    }
    function updateSection(url) {
        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'content-type': 'application/json'
            },
            success: function (result) {
                $("#questionBankSections").replaceWith(result);
            },
            error: function (result) {
                alert('Section not found');
            }
        });
    }

    function openModel(url) {
        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'content-type': 'application/json'
            },
            success: function (result) {
                $("#addModal").replaceWith(result);
            },
            error: function (result) {
                alert('failed to get questions');
            }
        });
    }

    $(document).on("click", "#editSectionBtn", function () {
        $("#editedSectionNameError").addClass("d-none");
        if (active == 0) {
            editedSectionId = $(this).closest("div.operation").find("input[name='aptitudeSectionId']").val();
        }
        else {
            editedSectionId = $(this).closest("div.operation").find("input[name='codingSectionId']").val();
        }

        $.ajax({
            type: "POST",
            url: "/Admin/EditSection",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                sectionId: parseInt(editedSectionId)
            }),
            success: function (result) {
                $("#editSection").replaceWith(result);
            },
            error: function (result) {
                alert('failed to get questions');
            }
        });
    })


    $(document).on("click", "#addOptionBtn", function () {
        $("#optionDiv").append(` <div class="d-flex align-items-center optionsSection"><input type="radio" name="opt" class="mr-2" value=${i} required><input type="text" class="inputs form-control option " id="${i}"  name="options"> <i class="fas fa-trash-alt ml-2 remove"  aria-hidden="true"></i></div>`);
        i++;
    });
    $(document).on("click", "#editSubmit", function () {
        let questionId = parseInt($("#questionId").val());
        let questionContent = $("#newQuestionContent").val();
        let isActive = Boolean($("#isActive").val() == "true");
        let optionSelector = $(".editoption");
        let options = [];
        if (questionContent.length == 0) {
            $("#aptitudeQuestionEditError").removeClass("d-none");
            return;
        }
        for (i = 0; i < optionSelector.length; i++) {

            let optionId = $(optionSelector[i]).attr("id");
            let option = $(optionSelector[i]).val();
            let isCorrect = optionSelector[i].previousElementSibling.checked;

            options.push({
                optionId: parseInt(optionId),
                option: option,
                isCorrect: isCorrect,
            });
        }
        $.ajax({
            type: "POST",
            url: "/Admin/AptitudeEditSubmit",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                questionId: questionId,
                questionContent: questionContent,
                options: options,
                isActive: isActive
            }),
            success: function (result) {
                $("#editQuestionModel").modal("hide");
            },
            error: function (result) {
                alert('User not found');
            },
        });

    });
    $(document).on("click", "#editCodingSubmit", function () {
        $("#codingQuestionEditError").addClass("d-none");
        $("#codingMarksError").addClass("d-none");
        let questionId = parseInt($("#questionId").val());
        let questionContent = $("#newQuestionContent").val();
        let isActive = Boolean($("#isActive").val() == "true");
        let questionMarks = $("#questionMarks").val();
        if (questionContent.length == 0) {
            $("#codingQuestionEditError").removeClass("d-none");
            return;
        }
        if (questionMarks.length == 0) {
            $("#codingMarksError").removeClass("d-none");
            return;
        }
        $.ajax({
            type: "POST",
            url: "/Admin/CodingEditSubmit",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                questionId: questionId,
                questionContent: questionContent,
                isActive: isActive,
                questionMarks: parseInt(questionMarks)
            }),
            success: function (result) {
                $("#editQuestionModel").modal("hide");
            },
            error: function (result) {
                alert('User not found');
            },
        });
    })

    $(document).on("click", ".saveModal", function () {
        setTimeout(1000);

        if (active == 0) {
            $("#aptitudeQuestionBankBtn").trigger('click');
        }
        else {
            $("#codingQuestionBankBtn").trigger('click');
        }
    })
    $(document).on("click", "#editAddOptionBtn", function () {
        $("#optionDiv").append(` <div class="d-flex align-items-center optionsSection"><input type="radio" name="editopt" class="mr-2" value=${i} required><input type="text" class="inputs form-control editoption " id="0" value="" name="options"> <i class="fas fa-trash-alt ml-2 remove"  aria-hidden="true"></i></div>`);
        i++;
    });
    $(document).on("click", ".remove", function (e) {
        $(event.target).parents(".optionsSection").remove();
    });

    $(document).on("click", "#addSubmit", function () {
        $("#aptitudeNewQuestionError").addClass("d-none");
        $("#aptitudeNewQuestionOptionError").addClass("d-none");
        let questionContent = $("#questionContent").val();
        let sectionId = parseInt(aciveSectionId);
        let optionSelector = $(".option");
        let radioValue = $(`input[name="opt"]:checked`).val();
        if (questionContent.length == 0) {
            $("#aptitudeNewQuestionError").removeClass("d-none");
            return;
        }
        if (radioValue == undefined) {
            $("#aptitudeNewQuestionOptionError").removeClass("d-none");
            return;
        }
        let correctOption = $(`#${radioValue}`).val();
        let options = [];
        for (let i = 0; i < optionSelector.length; i++) {
            options.push($(optionSelector[i]).val());
        }
        $.ajax({
            type: "POST",
            url: "/Admin/AptitudeAddSubmit",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                questionContent: questionContent,
                sectionId: sectionId,
                options: options,
                correctOption: correctOption,
            }),
            success: function (result) {
                $("#addQuestionModel").modal("hide");
            },
            error: function (result) {
                alert('User not found');
            },
        });

    });
    function editModel(url, questionId) {

        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                questionId: parseInt(questionId)
            }),
            success: function (result) {
                $("#editModal").replaceWith(result);
            },
            error: function (result) {
                alert('Unable to edit');
            },
        });
    }
    $(document).on("click", ".edit", function () {
        let questionId = $(this).closest("div.questionDiv").find("input[name='questionId']").val();

        if (active == 0) {
            $("#aptitudeQuestionEditError").addClass("d-none");
            editModel("/Admin/AptitudeQuestionEdit", questionId);
        }
        else {
            $("#codingQuestionEditError").addClass("d-none");
            editModel("/Admin/CodingQuestionEdit", questionId)
        }

    });
    $(document).on("click", "#editSectionSubmit", function () {

        let sectionId = parseInt(editedSectionId);
        let sectionName = $("#editedSectionName").val();
        let isActive = Boolean($("#isSectionActive").val() == "true");
        let questionCount = parseInt($("#questionsGiven").val());
        if (sectionName.length == 0) {
            $("#editedSectionNameError").removeClass("d-none");
            return;
        }
        $.ajax({
            type: "POST",
            url: "/Admin/EditSectionSubmit",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                sectionId: sectionId,
                sectionName: sectionName,
                isActive: isActive,
                questionCount: questionCount
            }),
            success: function (result) {
                $("#editSectionModal").modal("hide");
            },
            error: function (result) {
                alert('Unable to edit');
            },
        });
    });

});
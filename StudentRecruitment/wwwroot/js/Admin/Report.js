﻿$(document).ready(function () {
    $("#clock").addClass("d-none");
    $(".link").removeClass("d-none");
    $("#reportLink").addClass("activeTab");
    let studentId = "";
    let dateGiven = "";
    let userId = "";

    $(document).on("click", ".student", function (e) {
        studentId = (e.target.dataset.value);
        var z = e.target;
        var $row = $(z).closest("tr");
        var $td = $row.find("td:nth-child(7)");
        dateGiven = $td.text();
    });
    $("#searchInput").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#tableBody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    $("#download").click(function () {
        $("#info").table2excel({
            filename: "StudentReport.xls",
            preserveColors: false
        });
    });
    function showReport() {
        let courseSelected = $("#selectedCourse").val();
        let collegeSelected = $("#selectedCollege").val();
        if (courseSelected.length == 0) {
            courseSelected = "0";
        }
        if (collegeSelected.length == 0) {
            collegeSelected = "0";
        }
        $.ajax({
            type: "POST",
            url: "/Admin/FilterReport",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                course: parseInt(courseSelected),
                college: parseInt(collegeSelected)

            }),
            success: function (result) {
                $("#info").replaceWith(result);
            },
            error: function (result) {
                alert('error');
            }
        });
    }
    $("#studentReport").on("click", function (e) {
        showReport();
    });
    showReport();
    $(document).on("click", ".deleteStudent", function (e) {
        e.stopPropagation();
        userId = (e.target.dataset.value);
        $("#deleteModal").modal("show");
    });
    $(document).on("click", "#deleteConfirm", function () {
        userId = userId;
        $.ajax({
            type: "POST",
            url: "/Admin/RemoveStudent",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                userId: userId
            }),
            success: function (result) {
                $("#deleteModal").modal("hide");
                showReport();
            },
            error: function (result) {
                alert('unable to delete');
            }
        });
    })
});
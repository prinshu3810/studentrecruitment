﻿$(document).ready(function () {
    $(".link").removeClass("d-none");
    const target = $("#tabs");
    generateQueryParams = (queryString)=> {
        if (!queryString) {
            return {};
        }
        if (queryString.startsWith('?')) {
            queryString = queryString.substr(1);
        }
        const params = queryString.split('&');
        const queryParams = {};
        for (var index in params) {
            var keyValue = params[index].split('='); // index 0 -> key, 1 -> value.
            if (keyValue && keyValue[0]) {
                queryParams[keyValue[0]] = keyValue[1];
            }
        }
        return queryParams;
    }
    const studentDetails = generateQueryParams(window.location.search);
    let dateGiven = studentDetails.date;

    $.ajax({
        type: "POST",
        url: "/Admin/PersonalDetails",
        headers: {
            'content-type': 'application/json'
        },
        data: JSON.stringify({
            id: studentDetails.id,
        }),
        success: function (result) {
            if (result.alternatePhoneNumber == "") {
                result.alternatePhoneNumber = "_"
            }
            str = `<table class="table table-striped mt-5 personalInfo">
                     <tr>
                          <th>Name:</th>
                           <td>${result.name}</td>
                     </tr>
                       <tr>
                          <th>College:</th>
                           <td>${result.college}</td>
                        </tr>
                        <tr>
                         <th>Course:</th>
                         <td>${result.course}</td>
                        </tr>
                         <tr>
                         <th>ContactNumber:</th>
                         <td>${result.phoneNumber}</td>
                        </tr>
                          <tr>
                         <th>AlternateContactNumber:</th>
                         <td>${result.alternatePhoneNumber}</td>
                        </tr>
                    </table>`;
            html = $.parseHTML(str);
            target.after(html);
        },
        error: function (result) {
            alert('failed to update marks');
        }
    });

    $(document).on("click", ".rotate", function () {
        $(this).toggleClass("down");
    });

    $("#personalInfoTab").on("click", function (e) {
        $("#studentMarks").addClass("d-none");
        $(".personalInfo").remove();
        $("#codingTestInfoTab").removeClass("activeTest");
        $("#aptitudeTestInfoTab").removeClass("activeTest");
        $("#personalInfoTab").addClass("activeTest");
        $(".section").remove();
        $("#aptitudeInfo").remove();
        const target = $("#tabs");
        $.ajax({
            type: "POST",
            url: "/Admin/PersonalDetails",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                id: studentDetails.id
            }),
            success: function (result) {
                if (result.alternatePhoneNumber == "") {
                    result.alternatePhoneNumber = "_"
                }
                str = `<table class="table table-striped mt-5 personalInfo">
                         <tr>
                              <th>Name:</th>
                               <td>${result.name}</td>
                         </tr>
                           <tr>
                              <th>College:</th>
                               <td>${result.college}</td>
                            </tr>
                            <tr>
                             <th>Course:</th>
                             <td>${result.course}</td>
                            </tr>
                             <tr>
                             <th>ContactNumber:</th>
                             <td>${result.phoneNumber}</td>
                            </tr>
                              <tr>
                             <th>AlternateContactNumber:</th>
                             <td>${result.alternatePhoneNumber}</td>
                            </tr>
                        </table>`;
                html = $.parseHTML(str);
                target.after(html);
            },
            error: function (result) {
                alert('failed to update marks');
            }
        });

    });
   
    $("#codingTestInfoTab").on("click", function (e) {
        $(".section").remove();
        //$("#studentMarks").removeClass("d-none");
        $("#aptitudeTestInfoTab").removeClass("activeTest");
        $("#personalInfoTab").removeClass("activeTest");
        $("#codingTestInfoTab").addClass("activeTest");
        $(".personalInfo").addClass("d-none");
        $("#aptitudeInfo").remove();
        const target = $("#tabs");
        $.ajax({
            type: "POST",
            url: "/Admin/StudentCodingData",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                id: studentDetails.id,
                date: dateGiven
            }),
            success: function (result) {
                if (result === undefined) {
                    str = `<div class="section mt-5"><p>This user has not given coding Test</p></div>`
                    html = $.parseHTML(str);
                    target.after(html);
                }
                if (result.length === 0 || result == undefined) {
                    str = `<div class="section mt-5"><p>This user has not  attempted coding Test</p></div>`
                    html = $.parseHTML(str);
                    target.after(html);
                }
                else {
                    $("#studentMarks").removeClass("d-none");
                    if (result[0].marks) {
                        $("#marks").val(result[0].marks);
                        let status = result[0].codingStatus ? 1 : 0;
                        $("#result").val(status);
                    }
                    for (let i = 0; i < result.length; i++) {
                        str = `<div class='section mt-5'><p>Q:${result[i].question} <span class='text-danger ml-3'>Marks:${result[i].questionMarks}</span></p>
                        <textarea class="answer w-100">${result[i].response}</textarea></div>`;
                        html = $.parseHTML(str);
                        target.after(html);
                    }
                }

            },
            error: function (result) {
                alert('Student does not found');
            }
        });
    });
    $("#updateMarks").on("click", function () {
        const marksGiven = parseInt($("#marks").val());
        const result = parseInt($("#result").val())
        $.ajax({
            type: "POST",
            url: "/Admin/UpdateCodingMarks",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                id: studentDetails.id,
                marks: marksGiven,
                date: dateGiven,
                result: result
            }),
            success: function (result) {
                window.location.href = "/Admin/Report";
            },
            error: function (result) {
                alert('failed to update marks');
            }
        });
    });
    $("#aptitudeTestInfoTab").on("click", function () {
        $("#studentMarks").addClass("d-none");
        $(".personalInfo").remove();
        $(".section").remove();
        $("#aptitudeTestInfoTab").addClass("activeTest");
        $("#personalInfoTab").removeClass("activeTest");
        $("#codingTestInfoTab").removeClass("activeTest");
        $("#aptitudeInfo").remove();
        $.ajax({
            type: "POST",
            url: "/Admin/GetAttemptedQuestions",
            headers: {
                'content-type': 'application/json'
            },
            data: JSON.stringify({
                Id: studentDetails.id,
                date: dateGiven

            }),
            success: function (result) {
                $("#tabs").append(result);
            },
            error: function (result) {
                alert('failed to get questions');
            }
        });
    });
});
﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Recruitment.Domain.Shared;
using Recruitment.Domain.Shared.Contracts;
using Recruitment.Domain.Shared.Dto;
using Recruitment.Foundation.Common;
using Recruitment.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recruitment.Controllers
{
    public class AptitudeController : Controller
    {

        private readonly IUserManager _userManager;
        private readonly ICourseManager _courseManager;
        private readonly ICollegeManager _collegeManager;
        private readonly IAptitudeManager _testManager;
        private readonly ILoginManager _loginManager;

        public AptitudeController(ILoginManager loginManager, ICourseManager courseManager, ICollegeManager collegeManager, IUserManager userManager, IAptitudeManager testManager, IMapper mapper)
        {
            _courseManager = courseManager;
            _collegeManager = collegeManager;
            _userManager = userManager;
            _testManager = testManager;
            _loginManager = loginManager;
        }

        /// <summary>
        /// Test details regarding Aptitude Test
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public IActionResult TestDetails()
        {
            if (!IsLoggedIn())
            {
                return RedirectToAction(Constants.ViewNames.UserLogin, Constants.Controllernames.Login);
            }
            string savedId = HttpContext.Session.GetString(Constants.Keys.Id);
            Guid userId = Guid.Parse(savedId);
            int testId = Convert.ToInt32(TestTypes.AptitudeTestId);
            bool isAlreadyGiven = _userManager.CheckIsAlreadyGiven(userId, testId);

            if (isAlreadyGiven)
            {
                return View(Constants.ViewNames.AlreadyAttempted);
            }
            var response = _testManager.GetTestDuration();
            return View(response);
        }

        /// <summary>
        /// Post method to redirect to Actual Aptitude Test
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult TestDetails(string name)
        {
            return Redirect(Constants.ActionNames.Questions);
        }

        /// <summary>
        /// Aptitude Test View
        /// </summary>
        /// <returns>All the aptitude Questions</returns>
        [HttpGet]
        public IActionResult Questions()
        {
            if (!IsLoggedIn())
            {
                return RedirectToAction(Constants.ViewNames.UserLogin, Constants.Controllernames.Login);
            }
            string savedId = HttpContext.Session.GetString(Constants.Keys.Id);
            Guid userId = Guid.Parse(savedId);
            int testId = Convert.ToInt32(TestTypes.AptitudeTestId);
            bool isAlreadyGiven = _userManager.CheckIsAlreadyGiven(userId, testId);
            if (isAlreadyGiven)
            {
                return View(Constants.ViewNames.AlreadyAttempted);
            }
            _testManager.SaveUserAptitudeTestDetails(userId, testId);
            var response = _testManager.GetAptiQuestion();
            return View(response);
        }

        /// <summary>
        /// Aptitude Test Submit Method
        /// </summary>
        /// <param name="model">Model for answer provided by student</param>
        /// <returns>partial view if the student is not qualified or return true if qualified</returns>
        [HttpPost]
        public IActionResult Questions([FromBody] List<AptitudeUserQuestionAnswerModel> model)
        {
            string savedId = HttpContext.Session.GetString(Constants.Keys.Id);
            Guid userId = Guid.Parse(savedId);
            bool isQualified=_userManager.SaveAptitudeTestDetails(model, userId);
           
            if (isQualified)
            {
                return Json(true);
            }

            return PartialView(Constants.ViewNames._NotQualified);

        }

        [HttpGet]
        public int TestDuration()
        {
            var response = _testManager.GetTestDuration();
            return response.TestDuration;
        }

        public bool IsLoggedIn()
        {
            return _loginManager.IsLoggedIn(HttpContext.Session.GetString(Constants.Keys.IsLogged));
        }
    }
}

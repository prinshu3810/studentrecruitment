﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Recruitment.Domain.Shared.Contracts;
using Recruitment.Domain.Shared.Dto;
using Recruitment.Foundation.Common;
using Recruitment.Models.Request;
using Recruitment.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Controllers
{
    public class AdminController : Controller
    {
        private readonly ILoginManager _loginManager;
        private readonly IUserManager _userManager;
        private readonly ICourseManager _courseManager;
        private readonly ICollegeManager _collegeManager;
        private readonly ICodingManager _codingManager;
        private readonly IAptitudeManager _aptitudeManager;
        public AdminController(ILoginManager loginManager, ICourseManager courseManager, ICollegeManager collegeManager, IUserManager userManager, ICodingManager codingManager, IAptitudeManager aptitudeManager)
        {
            _loginManager = loginManager;
            _courseManager = courseManager;
            _collegeManager = collegeManager;
            _userManager = userManager;
            _codingManager = codingManager;
            _aptitudeManager = aptitudeManager;
        }

        [HttpGet]
        public IActionResult Report()
        {
            if (!IsLoggedIn())
            {
                return RedirectToAction(Constants.ViewNames.UserLogin, Constants.Controllernames.Login);
            }
            StudentReportModel response = new StudentReportModel();
            response.Colleges = _collegeManager.GetListedColleges(true);
            response.Courses = _courseManager.GetListedCourses();

            return View(response);
        }

        /// <summary>
        /// Function to filter student reports
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Partial View For student report</returns>
        [HttpPost]
        public IActionResult FilterReport([FromBody] AdminFilterReportRequestModel model)
        {
            List<StudentInfoModel> response = _userManager.GetStudentInfo();
            List<StudentInfoModel> filterStudents;

            if (model.Course != 0 && model.College != 0)
            {
                filterStudents = response.Where(z => z.CourseId == model.Course && z.CollegeId == model.College).ToList();
            }
            else if (model.College == 0 && model.Course != 0)
            {
                filterStudents = response.Where(z => z.CourseId == model.Course).ToList();
            }
            else if (model.College != 0 && model.Course == 0)
            {
                filterStudents = response.Where(z => z.CollegeId == model.College).ToList();
            }
            else
            {
                filterStudents = response;
            }

            return PartialView(Constants.ViewNames._FilterReport, filterStudents);

        }
        /// <summary>
        /// Function to get student Coding Data
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Student coding data</returns>
        [HttpPost]
        public List<StudentcodingDataResponseModel> StudentCodingData([FromBody] AdminStudentInfoRequestModel model)
        {
            Guid userId = Guid.Parse(model.Id);
            DateTime date = DateTime.Parse(model.Date);
            List<StudentcodingDataResponseModel> response = _codingManager.GetStudentCodingData(userId, date.Date); ;
            return response;

        }
        /// <summary>
        /// Function to update student coding marks on input provided by admin
        /// </summary>
        /// <param name="model"></param>
        /// <returns> true or false based on updated or not</returns>
        [HttpPost]
        public bool UpdateCodingMarks([FromBody] AdminUpdateMarksRequestModel model)
        {
            Guid userId = Guid.Parse(model.Id);
            DateTime date = DateTime.Parse(model.Date);
            bool response = _codingManager.UpdateMarks(userId, model.Marks, date.Date, model.Result);
            return response;
        }
        /// <summary>
        ///Student information view 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult StudentInformation()
        {
            if (!IsLoggedIn())
            {
                return RedirectToAction(Constants.ViewNames.UserLogin, Constants.Controllernames.Login);
            }
            return View();
        }

        /// <summary>
        /// function to get student Personal details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public StudentPersonalInfoModel PersonalDetails([FromBody] PersonalDetailsRequestModel model)
        {
            Guid userId = Guid.Parse(model.Id);
            var response = _userManager.GetPersonalDetails(userId);
            return response;
        }

        /// <summary>
        /// Function to get all attempted questions by a student
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult GetAttemptedQuestions([FromBody] AptitudeTestDetailsRequestModel model)
        {
            Guid userId = Guid.Parse(model.Id);
            DateTime date = DateTime.Parse(model.Date);
            var response = _userManager.AttemptedQuestions(userId, date);
            return PartialView(Constants.ViewNames._AptitudeTestReport, response);
        }
        /// <summary>
        /// Question bank View
        /// </summary>
        /// <returns></returns>
        public IActionResult QuestionBank()
        {
            if (!IsLoggedIn())
            {
                return RedirectToAction(Constants.ViewNames.UserLogin, Constants.Controllernames.Login);
            }

            return View();
        }
        /// <summary>
        /// Function to provide a partial viw to add a Question to Question Bank
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AptitudeQuestionAdd()
        {
            var response = _aptitudeManager.GetSection();
            return PartialView(Constants.ViewNames._AptitudeQuestionAdd, response);
        }

        /// <summary>
        /// function to add a question to  aptitude section
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        public void AptitudeAddSubmit([FromBody] AdminAptitudeAddQuestionRequestModel model)
        {
            _aptitudeManager.AddQuestion(model.SectionId, model.Options, model.QuestionContent, model.CorrectOption);
        }
        /// <summary>
        /// partial view for updating a section
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult UpdateAptitudeSection()
        {

            var response = _aptitudeManager.GetAllAptitudeSections();
            return PartialView(Constants.ViewNames._QuestionBankAptitudeSections, response);
        }
        /// <summary>
        /// partial view for updating a coding question
        /// </summary>
        /// <returns></returns>

        [HttpPost]
        public IActionResult UpdateCodingSection()
        {
            var response = _codingManager.GetAllCodingSections();
            return PartialView(Constants.ViewNames._QuestionBankCodingSections, response);
        }
        /// <summary>
        /// partial view for adding a coding question
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CodingQuestionAdd()
        {
            return PartialView(Constants.ViewNames._CodingQuestionAdd);
        }
        /// <summary>
        /// partial view for editing a aptitude question
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AptitudeQuestionEdit([FromBody] AdminQuestionEditRequestModel model)
        {
            var response = _aptitudeManager.GetQuestion(model.QuestionId);
            return PartialView(Constants.ViewNames._AptitudeQuestionEdit, response);

        }
        /// <summary>
        /// function to edit a aptitude question
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        public void AptitudeEditSubmit([FromBody] AptitudeQuestion model)
        {
            _aptitudeManager.EditQuestion(model);

        }
        /// <summary>
        /// function to add a coding question
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        public void CodingAddSubmit([FromBody] CodingQuestionInfo model)
        {
            _codingManager.AddQuestion(model);
        }
        /// <summary>
        /// Partial view to edit a coding question
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]

        public IActionResult CodingQuestionEdit([FromBody] AdminQuestionEditRequestModel model)
        {
            var response = _codingManager.GetQuestion(model.QuestionId);
            return PartialView(Constants.ViewNames._CodingQuestionEdit, response);
        }
        /// <summary>
        /// function to  edit a coding question
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        public void CodingEditSubmit([FromBody] CodingQuestionInfo model)
        {
            _codingManager.EditQuestion(model);

        }
        /// <summary>
        /// function to add a section in aptitude test
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        public void AddAptitudeSection([FromBody] AdminSectionAddRequestModel model)
        {
            _aptitudeManager.AddSection(model.SectionName);
        }
        /// <summary>
        /// function to add a section in coding test
        /// </summary>
        /// <param name="model"></param>
        public void AddCodingSection([FromBody] AdminSectionAddRequestModel model)
        {
            _codingManager.AddSection(model.SectionName);
        }
        /// <summary>
        /// Partial view to edit a section
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IActionResult EditSection([FromBody] AdminSectionEditRequestModel model)
        {
            var response = _aptitudeManager.GetSectionDetails(model.sectionId);
            return PartialView(Constants.ViewNames._AdminSectionEdit, response);
        }
        /// <summary>
        /// function to edit a section
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        public void EditSectionSubmit([FromBody] SectionFullDetailsResponseModel model)
        {
            _aptitudeManager.SaveSectionDetails(model);
        }

        public bool IsLoggedIn()
        {
            return _loginManager.IsLoggedIn(HttpContext.Session.GetString(Constants.Keys.IsAdminLogged));
        }

        public IActionResult CollegeInfo()
        {
            var response = _collegeManager.GetCollegeInfo();
            return View(response);
        }

        public IActionResult CollegeEdit([FromBody] CollegeEditRequestModel model)
        {
            var response = _collegeManager.GetCollege(model.CollegeId);
            return PartialView(Constants.ViewNames._CollegeEdit, response);
        }

        [HttpPost]
        public bool CollegeEditSubmit([FromBody] UpdateCollegeRequestModel model)
        {
            return _collegeManager.UpdateCollege(model.Id, model.Name, model.Cutoff, model.IsActive, model.ContactPerson, model.ContactPersonNumber, model.Address, model.State);
        }
        public IActionResult UpdateCollegeList()
        {
            var response = _collegeManager.GetCollegeInfo();
            return PartialView(Constants.ViewNames._CollegeInfo, response);
        }

        public IActionResult AddCollege()
        {
            return PartialView(Constants.ViewNames._AddCollege);
        }

        [HttpPost]
        public bool CollegeAddSubmit([FromBody] AddNewCollegeRequestModel model)
        {
            return _collegeManager.AddCollege(model.Name.Trim(), model.Cutoff, model.IsActive, model.ContactPerson.Trim(), model.ContactPersonNumber, model.Address.Trim(), model.State.Trim());

        }

        [HttpPost]
        public async Task RemoveStudent([FromBody] RemoveStudentRequestModel model)
        {
            Guid userId = Guid.Parse(model.UserId);

            await _userManager.RemoveStudent(userId);
        }
    }
}

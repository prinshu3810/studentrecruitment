﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using Recruitment.Domain.Shared.Contracts;
using Recruitment.Foundation.Common;
using Recruitment.Models.Request;
using Recruitment.Domain.Shared.Dto;
using Recruitment.Repository.Shared.Contracts;
using Newtonsoft.Json;
using Recruitment.Models.Response;

namespace Recruitment.Controllers
{
    public class LoginController : Controller
    {
        private readonly ILoginManager _loginManager;
        private readonly IUserManager _userManager;
        private readonly IStudentRepository _repor;

        public LoginController(ILoginManager loginManager, IUserManager userManager, IStudentRepository repor)
        {
            _loginManager = loginManager;
            _userManager = userManager;
            _repor = repor;
        }

        /// <summary>
        /// Landing page for Login
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public IActionResult UserLogin()
        {

            return View();
        }

        /// <summary>
        /// After submisssion of login form 
        /// </summary>
        /// <param name="model"></param>
        /// <returns>False if user not present or return partial view for otp if user is present</returns>
        [HttpPost]
        public IActionResult UserLogin([FromBody] LoginRequestModel model)
        {

            bool isPresent = _userManager.IsUserPresent(model.Input, model.flag); ;

            if (!isPresent)
            {
                return Json(false);

            }
            else
            {
                HttpContext.Session.SetString(Constants.Keys.LoginDetails, JsonConvert.SerializeObject(model));
                _loginManager.GenerateOtp(model.Input, model.flag);
                Guid id = _userManager.GetUserId(model.Input, model.flag);
                HttpContext.Session.SetString(Constants.Keys.Id, id.ToString());

                return PartialView(Constants.ViewNames._OtpAuthentication);
            }
        }

        /// <summary>
        /// Check whether the otp is right or not
        /// </summary>
        /// <param name="model"></param>
        /// <returns>return true if otp matches or else return false</returns>
        [HttpPost]
        public OtpVerificationResponseModel OtpAuthentication([FromBody] OtpRequestModel model)
        {
            string savedId = HttpContext.Session.GetString(Constants.Keys.Id);
            Guid userId = Guid.Parse(savedId);
            var response = _userManager.GetOtp(userId,model.Otp);

            if (response.IsVerified)
            {
                HttpContext.Session.SetString(Constants.Keys.IsLogged, Constants.Keys.Yes); 
            }
            return new OtpVerificationResponseModel()
            {
                IsVerified = response.IsVerified,
                IsExpired = response.IsExpired
            };
        }

        /// <summary>
        /// Login Page for admin
        /// </summary>
        /// <returns>Returns partial View for admin login</returns>
        [HttpGet]
        public IActionResult AdminLogin()
        {
            return PartialView(Constants.ViewNames._AdminLogin);
        }

        /// <summary>
        /// After Login Page submission
        /// </summary>
        /// <param name="model"></param>
        /// <returns>whether admin is present or not</returns>
        [HttpPost]
        public IActionResult AdminLogin([FromBody] AdminLoginRequestModel model)
        {
            AdminLoginResponseModel response = _userManager.IsAdminPresent(model.Username, model.Password);
            if (response.IsPresent == true && response.IsPasswordCorrect == true)
            {
                HttpContext.Session.SetString(Constants.Keys.IsAdminLogged, Constants.Keys.Yes);
            }
            return Json(response);
        }

        public void LoggedOut()
        {
            HttpContext.Session.SetString(Constants.Keys.IsAdminLogged, Constants.Keys.No);
            HttpContext.Session.SetString(Constants.Keys.IsLogged, Constants.Keys.No);
        }

        [HttpGet]
        public bool ResendOtp()
        {
            string email;
            var register = HttpContext.Session.GetString(Constants.Keys.Registration);
            if (register != null)
            {
                var user = JsonConvert.DeserializeObject<RegistrationRequestModel>(register);
                email = user.Email;

            }
            else
            {
                var login = HttpContext.Session.GetString(Constants.Keys.LoginDetails);
                var user = JsonConvert.DeserializeObject<LoginRequestModel>(login);
                email = user.Input;
            }
            _loginManager.GenerateOtp(email, 0);

            return true;

        }
    }
}

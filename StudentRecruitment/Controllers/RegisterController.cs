﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Recruitment.Domain.Shared.Contracts;
using Recruitment.Domain.Shared.Dto;
using Recruitment.Foundation.Common;
using Recruitment.Models;
using Recruitment.Models.Request;
using Recruitment.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Controllers
{
    public class RegisterController : Controller
    {
        private readonly ILoginManager _loginManager;
        private readonly IUserManager _userManager;
        private readonly ICourseManager _courseManager;
        private readonly ICollegeManager _collegeManager;

        public RegisterController(ILoginManager loginManager, ICourseManager courseManager, ICollegeManager collegeManager, IUserManager userManager)
        {
            _loginManager = loginManager;
            _courseManager = courseManager;
            _collegeManager = collegeManager;
            _userManager = userManager;
        }

        /// <summary>
        /// Page for Registration of users
        /// </summary>
        /// <returns>View for register page</returns>
        [HttpGet]
        public IActionResult Registration()
        {
            RegisterResponseViewModel registrationViewModel = new RegisterResponseViewModel();
            registrationViewModel.Colleges = _collegeManager.GetListedColleges(false);
            registrationViewModel.Courses = _courseManager.GetListedCourses();

            return View(registrationViewModel);
        }

        /// <summary>
        /// After submission of Register Page
        /// </summary>
        /// <param name="model"></param>
        /// <returns>return response based on whether user is already present or not</returns>
        [HttpPost]
        public IActionResult Registration([FromBody] RegistrationRequestModel model)
        {

            int flag = model.AuthenticateVia;
            var response = _userManager.IsUserPresent(model.PhoneNumber, model.Email);

            if (response)
            {
                return Json(response);
            }
            else
            {
                HttpContext.Session.SetString(Constants.Keys.Registration, JsonConvert.SerializeObject(model));
                Guid id = Guid.NewGuid();
                _userManager.SaveStudentData(model.Name, model.Password, model.PhoneNumber, model.AlternatePhoneNumber, model.CollegeId, model.CourseId, model.Email, model.RollNo, model.Semester, id);

                if (flag == 1)
                {
                    _loginManager.GenerateOtp(model.PhoneNumber, flag);
                }
                else
                {
                    _loginManager.GenerateOtp(model.Email, flag);
                }

                HttpContext.Session.SetString(Constants.Keys.Id, id.ToString());

                return PartialView(Constants.ViewNames._OtpAuthentication);
            }
        }
    }
}


﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Recruitment.CodeCompilation.Shared.Contracts;
using Recruitment.CodeCompilation.Shared.Dto;
using Recruitment.Domain.Shared;
using Recruitment.Domain.Shared.Contracts;
using Recruitment.Foundation.Common;
using Recruitment.Models.Request;
using Recruitment.Models.Response;
using System;
using System.Threading.Tasks;

namespace Recruitment.Controllers
{
    public class CodingController : Controller
    {
        private readonly IUserManager _userManager;
        private readonly ICodingManager _codingTestManager;
        private readonly ICodeCompiler _codeCompiler;
        private readonly ILoginManager _loginManager;

        public CodingController( IUserManager userManager, IAptitudeManager aptitudeTestManager, ICodingManager codingTestManager, ICodeCompiler codeCompiler,ILoginManager loginManager)
        {
            _userManager = userManager;
            _codingTestManager = codingTestManager;
            _codeCompiler = codeCompiler;
            _loginManager = loginManager;
        }

        /// <summary>
        /// Coding Test Details 
        /// </summary>
        /// <returns>View for details of Coding Test</returns>
        [HttpGet]
        public IActionResult TestDetails()
        {
            if (!IsLoggedIn())
            {
                return RedirectToAction(Constants.ViewNames.UserLogin, Constants.Controllernames.Login);
            }
            string savedId = HttpContext.Session.GetString(Constants.Keys.Id);
            Guid userId = Guid.Parse(savedId);
            bool isgiven = _codingTestManager.TestGiven(userId);

            if (isgiven)
            {
                return View(Constants.ViewNames.RedirectAlreadyAttempted);
            }
            var response = _codingTestManager.GetTestDuration();
            return View(response);
        }

        /// <summary>
        /// coding Test Post method for redirecting to Coding Test
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult TestDetails(string name)
        {
            return Redirect(Constants.ActionNames.Test);
        }

        /// <summary>
        /// Actual Coding Test
        /// </summary>
        /// <returns>View for Actual coding test containing the first coding question</returns>
        [HttpGet]
        public IActionResult Test()
        {
            if (!IsLoggedIn())
            {
                return RedirectToAction(Constants.ViewNames.UserLogin, Constants.Controllernames.Login);
            }
            string savedId = HttpContext.Session.GetString(Constants.Keys.Id);
            Guid userId = Guid.Parse(savedId);
            bool isgiven = _codingTestManager.TestGiven(userId);

            if (isgiven)
            {
                return View(Constants.ViewNames.RedirectAlreadyAttempted);
            }
            int UserTestId=_userManager.SaveUserCodingTestDetails(userId);
            CodingQuestionsResponseModel codingTestQuestionsViewModel = new CodingQuestionsResponseModel();
            var codingQuestion = _codingTestManager.GetCodingQuestion( userId);

            codingTestQuestionsViewModel.CodingQuestion = new CodingTestQuestionResponseModel()
            {
                QuestionContent = codingQuestion.QuestionContent,
                QuestionId = codingQuestion.QuestionId,
                QuestionType = codingQuestion.QuestionType,
                UserTestId=UserTestId
            };

            return View(codingTestQuestionsViewModel);
        }

        /// <summary>
        /// Actual Coding Test
        /// </summary>
        /// <param name="model">Request model for the code written by student to submit</param>
        /// <returns></returns>
        [HttpPost]
        public bool Test([FromBody] CodingSubmitRequestModel model)
        {
            string savedId = HttpContext.Session.GetString(Constants.Keys.Id);
            Guid userId = Guid.Parse(savedId);
            _userManager.SaveUserCode(userId, model.QuestionId, model.Usercode,model.UserTestId);

            return true;
        }

        /// <summary>
        /// View for Test Completion
        /// </summary>
        /// <returns>View for the test completion</returns>
        public IActionResult TestCompleted()
        {
            string isLoggedIn = HttpContext.Session.GetString(Constants.Keys.IsLogged);
            if (!IsLoggedIn())
            {
                return RedirectToAction(Constants.ViewNames.UserLogin, Constants.Controllernames.Login);
            }
            return View();
        }

        /// <summary>
        /// Partial View for changing Question
        /// </summary>
        /// <param name="Model">coding question change model</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CodingSection([FromBody] CodingQuestionChangeRequestModel Model)
        {
            if (Model.Usercode == null)
            {
                Model.Usercode = Constants.Keys.StringInitialisation;
            }

            string savedId = HttpContext.Session.GetString(Constants.Keys.Id);
            Guid userId = Guid.Parse(savedId);
            _userManager.SaveUserCode(userId,Model.QuestionId, Model.Usercode,Model.UserTestId);
            var codingQuestion = _codingTestManager.GetCodingQuestion(userId, Model.SelectedQuestion);
            CodingQuestionsResponseModel codingTestQuestionsViewModel = new CodingQuestionsResponseModel();

            codingTestQuestionsViewModel.CodingQuestion = new CodingTestQuestionResponseModel()
            {
                QuestionContent = codingQuestion.QuestionContent,
                QuestionId = codingQuestion.QuestionId,
                QuestionType = codingQuestion.QuestionType,
                CodeSubmitted=codingQuestion.CodeSubmitted
            };

            return PartialView(Constants.ViewNames._CodingSection, codingTestQuestionsViewModel);
        }

        /// <summary>
        /// Method for compiling User Code
        /// </summary>
        /// <param name="model">Model for code compilation</param>
        /// <returns></returns>
        [HttpPost]
        public Task<CodeCompilerApiResponseModel> CompileCode([FromBody] CompileRequestModel model)
        {
            return _codeCompiler.CompileCode(model.Script, model.Language);
            
        }

        [HttpGet]
        public int TestDuration()
        {
            var response = _codingTestManager.GetTestDuration();
            return response.TestDuration;
        }

        public bool IsLoggedIn()
        {
            return _loginManager.IsLoggedIn(HttpContext.Session.GetString(Constants.Keys.IsLogged));
        }
    }

}

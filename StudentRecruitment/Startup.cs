using AutoMapper;
using IRepository.Shared.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Recruitment.CodeCompilation.Implementataion.Concretes;
using Recruitment.CodeCompilation.Shared.Contracts;
using Recruitment.Domain.Implementation;
using Recruitment.Domain.Implementation.Concretes;
using Recruitment.Domain.Shared.Contracts;
using Recruitment.EmailGateway.Implementation.Concretes;
using Recruitment.EmailGateway.Shared.Contracts;
using Recruitment.Foundation.Common;
using Recruitment.Repository.Implementation.Concretes;
using Recruitment.Repository.Shared.Contracts;
using Recruitment.Repository.Shared.Entities;
using Recruitment.SMSGateway.Implementation.Concretes;
using Recruitment.SMSGateway.Implementation.SMSCredentials;
using Recruitment.SMSGateway.Shared.Contracts;
using System;

namespace Recruitment
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IConfigurationSection EmailSettings { get { return Configuration.GetSection("EmailSetting");  } }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapping());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.Configure<EmailOptions>(options =>
            {
                options.SenderEmailId = EmailSettings["EmailId"];
                options.ApiKey = EmailSettings["ApiKey"];
            });
            services.AddControllersWithViews();
            services.AddDbContext<StudentRecruitmentContext>(
            options => options.UseSqlServer(Configuration.GetConnectionString("RecruitmentDatabase")));
            var smsSettingSection = Configuration.GetSection("SmsSetting");
            services.Configure<SmsCredentials>(smsSettingSection);
            services.AddScoped<ILoginManager, LoginManager>();
            services.AddScoped<ICourseManager, CourseManager>();
            services.AddScoped<ICollegeManager, CollegeManager>();
            services.AddScoped<IUserManager, UserManager>();
            services.AddScoped<IAptitudeManager, AptitudeManager>();
            services.AddScoped<ICodingManager, CodingManager>();
            services.AddScoped<ICollegeInfoRepository, CollegeInfoRepository>();
            services.AddScoped<ICourseRepository, CourseRepository>();
            services.AddTransient<IEndUserRepository, EndUserRepository>();
            services.AddScoped<IQuestionOptionRepository, QuestionOptionRepository>();
            services.AddScoped<IQuestionRepository, QuestionRepository>();
            services.AddTransient<IStudentRepository, StudentRepository>();
            services.AddScoped<ITestRepository, TestRepository>();
            services.AddScoped<ISectionRepository, SectionRepository>();
            services.AddTransient<IUserQuestionAnswerRepository, UserQuestionAnswerRepository>();
            services.AddTransient<IUserTestRepository, UserTestRepository>();
            services.AddScoped<ISMSHelper, SMSHelper>();
            services.AddScoped<IEmailHelper, EmailHelper>();
            services.AddScoped<ICodeCompiler, CodeCompiler>();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(100);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Login}/{action=UserLogin}/{id?}");
            });
        }
    }
}

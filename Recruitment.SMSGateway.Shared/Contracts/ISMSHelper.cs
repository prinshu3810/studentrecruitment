﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.SMSGateway.Shared.Contracts
{
    public interface ISMSHelper
    {
        string SendOtp(string phoneNumber);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Foundation.Common
{
    public class Constants
    {
        public class ActionNames
        {
            public static string Registration = "Registration";
            public static string Questions = "Questions";
            public static string TestDetails = "TestDetails";
            public static string Test = "Test";
            public static string UserLogin = "UserLogin";

        }
        public class ViewNames
        {
            public static string AlreadyAttempted = "AlreadyAttempted";
            public static string RedirectAlreadyAttempted = "../Aptitude/AlreadyAttempted";
            public static string _NotQualified = "_NotQualified";
            public static string _CodingSection = "_CodingSection";
            public static string _OtpAuthentication = "_OtpAuthentication";
            public static string _AdminLogin = "_AdminLogin";
            public static string _FilterReport = "_FilterReport";
            public static string _AptitudeTestReport = "_AptitudeTestReport";
            public static string _AptitudeQuestionAdd = "_AptitudeQuestionAdd";
            public static string _QuestionBankAptitudeSections = "_QuestionBankAptitudeSections";
            public static string _CodingQuestionAdd = "_CodingQuestionAdd";
            public static string _QuestionBankCodingSections = "_QuestionBankCodingSections";
            public static string _AptitudeQuestionEdit = "_AptitudeQuestionEdit";
            public static string _CodingQuestionEdit = "_CodingQuestionEdit";
            public static string StudentInformation = "StudentInformation";
            public static string _AdminSectionEdit = "_AdminSectionEdit";
            public static string _CollegeEdit = "_CollegeEdit";
            public static string UserLogin = "UserLogin";
            public static string _CollegeInfo = "_CollegeInfo";
            public static string _AddCollege = "_AddCollege";
        }
        public class Controllernames
        {
            public static string Aptitude = "AptitudeController";
            public static string Coding = "CodingController";
            public static string Login = "Login";
            public static string Register = "Register";


        }
        public class SendGrid
        {
            public static string Name = "SendGridSetting";
            public static string ApiKey = "ApiKey";
        }
        public class DatabaseNames
        {
            public static string AptitudeTest = "Aptitude Test";
            public static string CodingTest = "Coding Test";
            public static string Coding = "Coding";

        }
        public class SmsSettings
        {
            public static string Name = "SmsSetting";
            public static string TwilioId = "TwilioId";
            public static string TwilioToken = "TwilioToken";
            public static string TwilioPhoneNumber = "TwilioPhoneNumber";
            public static string CountryCode = "+91";

        }
        public class EmailSettings
        {
            public static string Name = "EmailSetting";
            public static string EmailId = "EmailId";
            public static string Password = "Password";
            public static string ClientName = "ClientName";
            public static string PortNumber = "PortNumber";
            public static string Subject = "GrapeCity Recruitment-Email Verification";
            public static string Message = "Your OTP for authentication is";
            public static string HtmlPath = @"Templates\OTP.html";
        }
        public class CompilerSettings
        {
            public static string ClientId = "ClientId";
            public static string ClientSecret = "ClientSecret";
            public static string VersionIndex = "VersionIndex";
            public static string Name = "CompilerSettings";
            public static string Url = "https://api.jdoodle.com/v1/execute";
            public static string Method = "POST";
        }
        public class RandomNumberGenerator
        {
            public static int InitialNumber = 100000;
            public static int FinalNumber = 1000000;

        }
        public class ApiRequest
        {
            public static string ContentType = "application/json";
        }
        public class Keys
        {
            public static string Id = "Id";
            public static string StringInitialisation = "";
            public static string IsLogged = "IsLogged";
            public static string IsAdminLogged = "IsAdminLogged";
            public static string Yes = "Yes";
            public static string No = "No";
            public static int NumberOfQuestions = 10;
            public static string LoginDetails = "LoginDetails"; 
            public static string Registration = "Registration";
            public static int OtpExpire = 300;

        }
    }
}

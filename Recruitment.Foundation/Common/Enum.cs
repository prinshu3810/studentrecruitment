﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Foundation.Common
{
    public enum TestTypes
    {
        AptitudeTestId = 1,
        CodingTestId=2,
    }
    public enum SectionId
    {
        OOPS=1,
        Logical=2,
        Language=3,
        Programming=4
    }
    public enum Marks
    {
        AptitudeQuestions=1
    }
}

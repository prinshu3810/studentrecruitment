﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Foundation.Common
{
    public class EmailOptions
    {
        public string SenderEmailId { get; set; }
        public string ApiKey { get; set; }
    }
}

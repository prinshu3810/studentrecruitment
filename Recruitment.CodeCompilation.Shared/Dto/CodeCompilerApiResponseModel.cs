﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.CodeCompilation.Shared.Dto
{
    public class CodeCompilerApiResponseModel
    {
        public string Output { get; set; }
        public int StatusCode { get; set; }
    }
    public class CodeCompilerResponseModel
    {
        public string output { get; set; }
        public int statusCode { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.CodeCompilation.Shared.Dto
{
    public class CompileClientRequestModel
    {
        public string clientId { get; set; }
        public string versionIndex { get; set; }
        public string clientSecret { get; set; }
        public string language { get; set; }
        public string script { get; set; }
    }
}

﻿using Recruitment.CodeCompilation.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.CodeCompilation.Shared.Contracts
{
    public interface ICodeCompiler
    {
        Task<CodeCompilerApiResponseModel> CompileCode(string code, string language);
    }
}

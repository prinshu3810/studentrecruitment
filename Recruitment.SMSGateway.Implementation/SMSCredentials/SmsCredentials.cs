﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.SMSGateway.Implementation.SMSCredentials
{
    public class SmsCredentials
    {
        private readonly IConfiguration _configuration;
        public SmsCredentials(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public static string TWILIO_ACCOUNT_SID { get; set; } 
        public string TWILIO_AUTH_TOKEN { get; set; }
    }
}

﻿using Recruitment.SMSGateway.Shared.Contracts;
using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Microsoft.Extensions.Configuration;
using Recruitment.Foundation.Common;

namespace Recruitment.SMSGateway.Implementation.Concretes
{
    public class SMSHelper:ISMSHelper
    {
        private readonly IConfiguration _configuration;
        public SMSHelper(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public string SendOtp(string phoneNumber)
        {
          
            string twilioAccountId = _configuration.GetSection(Constants.SmsSettings.Name).GetSection(Constants.SmsSettings.TwilioId).Value;
            string  twilioAuthToken= _configuration.GetSection(Constants.SmsSettings.Name).GetSection(Constants.SmsSettings.TwilioToken).Value;
            string twilioPhoneNumber = _configuration.GetSection(Constants.SmsSettings.Name).GetSection(Constants.SmsSettings.TwilioPhoneNumber).Value;
            Random generator = new Random();
            int r = generator.Next(Constants.RandomNumberGenerator.InitialNumber,Constants.RandomNumberGenerator.FinalNumber);
            string otp = r.ToString();
            string accountSid = twilioAccountId;
            string authToken = twilioAuthToken;
            string modifiedPhoneNumber = Constants.SmsSettings.CountryCode+ phoneNumber;

            TwilioClient.Init(accountSid, authToken);

            var message = MessageResource.Create(
                body: otp,
                from: new Twilio.Types.PhoneNumber(twilioPhoneNumber),
                to: new Twilio.Types.PhoneNumber(modifiedPhoneNumber)
            );
            return otp;
        }
    }
}

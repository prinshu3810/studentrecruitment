﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Recruitment.EmailGateway.Shared.Contracts;
using Recruitment.Foundation.Common;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Recruitment.EmailGateway.Implementation.Concretes
{
    public class EmailHelper : IEmailHelper
    {
        private readonly IConfiguration _configuration;
        private readonly EmailOptions _emailOptions;
        public EmailHelper(IConfiguration configuration,IOptions<EmailOptions> emailOptions)
        {
            _configuration = configuration;
            _emailOptions = emailOptions.Value;
        }
        public string SendEmail(string emailId)
        {

            string senderEmailId = _configuration.GetSection(Constants.EmailSettings.Name).GetSection(Constants.EmailSettings.EmailId).Value;
            string password = _configuration.GetSection(Constants.EmailSettings.Name).GetSection(Constants.EmailSettings.Password).Value;
            string clientName = _configuration.GetSection(Constants.EmailSettings.Name).GetSection(Constants.EmailSettings.ClientName).Value;
            string portNumber = _configuration.GetSection(Constants.EmailSettings.Name).GetSection(Constants.EmailSettings.PortNumber).Value;
            string subject = Constants.EmailSettings.Subject;
            string Message = Constants.EmailSettings.Message;
            int port = Convert.ToInt32(portNumber);

            Random generator = new Random();
            int r = generator.Next(Constants.RandomNumberGenerator.InitialNumber, Constants.RandomNumberGenerator.FinalNumber);
            string otp = r.ToString();


            var client = new SmtpClient(clientName, port)
            {
                Credentials = new NetworkCredential(senderEmailId, password),
                EnableSsl = true
            };


            client.Send(senderEmailId, emailId, subject, Message + otp);
            return otp;
        }
        public async Task<string> SendEmailSendGridAsync(string emailId)
        {
            string mailText = string.Empty;
            Random generator = new Random(); int r = generator.Next(Constants.RandomNumberGenerator.InitialNumber, Constants.RandomNumberGenerator.FinalNumber);
            string otp = r.ToString();
            string senderEmailId = _emailOptions.SenderEmailId;
            var apiKey = _emailOptions.ApiKey;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(senderEmailId);
            var subject = Constants.EmailSettings.Subject;
            var to = new EmailAddress(emailId); var plainTextContent = "";
            var message=  "<div><p>Dear Candidate,</p><p> Your One Time Password is {0}.</p><p>Use this OTP to verify your Email on GrapeCity Recruitment Portal.</p><br> <p>Thank you,</p><p>GrapeCity Recruitment Team</p>";
            var htmlContent = String.Format(message, otp);
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            client.SendEmailAsync(msg).ConfigureAwait(false);

            return otp;
        }
    }
}

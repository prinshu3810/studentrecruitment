﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Domain.Shared.Dto
{
    public class AptitudeOption
    {
        public int OptionId { get; set; }
        public string Option { get; set; }
        public bool IsCorrect { get; set; }
        public bool IsActive { get; set; }
    }
}

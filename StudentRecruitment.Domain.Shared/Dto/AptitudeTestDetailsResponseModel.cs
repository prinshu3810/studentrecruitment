﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Domain.Shared.Dto
{
    public class AptitudeTestDetailsResponseModel
    {
        public int TestDuration { get; set; }
    }
}

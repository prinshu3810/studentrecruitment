﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Domain.Shared.Dto
{
    public class RegistrationResponseModel
    {
        public bool IsUserPresent { get; set; }
        public bool IsPhoneNumberPresent { get; set; }
        public bool IsEmailPresent { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Domain.Shared.Dto
{
    public class AdminLoginResponseModel
    {
        public bool IsPresent { get; set; }
        public bool IsPasswordCorrect { get; set; }
    }
}

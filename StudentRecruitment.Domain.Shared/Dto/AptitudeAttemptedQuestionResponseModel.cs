﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class AptitudeAttemptedQuestionResponseModel
    {
        public List<AptitudeInfoQuestion> Section { get; set; }
        public string SectionName { get; set; }
        public string TrimmedSectionName { get; set; }
        public int SectionMarks { get; set; }
        public int SectionTotalMarks { get; set; }
    }
}

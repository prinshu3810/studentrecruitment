﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class CollegeInfoResponseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int Cutoff { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonNumber  { get; set; }
        public string Address { get; set; }
        public string State { get; set; }

    }
}

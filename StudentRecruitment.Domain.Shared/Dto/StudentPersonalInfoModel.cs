﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class StudentPersonalInfoModel
    {
        public string Name { get; set; }
        public string College { get; set; }
        public string Course { get; set; }
        public string PhoneNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }

    }
}

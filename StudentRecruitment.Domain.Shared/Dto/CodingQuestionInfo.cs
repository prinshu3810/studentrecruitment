﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class CodingQuestionInfo
    {
        public string QuestionContent { get; set; }
        public int QuestionId { get; set; }
        public int SectionId { get; set; }
        public int QuestionMarks { get; set; }
        public bool IsActive { get; set; }

    }
}

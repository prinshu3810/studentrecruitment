﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class SectionFullDetailsResponseModel
    {
        public int SectionId { get; set; }
        public string SectionName { get; set; }
        public int QuestionCount { get; set; }
        public bool IsActive { get; set; }
        public int ActiveQuestions { get; set; }


    }
}

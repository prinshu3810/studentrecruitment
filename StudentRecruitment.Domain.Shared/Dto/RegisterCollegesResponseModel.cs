﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Domain.Shared.Dto
{
    public class RegisterCollegesResponseModel
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}

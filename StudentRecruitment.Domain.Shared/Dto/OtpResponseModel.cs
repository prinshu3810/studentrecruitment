﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class OtpResponseModel
    {
        public bool IsVerified { get; set; }
        public bool IsExpired { get; set; }
    }
}

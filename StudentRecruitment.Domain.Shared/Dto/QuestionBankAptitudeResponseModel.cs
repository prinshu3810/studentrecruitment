﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class QuestionBankAptitudeResponseModel
    {
        public List<AptitudeInfoQuestion> SectionQuestions { get; set; }
        public string SectionName { get; set; }
        public int SectionId { get; set; }

        public string TrimmedSectionName { get; set; }
        
    }
}

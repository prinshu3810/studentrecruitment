﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Domain.Shared.Dto
{
    public class AptitudeUserQuestionAnswerModel
    {
        public int QuestionId { get; set; }
        public int? OptionId { get; set; }
    }
}

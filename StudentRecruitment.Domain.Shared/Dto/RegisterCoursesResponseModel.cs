﻿namespace Recruitment.Domain.Shared.Dto
{
    public class RegisterCoursesResponseModel
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}

﻿namespace Recruitment.Domain.Shared.Dto
{
    public class CodingQuestion
    {
        public string QuestionContent { get; set; }
        public int QuestionId { get; set; }
        public string QuestionType { get; set; }
        public string CodeSubmitted { get; set; }
    }
}

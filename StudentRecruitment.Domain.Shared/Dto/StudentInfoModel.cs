﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class StudentInfoModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Course { get; set; }
        public string College { get; set; }
        public int AptiMarks { get; set; }
        public int CodingMarks { get; set; }
        public DateTime Date { get; set; }
        public int CollegeId { get; set; }
        public int CourseId { get; set; }
        public string PhoneNumber { get; set; }
        public bool? AptiStatus { get; set; }
        public bool? CodingStatus { get; set; }
        public string Email { get; set; }
    }
}

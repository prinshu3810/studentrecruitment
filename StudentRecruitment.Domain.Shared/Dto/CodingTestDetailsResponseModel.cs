﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class CodingTestDetailsResponseModel
    {
        public int TestDuration { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class StudentcodingDataResponseModel
    {
        public string Question { get; set; }
        public string Response { get; set; }
        public int QuestionMarks { get; set; }
        public int? Marks { get; set; }
        public bool? CodingStatus { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class TestSectionResponseModel
    {
        public int sectionId { get; set; }
        public string sectionName { get; set; }
    }
}

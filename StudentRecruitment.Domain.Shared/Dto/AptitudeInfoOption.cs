﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class AptitudeInfoOption
    {
        public int OptionId { get; set; }
        public string Option { get; set; }
        public bool? IsTrue { get; set; }
        public bool? IsActive { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Domain.Shared.Dto
{
    public class AptitudeQuestion
    {
        public int QuestionId { get; set; }
        public string QuestionContent { get; set; }
        public List<AptitudeOption> Options { get; set; } = new List<AptitudeOption>();
        public int SectionId { get; set; }
        public bool IsActive { get; set; }
    }
}

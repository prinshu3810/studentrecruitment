﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class AptitudeInfoQuestion
    {
        public int QuestionId { get; set; }
        public string QuestionContent { get; set; }
        public List<AptitudeInfoOption> Options { get; set; } = new List<AptitudeInfoOption>();
        public int SectionId { get; set; }
        public int? UserOptionId { get; set; }
        public bool IsActive { get; set; }
    }
}

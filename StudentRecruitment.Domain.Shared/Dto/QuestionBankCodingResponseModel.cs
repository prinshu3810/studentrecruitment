﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Domain.Shared.Dto
{
    public class QuestionBankCodingResponseModel
    {
        public List<CodingQuestionInfo> SectionQuestions { get; set; }
        public string SectionName { get; set; }
        public int SectionId { get; set; }

        public string TrimmedSectionName { get; set; }

    }
}

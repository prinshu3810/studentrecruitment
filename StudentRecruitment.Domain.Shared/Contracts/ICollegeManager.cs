﻿using Recruitment.Domain.Shared.Dto;
using System.Collections.Generic;

namespace Recruitment.Domain.Shared.Contracts
{
    public interface ICollegeManager
    {
        List<RegisterCollegesResponseModel> GetListedColleges(bool flag);
        List<CollegeInfoResponseModel> GetCollegeInfo();
        CollegeInfoResponseModel GetCollege(int Id);
        bool UpdateCollege(int id,string name, int cutOff, bool isActve, string contactPerson, string contactNumber, string address, string state);
        bool AddCollege(string name, int cutoff, bool isActive, string contactPerson, string contactNumber,string address,string state);
    }
}

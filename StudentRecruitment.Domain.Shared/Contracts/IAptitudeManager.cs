﻿using Recruitment.Domain.Shared.Dto;
using System;
using System.Collections.Generic;

namespace Recruitment.Domain.Shared.Contracts
{
    public interface IAptitudeManager
    {
        List<AptitudeSectionResponseModel> GetAptiQuestion();
        void SaveUserAptitudeTestDetails(Guid userId, int testId);
        List<QuestionBankAptitudeResponseModel> GetAllAptitudeSections();
        List<TestSectionResponseModel> GetSection();
        void AddQuestion(int sectionId, List<string> options, string questionContent, string correctOption);
        AptitudeQuestion GetQuestion(int questionId);
        void EditQuestion(AptitudeQuestion model);
        void AddSection(string sectionName);
        SectionFullDetailsResponseModel GetSectionDetails(int sectionId);
        void SaveSectionDetails(SectionFullDetailsResponseModel model);
        AptitudeTestDetailsResponseModel GetTestDuration();
    }
}

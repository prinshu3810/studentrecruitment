﻿namespace Recruitment.Domain.Shared.Contracts
{
    public interface ILoginManager
    {
        void GenerateOtp(string PhoneNumber, int flag);
        bool IsLoggedIn(string loggedIn);

    }
}

﻿using Recruitment.Domain.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Domain.Shared.Contracts
{
    public interface ICourseManager
    {
        List<RegisterCoursesResponseModel> GetListedCourses();
    }
}

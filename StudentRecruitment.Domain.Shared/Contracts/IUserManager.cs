﻿using Recruitment.Domain.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Domain.Shared.Contracts
{
    public interface IUserManager
    {
        Guid GetUserId(string phoneNo, int flag);
        bool IsUserPresent(string input, int flag);
        bool IsUserPresent(string phoneNumber, string email);
        void SaveStudentData(string Name, string password, string phoneNo, string alternatePhoneNo, int collegeId, int courseId, string email, string rollNo, int semester, Guid id);
        OtpResponseModel GetOtp(Guid id, string otp);
        bool CheckIsAlreadyGiven(Guid UserId, int testId);
        bool IsQualified(Guid userId);
        int SaveUserCodingTestDetails(Guid userId);
        void SaveUserCode(Guid userId, int questionId, string code, int userTestId);
        AdminLoginResponseModel IsAdminPresent(string username, string password);
        List<StudentInfoModel> GetStudentInfo();
        StudentPersonalInfoModel GetPersonalDetails(Guid userId);
        List<AptitudeAttemptedQuestionResponseModel> AttemptedQuestions(Guid userId, DateTime dateGiven);
        bool SaveAptitudeTestDetails(List<AptitudeUserQuestionAnswerModel> aptitudeTestUserQuestionAnswerModel, Guid UserId);
        Task RemoveStudent(Guid userId);
    }
}

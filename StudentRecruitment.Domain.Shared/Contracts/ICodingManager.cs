﻿using Recruitment.Domain.Shared.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Domain.Shared.Contracts
{
    public interface ICodingManager
    {
        CodingQuestion GetCodingQuestion(Guid userId, int index = 1);
        bool TestGiven(Guid userId);
        List<StudentcodingDataResponseModel> GetStudentCodingData(Guid userId,DateTime date);
        bool UpdateMarks(Guid userId, int marks,DateTime date,int result);
        List<QuestionBankCodingResponseModel> GetAllCodingSections();
        void AddQuestion(CodingQuestionInfo model);
        CodingQuestionInfo GetQuestion(int questionId);
        void EditQuestion(CodingQuestionInfo model);
        void AddSection(string sectionName);
        CodingTestDetailsResponseModel GetTestDuration();
    }
}

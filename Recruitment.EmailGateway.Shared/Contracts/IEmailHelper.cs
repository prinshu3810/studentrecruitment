﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.EmailGateway.Shared.Contracts
{
    public interface IEmailHelper
    {
        string SendEmail(string emailId);
        Task<string> SendEmailSendGridAsync(string emailId);
    }
}

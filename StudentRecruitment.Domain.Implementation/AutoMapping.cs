﻿using AutoMapper;
using Recruitment.Domain.Shared.Dto;
using Recruitment.Repository.Shared.Dto;

namespace Recruitment.Domain.Implementation
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<AptitudeOptionRepo, AptitudeOption>();
            CreateMap<AptitudeQuestionRepo, AptitudeQuestion>();
            CreateMap<StudentInfoRepoModel, StudentInfoModel>();
            CreateMap<StudentPersonalInfoRepoModel, StudentPersonalInfoModel>();
            CreateMap<AptitudeOptionRepo, AptitudeOption>();
            CreateMap<AptitudeQuestionRepo, AptitudeQuestion>();
        }
    }
}

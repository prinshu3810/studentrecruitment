﻿using Recruitment.Domain.Shared;
using Recruitment.Domain.Shared.Contracts;
using Recruitment.EmailGateway.Shared.Contracts;
using Recruitment.Foundation.Common;
using Recruitment.Repository.Shared.Contracts;
using Recruitment.SMSGateway.Shared.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Domain.Implementation
{

    public class LoginManager : ILoginManager
    {
        IEmailHelper _emailHelper;
        ISMSHelper _smsHelper;
        IEndUserRepository _endUserRepository;

        public LoginManager(ISMSHelper smsHelper, IEndUserRepository endUserRepository, IEmailHelper emailHelper)
        {
            _smsHelper = smsHelper;
            _endUserRepository = endUserRepository;
            _emailHelper = emailHelper;
        }

        /// <summary>
        /// function to generate otp based on input provided by the user
        /// </summary>
        /// <param name="input">user input</param>
        /// <param name="flag">flag for otp on mobile and email 0 for email 1 for mobile</param>
        public void GenerateOtp(string input, int flag)
        {
            if (flag == 1)
            {
                var endUser = _endUserRepository.GetByPhonenumber(input);

                if (endUser != null)
                {
                    string otp = _smsHelper.SendOtp(input);
                    endUser.Otp = otp;
                    _endUserRepository.Update(endUser);
                }
            }
            else
            {
                var endUser = _endUserRepository.GetByEmail(input);

                if (endUser != null)
                {
                    Task<string> res= _emailHelper.SendEmailSendGridAsync(input);
                    string otp = res.Result;
                    endUser.Otp = otp;
                    endUser.OtpCreated = DateTime.Now;
                    _endUserRepository.Update(endUser);
                }
            }
        }

        public bool IsLoggedIn(string loggedIn)
        {
            return loggedIn == Constants.Keys.Yes;
        }

    }
}

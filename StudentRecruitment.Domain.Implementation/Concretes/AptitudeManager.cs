﻿using AutoMapper;
using Recruitment.Domain.Shared.Contracts;
using Recruitment.Domain.Shared.Dto;
using Recruitment.Foundation.Common;
using Recruitment.Repository.Shared.Contracts;
using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recruitment.Domain.Implementation.Concretes
{
    public class AptitudeManager : IAptitudeManager
    {
        private readonly IQuestionRepository _questionRepository;
        private readonly IUserTestRepository _userTestRepository;
        private readonly IMapper _mapper;
        private readonly ISectionRepository _sectionRepository;
        private readonly IQuestionOptionRepository _questionOption;
        private readonly ITestRepository _testRepository;

        public AptitudeManager(IQuestionRepository questionRepository, IUserTestRepository userTestRepository, IMapper mapper, ISectionRepository sectionRepository, IQuestionOptionRepository questionOption, ITestRepository testRepository)
        {
            _questionRepository = questionRepository;
            _userTestRepository = userTestRepository;
            _mapper = mapper;
            _sectionRepository = sectionRepository;
            _questionOption = questionOption;
            _testRepository = testRepository;
        }

        /// <summary>
        /// Function for gettting Random aptitude Questions
        /// </summary>
        /// <returns>Random Apti Questions</returns>
        public List<AptitudeSectionResponseModel> GetAptiQuestion()
        {
            List<AptitudeSectionResponseModel> response = new List<AptitudeSectionResponseModel>();
            var allAptitudeSections = _sectionRepository.GetDetailsByTestId((int)TestTypes.AptitudeTestId);
            var activeSections = allAptitudeSections.Where(sec => sec.IsActive == true).ToList();
            foreach (var sec in activeSections)
            {
                List<AptitudeInfoQuestion> section = new List<AptitudeInfoQuestion>();
                var totalQuestions = sec.Questions;
                var activeQuestion = totalQuestions.Where(q => q.IsActive == true).ToList();
                var randomList = GenerateRondomQuestions(activeQuestion, sec.QuestionCount);
                List<Question> randomQuestions = new List<Question>();
                foreach (int i in randomList)
                {
                    randomQuestions.Add(activeQuestion[i]);
                }
                foreach (var q in randomQuestions)
                {

                    var questionOptions =q.QuestionOption.Where(o=>o.IsActive).ToList();
                    var options = questionOptions.Select(op => new AptitudeInfoOption()
                    {
                        Option = op.QuestionOption1,
                        OptionId = op.Id,
                        IsActive = op.IsActive,
                        IsTrue = op.IsCorrect
                    }).ToList();
                    section.Add(new AptitudeInfoQuestion()
                    {
                        QuestionContent = q.QuestionContent,
                        QuestionId = q.Id,
                        Options = options,
                        SectionId = q.SectionId,
                        IsActive = q.IsActive
                    });
                }
                response.Add(new AptitudeSectionResponseModel()
                {
                    SectionName = sec.SectionName,
                    Section = section,
                    TrimmedSectionName = String.Concat(sec.SectionName.Where(c => !Char.IsWhiteSpace(c)))
                });

            }

            return response;
        }

        public List<int> GenerateRondomQuestions(List<Question> list, int num)
        {
            List<int> listNumbers = new List<int>();
            Random rand = new Random();
            int number;
            for (int i = 0; i < num; i++)
            {
                do
                {
                    number = rand.Next(0, list.Count);
                } while (listNumbers.Contains(number));
                listNumbers.Add(number);
            }

            return listNumbers;

        }
        /// <summary>
        /// Save User test details
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="testId"></param>
        public void SaveUserAptitudeTestDetails(Guid userId, int testId)
        {
            UserTest userTest = new UserTest()
            {
                UserId = userId,
                TestId = testId,
                Marks = 0,
                DateGiven = DateTime.Today,
            };
            _userTestRepository.Add(userTest);
        }

        public List<QuestionBankAptitudeResponseModel> GetAllAptitudeSections()
        {
            List<QuestionBankAptitudeResponseModel> response = new List<QuestionBankAptitudeResponseModel>();
            var aptitudeSections = _sectionRepository.GetDetailsByTestId((int)TestTypes.AptitudeTestId);

            foreach (var sec in aptitudeSections)
            {
                List<AptitudeInfoQuestion> section = new List<AptitudeInfoQuestion>();
                var questions =sec.Questions;
                foreach (var q in questions)
                {

                    var questionOptions = q.QuestionOption.Where(q=>q.IsActive).ToList();
                    var options = questionOptions.Select(op => new AptitudeInfoOption()
                    {
                        Option = op.QuestionOption1,
                        OptionId = op.Id,
                        IsActive = op.IsActive,
                        IsTrue = op.IsCorrect
                    }).ToList();
                    section.Add(new AptitudeInfoQuestion()
                    {
                        QuestionContent = q.QuestionContent,
                        QuestionId = q.Id,
                        Options = options,
                        SectionId = q.SectionId,
                        IsActive = q.IsActive
                    });
                }
                response.Add(new QuestionBankAptitudeResponseModel()
                {
                    SectionName = sec.SectionName,
                    SectionQuestions = section,
                    SectionId = sec.Id,
                    TrimmedSectionName=String.Concat(sec.SectionName.Where(c=> !Char.IsWhiteSpace(c)))
                });

            }

            return response;
        }

        public List<TestSectionResponseModel> GetSection()
        {
            var sections = _sectionRepository.GetByTestId((int)TestTypes.AptitudeTestId);
            return sections.Select(sec => new TestSectionResponseModel()
            {
                sectionId = sec.Id,
                sectionName = sec.SectionName
            }).ToList();
        }

        public void AddQuestion(int sectionId, List<string> options, string questionContent, string correctOption)
        {
            var isCorrect = false;
            Question question = new Question()
            {
                QuestionContent = questionContent,
                SectionId = sectionId,
                IsActive = true,
                Marks = (int)Marks.AptitudeQuestions,
            };
            _questionRepository.Add(question);
            var ques = _questionRepository.GetByQuestionContent(questionContent);
            int quesId = ques.Id;
            for (int i = 0; i < options.Count; i++)
            {
                if (options[i] == correctOption)
                {
                    isCorrect = true;
                }
                QuestionOption option = new QuestionOption()
                {
                    QuestionOption1 = options[i],
                    IsActive = true,
                    IsCorrect = isCorrect,
                    QuestionId = quesId

                };
                _questionOption.Add(option);
                isCorrect = false;
            }
        }

        public AptitudeQuestion GetQuestion(int questionId)
        {
            var question = _questionRepository.GetById(questionId);
            var options = _questionOption.GetByQuestionId(questionId);
            return new AptitudeQuestion()
            {
                QuestionId = question.Id,
                QuestionContent = question.QuestionContent,
                SectionId = question.SectionId,
                Options = options.Select(opt => new AptitudeOption()
                {
                    Option = opt.QuestionOption1,
                    OptionId = opt.Id,
                    IsActive = opt.IsActive,
                    IsCorrect = opt.IsCorrect
                }).ToList(),
                IsActive = question.IsActive
            };
        }
        public void EditQuestion(AptitudeQuestion model)
        {
            var question = _questionRepository.GetById(model.QuestionId);
            question.QuestionContent = model.QuestionContent;
            question.IsActive = model.IsActive;
            _questionRepository.Update(question);
            var options = _questionOption.GetByQuestionId(model.QuestionId);

            foreach (var opt in options)
            {
                var option = model.Options.FirstOrDefault(op => op.OptionId == opt.Id);
                if (option != null)
                {
                    opt.QuestionOption1 = option.Option;
                    opt.IsCorrect = option.IsCorrect;
                    opt.IsActive = true;
                    _questionOption.Update(opt);
                }
                else
                {
                    opt.IsActive = false;
                    _questionOption.Update(opt);
                }
            }
            var newOptions = model.Options.Where(np => np.OptionId == 0).ToList();
            foreach (var nopt in newOptions)
            {
                _questionOption.Add(new QuestionOption()
                {
                    QuestionOption1 = nopt.Option,
                    QuestionId = model.QuestionId,
                    IsActive = true,
                    IsCorrect = nopt.IsCorrect,

                });

            }
        }

        public void AddSection(string sectionName)
        {
            _sectionRepository.Add(new Section()
            {
                SectionName = sectionName,
                QuestionCount = 0,
                TestId = (int)TestTypes.AptitudeTestId,
                IsActive = false
            });
        }

        public SectionFullDetailsResponseModel GetSectionDetails(int sectionId)
        {
            int activeQuestions = 0;
            var section = _sectionRepository.GetById(sectionId);
            var question = _questionRepository.GetBySectionId(sectionId);
            if (question != null)
            {
                activeQuestions = (question.Where(q => q.IsActive == true).ToList()).Count;
            }
            return new SectionFullDetailsResponseModel()
            {
                SectionId = section.Id,
                IsActive = section.IsActive,
                SectionName = section.SectionName,
                QuestionCount = section.QuestionCount,
                ActiveQuestions = activeQuestions
            };
        }

        public void SaveSectionDetails(SectionFullDetailsResponseModel model)
        {
            var section = _sectionRepository.GetById(model.SectionId);
            section.IsActive = model.IsActive;
            section.SectionName = model.SectionName;
            section.QuestionCount = model.QuestionCount;
            _sectionRepository.Update(section);
        }

        public AptitudeTestDetailsResponseModel GetTestDuration()
        {
            var test = _testRepository.GetById((int)TestTypes.AptitudeTestId);
            return new AptitudeTestDetailsResponseModel()
            {
                TestDuration = test.Duration
            };
        }

    }
}


﻿using AutoMapper;
using IRepository.Shared.Contracts;
using Recruitment.Domain.Shared.Contracts;
using Recruitment.Domain.Shared.Dto;
using Recruitment.Foundation.Common;
using Recruitment.Repository.Shared.Contracts;
using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Domain.Implementation
{
    public class UserManager : IUserManager
    {

        private readonly IEndUserRepository _endUserRepository;
        private readonly ICollegeInfoRepository _collegeInfoRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IUserTestRepository _userTestRepository;
        private readonly ITestRepository _testRepository;
        private readonly IQuestionRepository _questionRepository;
        private readonly IUserQuestionAnswerRepository _userQuestionAnswerRepository;
        private readonly IMapper _mapper;
        private readonly IQuestionOptionRepository _questionOptionRepository;
        private readonly ISectionRepository _sectionRepository;


        public UserManager(IEndUserRepository endUserRepository, ICollegeInfoRepository collegeInfoRepository, ICourseRepository courseRepository, IStudentRepository studentRepository, IUserTestRepository userTestRepository, ITestRepository testRepository, IQuestionRepository questionRepository, IUserQuestionAnswerRepository userQuestionAnswerRepository, IMapper mapper, IQuestionOptionRepository questionOptionRepository, ISectionRepository sectionRepository, StudentRecruitmentContext context)
        {
            _endUserRepository = endUserRepository;
            _collegeInfoRepository = collegeInfoRepository;
            _courseRepository = courseRepository;
            _studentRepository = studentRepository;
            _userTestRepository = userTestRepository;
            _testRepository = testRepository;
            _questionRepository = questionRepository;
            _userQuestionAnswerRepository = userQuestionAnswerRepository;
            _mapper = mapper;
            _questionOptionRepository = questionOptionRepository;
            _sectionRepository = sectionRepository;
        }

        /// <summary>
        /// Function to check if user is present or not
        /// </summary>
        /// <param name="input">userinput</param>
        /// <param name="flag">0 for email 1 for mobile</param>
        /// <returns></returns>
        public bool IsUserPresent(string input, int flag)
        {
            if (flag == 1)
            {
                var user = _endUserRepository.GetByPhonenumber(input);
                if (user != null)
                {
                    return true;
                }
            }

            else if (flag == 0)
            {
                var user = _endUserRepository.GetByEmail(input);
                if (user != null)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// function to get user Id of specific user
        /// </summary>
        /// <param name="input"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        public Guid GetUserId(string input, int flag)
        {

            if (flag == 1)
            {
                var user = _endUserRepository.GetByPhonenumber(input);

                return user.Id;
            }
            else
            {
                var user = _endUserRepository.GetByEmail(input);

                return user.Id;
            }
        }

        /// <summary>
        /// Function to save student detail after registration
        /// </summary>
        /// <param name="name"></param>
        /// <param name="password"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="alternatePhoneNumber"></param>
        /// <param name="collegeId"></param>
        /// <param name="courseId"></param>
        /// <param name="email"></param>
        /// <param name="rollNo"></param>
        /// <param name="semester"></param>
        /// <param name="id"></param>
        public void SaveStudentData(string name, string password, string phoneNumber, string alternatePhoneNumber, int collegeId, int courseId, string email, string rollNo, int semester, Guid id)
        {

            EndUser user = new EndUser()
            {
                Id = id,
                Name = name,
                Password = password,
                PhoneNo = phoneNumber,
                Email = email,
                RoleId = 1,
            };
            _endUserRepository.AddUser(user);

            Student student = new Student()
            {
                Id = id,
                AlternatePhoneNo = alternatePhoneNumber,
                CollegeId = collegeId,
                CourseId = courseId,
                Semester = semester,
                RollNo = rollNo
            };
            _studentRepository.Add(student);

        }

        /// <summary>
        /// function to get the otp saved for user
        /// </summary>
        /// <param name="id"></param>
        /// <returns>saved otp</returns>
        public OtpResponseModel GetOtp(Guid id, string otp)
        {
            var user = _endUserRepository.GetById(id);
            var savedOtp = user.Otp;
            var now = DateTime.Now;
            var then = user.OtpCreated ?? DateTime.Now;

            var difference = now - then;
            if (difference.TotalSeconds > Constants.Keys.OtpExpire)
            {
                return new OtpResponseModel()
                {
                    IsExpired = true,
                    IsVerified = false
                };
            }
            if (savedOtp == otp)
            {
                return new OtpResponseModel()
                {
                    IsExpired = false,
                    IsVerified = true
                }; ;

            }
            else
            {
                return new OtpResponseModel()
                {
                    IsExpired = false,
                    IsVerified = false
                };
            }
        }

        /// <summary>
        /// function to check whether user has alrady given the test or not based on test id and user id
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="testId"></param>
        /// <returns></returns>
        public bool CheckIsAlreadyGiven(Guid UserId, int testId)
        {

            var userTest = _userTestRepository.Get(UserId, testId, DateTime.Today);

            if (userTest != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Function to save the save user test details provided by user for aptiude Questions
        /// </summary>
        /// <param name="aptitudeTestUserQuestionAnswerModel"></param>
        /// <param name="userId"></param>
        public bool SaveUserAptitudeTestMarks(List<AptitudeUserQuestionAnswerModel> aptitudeTestUserQuestionAnswerModel, Guid userId)
        {

            int marks = 0;
            bool isQualified = false;
            int testId = Convert.ToInt32(TestTypes.AptitudeTestId);

            var correctAnswers = _questionRepository.GetAptiQuestionAnswer();

            foreach (var q in aptitudeTestUserQuestionAnswerModel)
            {
                var answer = correctAnswers.First(a => a.QuestionId == q.QuestionId);

                if (answer.CorrectOptionId == q.OptionId)
                {
                    marks += answer.QuestionMarks;
                }
            }
            bool pass = false;
            var user = _studentRepository.GetDetailsById(userId);
            int cutOff = user.College.Cutoff;
            if (marks >= cutOff)
            {
                pass = true;
                isQualified = true;
            }
            var userTest = user.IdNavigation.UserTest.FirstOrDefault(u => u.DateGiven == DateTime.Today && u.TestId == testId);
            userTest.Marks = marks;
            userTest.Pass = pass;
            _userTestRepository.UpdateWithoutSave(userTest);
            return isQualified;
        }

        public bool SaveAptitudeTestDetails(List<AptitudeUserQuestionAnswerModel> model, Guid userId)
        {
            bool isQualified = SaveUserAptitudeTestMarks(model, userId);
            SaveStudentAptitudeAnswers(model, userId);
            return isQualified;
        }

        /// <summary>
        /// function to save answers of user for aptitude test
        /// </summary>
        /// <param name="aptiQuestionAnswer"></param>
        /// <param name="userId"></param>
        public void SaveStudentAptitudeAnswers(List<AptitudeUserQuestionAnswerModel> aptiQuestionAnswer, Guid userId)
        {
            int testId = Convert.ToInt32(TestTypes.AptitudeTestId);
            var userTest = _userTestRepository.Get(userId, testId, DateTime.Today);
            int userTestId = userTest.Id;
            List<UserQuestionAnswer> userQuestionAnswers = new List<UserQuestionAnswer>();

            for (int i = 0; i < aptiQuestionAnswer.Count; i++)
            {
                userQuestionAnswers.Add(new UserQuestionAnswer()
                {

                    UserTestId = userTestId,
                    QuestionId = aptiQuestionAnswer[i].QuestionId,
                    OptionId = aptiQuestionAnswer[i].OptionId
                });

            }
            _userQuestionAnswerRepository.AddMultiple(userQuestionAnswers);
        }

        /// <summary>
        /// function to check whether the user qualified for next round or not
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>true or false based on score</returns>
        public bool IsQualified(Guid userId)
        {
            int testId = Convert.ToInt32(TestTypes.AptitudeTestId);
            var test = _testRepository.GetById(testId);
            var user = _studentRepository.GetById(userId);
            int collegeId = user.CollegeId;
            var college = _collegeInfoRepository.GetById(collegeId);
            int cutOffMarks = college.Cutoff;
            var userTest = _userTestRepository.Get(userId, testId, DateTime.Today);
            int marksScored = userTest.Marks;

            if (marksScored >= cutOffMarks)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// function to save user Coding Test details
        /// </summary>
        /// <param name="userId"></param>
        public int SaveUserCodingTestDetails(Guid userId)
        {
            int testId = Convert.ToInt32(TestTypes.CodingTestId);

            UserTest userTest = new UserTest()
            {
                UserId = userId,
                TestId = testId,
                Marks = 0,
                DateGiven = DateTime.Today
            };

            _userTestRepository.Add(userTest);
            return userTest.Id;
        }

        /// <summary>
        /// Function to save user code 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="questionId"></param>
        /// <param name="code"></param>
        public void SaveUserCode(Guid userId, int questionId, string code, int userTestId)
        {
            var questionAnswers = _userQuestionAnswerRepository.GetByQuestionId(questionId);
            var questionAnswer = questionAnswers.FirstOrDefault(a => a.UserTestId == userTestId);
            if (questionAnswer != null)
            {
                questionAnswer.TextResponse = code;
                _userQuestionAnswerRepository.Update(questionAnswer);
            }
            else
            {
                UserQuestionAnswer userQuestionAnswer = new UserQuestionAnswer()
                {
                    UserTestId = userTestId,
                    QuestionId = questionId,
                    TextResponse = code,

                };

                _userQuestionAnswerRepository.Add(userQuestionAnswer);
            }
        }

        /// <summary>
        /// function to check whether the user is already present or not art time of registartion
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool IsUserPresent(string phoneNumber, string email)
        {
            RegistrationResponseModel registrationResponseModel = new RegistrationResponseModel();

            //enduserphone for user registered with phone number and enduseremail for user registered with email
            var endUserEmail = _endUserRepository.GetByEmail(email);
            return endUserEmail != null;

        }

        /// <summary>
        /// function to check whether the input provided buy admin are correct or not
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public AdminLoginResponseModel IsAdminPresent(string username, string password)
        {
            bool isPresent = false;
            bool isPasswordCorrect = false;
            var endUser = _endUserRepository.GetByUsername(username);
            if (endUser != null)
            {
                if (endUser.Password == password)
                {
                    isPresent = true;
                    isPasswordCorrect = true;
                }
                else
                {
                    isPresent = true;
                    isPasswordCorrect = false;
                }
            }

            return new AdminLoginResponseModel()
            {
                IsPasswordCorrect = isPasswordCorrect,
                IsPresent = isPresent
            };
        }

        /// <summary>
        /// function to get student info to show on admin section
        /// </summary>
        /// <returns>List of students</returns>
        public List<StudentInfoModel> GetStudentInfo()
        {
            var studentsRepo = _studentRepository.GetInfo();
            var response = _mapper.Map<List<StudentInfoModel>>(studentsRepo);

            return response;

        }

        public StudentPersonalInfoModel GetPersonalDetails(Guid userId)
        {
            var response = _studentRepository.GetPersonalDetails(userId);
            return _mapper.Map<StudentPersonalInfoModel>(response);
        }

        public List<AptitudeAttemptedQuestionResponseModel> AttemptedQuestions(Guid userId, DateTime dateGiven)
        {
            List<AptitudeAttemptedQuestionResponseModel> response = new List<AptitudeAttemptedQuestionResponseModel>();
            var allAptitudeSections = _sectionRepository.GetByTestId((int)TestTypes.AptitudeTestId);
            var activeSections = allAptitudeSections.Where(sec => sec.IsActive == true).ToList();
            int testId = Convert.ToInt32(TestTypes.AptitudeTestId);
            var userTest = _userTestRepository.Get(userId, testId, dateGiven);
            int userTestId = userTest.Id;
            var userQuestionAnswer = _userQuestionAnswerRepository.GetByUserTestId(userTestId);
            var correctAnswers = _questionRepository.GetAptiQuestionAnswer();
            var totalMarks = 0;
            var marks = 0;
            foreach (var sec in activeSections)
            {
                var questions = _questionRepository.GetBySectionId(sec.Id);
                List<AptitudeInfoQuestion> sectionQuestions = new List<AptitudeInfoQuestion>();
                foreach (var que in questions)
                {
                    var question = userQuestionAnswer.FirstOrDefault(q => q.QuestionId == que.Id);
                    if (question != null)
                    {
                        var answer = correctAnswers.FirstOrDefault(a => a.QuestionId == que.Id);

                        if (answer != null)
                        {
                            totalMarks += answer.QuestionMarks;
                            if (question.OptionId == answer.CorrectOptionId)
                            {
                                marks += answer.QuestionMarks;
                            }
                        }

                        var questionOption = _questionOptionRepository.GetByQuestionId(que.Id);
                        List<AptitudeInfoOption> Options = new List<AptitudeInfoOption>();
                        foreach (var op in questionOption)
                        {
                            Options.Add(new AptitudeInfoOption()
                            {
                                OptionId = op.Id,
                                Option = op.QuestionOption1,
                                IsTrue = op.IsCorrect
                            });
                        }
                        sectionQuestions.Add(new AptitudeInfoQuestion()
                        {
                            QuestionContent = que.QuestionContent,
                            QuestionId = question.Id,
                            Options = Options,
                            SectionId = (int)SectionId.OOPS,
                            UserOptionId = question.OptionId
                        });
                    }
                }
                response.Add(new AptitudeAttemptedQuestionResponseModel()
                {
                    Section = sectionQuestions,
                    SectionMarks = marks,
                    SectionTotalMarks = totalMarks,
                    SectionName = sec.SectionName,
                    TrimmedSectionName = String.Concat(sec.SectionName.Where(c => !Char.IsWhiteSpace(c)))
                });
                totalMarks = 0;
                marks = 0;
            }
            return response;
        }

        public async Task RemoveStudent(Guid userId)
        {

            var enduser = _endUserRepository.GetDetailsById(userId);
            await _endUserRepository.DeleteAsync(enduser);

        }
    }
}

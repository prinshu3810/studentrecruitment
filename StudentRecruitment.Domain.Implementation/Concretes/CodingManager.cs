﻿using Recruitment.Domain.Shared.Contracts;
using Recruitment.Domain.Shared.Dto;
using Recruitment.Foundation.Common;
using Recruitment.Repository.Shared.Contracts;
using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recruitment.Domain.Implementation
{
    public class CodingManager : ICodingManager
    {
        private readonly IQuestionRepository _questionRepository;
        private readonly IUserQuestionAnswerRepository _userQuestionAnswerRepository;
        private readonly IUserTestRepository _userTestRepository;
        private readonly ITestRepository _testRepository;
        private readonly ISectionRepository _sectionRepository;
        public CodingManager(IQuestionRepository questionRepository, IUserQuestionAnswerRepository userQuestionAnswerRepository, IUserTestRepository userTestRepository, ITestRepository testRepository, ISectionRepository sectionRepository)
        {
            _questionRepository = questionRepository;
            _userQuestionAnswerRepository = userQuestionAnswerRepository;
            _userTestRepository = userTestRepository;
            _testRepository = testRepository;
            _sectionRepository = sectionRepository;
        }

        public void AddQuestion(CodingQuestionInfo model)
        {
            _questionRepository.Add(new Question()
            {
                QuestionContent = model.QuestionContent,
                Marks = model.QuestionMarks,
                SectionId = model.SectionId,
                IsActive = true
            });
        }

        public void EditQuestion(CodingQuestionInfo model)
        {
            var question = _questionRepository.GetById(model.QuestionId);
            question.IsActive = model.IsActive;
            question.Marks = model.QuestionMarks;
            question.QuestionContent = model.QuestionContent;
            _questionRepository.Update(question);
        }

        public List<QuestionBankCodingResponseModel> GetAllCodingSections()
        {

            List<QuestionBankCodingResponseModel> response = new List<QuestionBankCodingResponseModel>();
            var codingSections = _sectionRepository.GetDetailsByTestId((int)TestTypes.CodingTestId);

            foreach (var sec in codingSections)
            {
                List<CodingQuestionInfo> section = new List<CodingQuestionInfo>();
                var questions = sec.Questions;
                foreach (var q in questions)
                {
                    section.Add(new CodingQuestionInfo()
                    {
                        QuestionContent = q.QuestionContent,
                        QuestionId = q.Id,
                        QuestionMarks = q.Marks,
                        SectionId = q.SectionId,
                        IsActive = q.IsActive
                    });
                }
                response.Add(new QuestionBankCodingResponseModel()
                {
                    SectionName = sec.SectionName,
                    SectionQuestions = section,
                    SectionId = sec.Id,
                    TrimmedSectionName = String.Concat(sec.SectionName.Where(c => !Char.IsWhiteSpace(c)))
                });
            }
            return response;
        }

        /// <summary>
        /// Function for getting the coding question 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="index">optional paramemeter</param>
        /// <returns>model for coding question</returns>
        public CodingQuestion GetCodingQuestion(Guid userId, int index = 1)
        {
            string codeSubmitted = Constants.Keys.StringInitialisation;
            int testId = Convert.ToInt32(TestTypes.CodingTestId);
            var questions = _questionRepository.GetCodingQuestion((int)SectionId.Programming);
            var userTest = _userTestRepository.Get(userId, testId, DateTime.Today);
            int userTestId = userTest.Id;
            var userQuestionnAnswers = _userQuestionAnswerRepository.GetByQuestionId(questions[index - 1].Id);
            var userQuestionnAnswer = userQuestionnAnswers.FirstOrDefault(a => a.UserTestId == userTestId);

            if (userQuestionnAnswer != null)
            {
                codeSubmitted = userQuestionnAnswer.TextResponse;
            }
            else
            {
                codeSubmitted = null;
            }

            return new CodingQuestion()
            {
                QuestionContent = questions[index - 1].QuestionContent,
                QuestionId = questions[index - 1].Id,
                CodeSubmitted = codeSubmitted
            };
        }

        public CodingQuestionInfo GetQuestion(int questionId)
        {
            var question = _questionRepository.GetById(questionId);

            return new CodingQuestionInfo()
            {
                QuestionId = question.Id,
                QuestionContent = question.QuestionContent,
                QuestionMarks = question.Marks,
                IsActive = question.IsActive,
                SectionId = question.SectionId
            };
        }

        /// <summary>
        /// function to get the Code written by user for attempted questions
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="date"></param>
        /// <returns>student coding data</returns>
        public List<StudentcodingDataResponseModel> GetStudentCodingData(Guid userId, DateTime date)
        {
            List<StudentcodingDataResponseModel> studentcodingDataResponseModel = new List<StudentcodingDataResponseModel>();
            int testId = (int)TestTypes.CodingTestId;
            var userTest = _userTestRepository.Get(userId, testId, date);

            if (userTest != null)
            {
                int userTestId = userTest.Id;
                var response = _userQuestionAnswerRepository.GetByUserTestId(userTestId);
                var questionAnswers = response.Select(a => new
                {
                    QuestionId = a.QuestionId,
                    Usercode = a.TextResponse
                }).ToList();

                foreach (var q in questionAnswers)
                {
                    var question = _questionRepository.GetById(q.QuestionId);
                    string questionContent = question.QuestionContent;
                    int questionMarks = question.Marks;
                    studentcodingDataResponseModel.Add(new StudentcodingDataResponseModel()
                    {
                        Question = questionContent,
                        Response = q.Usercode,
                        QuestionMarks = questionMarks,
                        Marks=userTest.Marks,
                        CodingStatus=userTest.Pass
                    });
                }

                return studentcodingDataResponseModel;

            }

            return null;
        }

        /// <summary>
        /// Function to check whether the user has already given the coding test or not
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>true or false based on test given or not</returns>
        public bool TestGiven(Guid userId)
        {
            int testId = (int)TestTypes.CodingTestId;
            var test = _userTestRepository.Get(userId, testId, DateTime.Today);
            if (test != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// function to update the marks in coding test by admin
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="marks"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool UpdateMarks(Guid userId, int marks, DateTime date, int result)
        {
            int testId = (int)TestTypes.CodingTestId;
            var pass = false;

            if (result == 1)
            {
                pass = true;
            }
            var userTest = _userTestRepository.Get(userId, testId, date);
            userTest.Marks = marks;
            userTest.Pass = pass;
            _userTestRepository.Update(userTest);

            return true;

        }
        public void AddSection(string sectionName)
        {
            _sectionRepository.Add(new Section()
            {
                SectionName = sectionName,
                QuestionCount = 0,
                TestId = (int)TestTypes.CodingTestId,
                IsActive = false
            }); ;
        }

        public CodingTestDetailsResponseModel GetTestDuration()
        {
            var test = _testRepository.GetById((int)TestTypes.CodingTestId);
            return new CodingTestDetailsResponseModel()
            {
                TestDuration = test.Duration
            };
        }
    }
}

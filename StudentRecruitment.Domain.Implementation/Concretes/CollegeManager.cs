﻿using Recruitment.Domain.Shared.Contracts;
using Recruitment.Domain.Shared.Dto;
using Recruitment.Repository.Shared.Contracts;
using Recruitment.Repository.Shared.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Recruitment.Domain.Implementation
{
    public class CollegeManager : ICollegeManager
    {
        private readonly ICollegeInfoRepository _collegeInfoRepository;
        public CollegeManager(ICollegeInfoRepository collegeInfoRepository)
        {
            _collegeInfoRepository = collegeInfoRepository;
        }

        public bool AddCollege(string name, int cutoff, bool isActive, string contactPerson, string contactNumber,string address,string state)
        {
            var college = _collegeInfoRepository.GetByName(name);
            if (college == null)
            {
                _collegeInfoRepository.Add(new CollegeInfo
                {
                    CollegeName=name,
                    Cutoff=cutoff,
                    IsActive=isActive,
                    CollegeAddress=address,
                    State=state,
                    ContactPerson=contactPerson,
                    ContactPersonPhoneNo=contactNumber
                });
                return true;
            }
            return false;
        }

        public CollegeInfoResponseModel GetCollege(int id)
        {
            var college = _collegeInfoRepository.GetById(id);
            if (college != null)
            {
                return new CollegeInfoResponseModel()
                {
                    Id = college.Id,
                    Name = college.CollegeName,
                    ContactPerson = college.ContactPerson,
                    Cutoff = college.Cutoff,
                    ContactPersonNumber = college.ContactPersonPhoneNo,
                    IsActive = college.IsActive,
                    Address=college.CollegeAddress,
                    State=college.State
                };
            }
            return null;

        }

        public List<CollegeInfoResponseModel> GetCollegeInfo()
        {
            var courses = _collegeInfoRepository.GetAllColleges(true);
            return courses.Select(col => new CollegeInfoResponseModel()
            {
                Id = col.Id,
                Name = col.CollegeName,
                IsActive = col.IsActive,
                ContactPerson = col.ContactPerson,
                ContactPersonNumber = col.ContactPersonPhoneNo,
                Cutoff = col.Cutoff
            }).ToList();
        }

        /// <summary>
        /// Function to get the list of all colleges
        /// </summary>
        /// <returns>List of all colleges</returns>
        public List<RegisterCollegesResponseModel> GetListedColleges(bool flag)
        {
            var courses = _collegeInfoRepository.GetAllColleges(flag);

            return courses.Select(a => new RegisterCollegesResponseModel()
            {
                Id = a.Id,
                Name = a.CollegeName
            }).ToList();
        }

        public bool UpdateCollege(int id,string name, int cutOff, bool isActve, string contactPerson, string contactNumber,string address,string state)
        {
            string trimmedName = name.Trim();
            var col = _collegeInfoRepository.GetByName(trimmedName);
            var college = _collegeInfoRepository.GetById(id);
            
            if (col!=null)
            {
                if (col.Id != college.Id)
                {
                    return false;
                }
            }
            if (college != null)
            {
                college.CollegeName = name;
                college.Cutoff = cutOff;
                college.ContactPersonPhoneNo = contactNumber;
                college.ContactPerson = contactPerson;
                college.IsActive = isActve;
                college.State = state;
                college.CollegeAddress = address;
            }
            _collegeInfoRepository.Update(college);
            return true;

        }
    }
}

﻿using IRepository.Shared.Contracts;
using Recruitment.Domain.Shared.Contracts;
using Recruitment.Domain.Shared.Dto;
using System.Collections.Generic;
using System.Linq;

namespace Recruitment.Domain.Implementation
{
    public class CourseManager : ICourseManager
    {
        private readonly ICourseRepository _courseRepository;
        public CourseManager(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }

        /// <summary>
        /// function to get all the listed courses 
        /// </summary>
        /// <returns>List of all courses</returns>
        public List<RegisterCoursesResponseModel> GetListedCourses()
        {
            var courses = _courseRepository.GetAllCourses();

            return courses.Select(a => new RegisterCoursesResponseModel()
            {
                Id = a.Id,
                Name = a.CourseType
            }).ToList();
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Recruitment.CodeCompilation.Shared.Contracts;
using Recruitment.CodeCompilation.Shared.Dto;
using Recruitment.Foundation.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.CodeCompilation.Implementataion.Concretes
{
    public class CodeCompiler : ICodeCompiler
    {
        private readonly IConfiguration _configuration;
        public CodeCompiler(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<CodeCompilerApiResponseModel> CompileCode(string code, string language)
        {

            using (var client = new WebClient())
            {
                client.Headers.Add(HttpRequestHeader.ContentType, Constants.ApiRequest.ContentType);
                CompileClientRequestModel u = new CompileClientRequestModel()
                {
                    clientId = _configuration.GetSection(Constants.CompilerSettings.Name).GetSection(Constants.CompilerSettings.ClientId).Value,
                    clientSecret = _configuration.GetSection(Constants.CompilerSettings.Name).GetSection(Constants.CompilerSettings.ClientSecret).Value,
                    script = code,
                    language = language,
                    versionIndex = _configuration.GetSection(Constants.CompilerSettings.Name).GetSection(Constants.CompilerSettings.VersionIndex).Value,
                };

                var stringContent = (JsonConvert.SerializeObject(u));
                try
                {
                    var response = await client.UploadStringTaskAsync(new Uri(Constants.CompilerSettings.Url), Constants.CompilerSettings.Method, stringContent);
                    var result = JsonConvert.DeserializeObject<CodeCompilerResponseModel>(response);

                    return (new CodeCompilerApiResponseModel()
                    {
                        Output = result.output,
                        StatusCode = result.statusCode


                    });

                }
                catch (Exception exp)
                {
                   return null;
                }
            }
        }
    }
}

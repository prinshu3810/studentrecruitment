﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Recruitment.Repository.Shared.Entities
{
    public partial class StudentRecruitmentContext : DbContext
    {
        public StudentRecruitmentContext()
        {
        }

        public StudentRecruitmentContext(DbContextOptions<StudentRecruitmentContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CollegeInfo> CollegeInfo { get; set; }
        public virtual DbSet<Course> Course { get; set; }
        public virtual DbSet<EndUser> EndUser { get; set; }
        public virtual DbSet<Question> Question { get; set; }
        public virtual DbSet<QuestionOption> QuestionOption { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Section> Section { get; set; }
        public virtual DbSet<Student> Student { get; set; }
        public virtual DbSet<Test> Test { get; set; }
        public virtual DbSet<UserQuestionAnswer> UserQuestionAnswer { get; set; }
        public virtual DbSet<UserTest> UserTest { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CollegeInfo>(entity =>
            {
                entity.Property(e => e.CollegeAddress)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.CollegeName)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.ContactPerson)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPersonPhoneNo)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Course>(entity =>
            {
                entity.Property(e => e.CourseType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EndUser>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Otp)
                    .HasColumnName("otp")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.OtpCreated).HasColumnType("datetime");

                entity.Property(e => e.Password).HasMaxLength(100);

                entity.Property(e => e.PhoneNo)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.EndUser)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__EndUser__RoleId__276EDEB3");
            });

            modelBuilder.Entity<Question>(entity =>
            {
                entity.Property(e => e.QuestionContent)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<QuestionOption>(entity =>
            {
                entity.Property(e => e.QuestionOption1)
                    .IsRequired()
                    .HasColumnName("QuestionOption")
                    .IsUnicode(false);

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.QuestionOption)
                    .HasForeignKey(d => d.QuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__QuestionO__Quest__3E52440B");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.RoleType)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Section>(entity =>
            {
                entity.Property(e => e.SectionName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Test)
                    .WithMany(p => p.Section)
                    .HasForeignKey(d => d.TestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Section__TestId__160F4887");
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AlternatePhoneNo)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RollNo)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.College)
                    .WithMany(p => p.Student)
                    .HasForeignKey(d => d.CollegeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Student__College__32E0915F");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.Student)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Student__CourseI__300424B4");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.Student)
                    .HasForeignKey<Student>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EnddUser");
            });

            modelBuilder.Entity<Test>(entity =>
            {
                entity.Property(e => e.TestName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserQuestionAnswer>(entity =>
            {
                entity.Property(e => e.TextResponse).IsUnicode(false);

                entity.HasOne(d => d.Option)
                    .WithMany(p => p.UserQuestionAnswer)
                    .HasForeignKey(d => d.OptionId)
                    .HasConstraintName("FK__UserQuest__Optio__4316F928");

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.UserQuestionAnswer)
                    .HasForeignKey(d => d.QuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__UserQuest__Quest__4222D4EF");

                entity.HasOne(d => d.UserTest)
                    .WithMany(p => p.UserQuestionAnswer)
                    .HasForeignKey(d => d.UserTestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__UserQuest__UserT__412EB0B6");
            });

            modelBuilder.Entity<UserTest>(entity =>
            {
                entity.Property(e => e.DateGiven).HasColumnType("date");

                entity.HasOne(d => d.Test)
                    .WithMany(p => p.UserTest)
                    .HasForeignKey(d => d.TestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__UserTest__TestId__38996AB5");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserTest)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserTests");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Repository.Shared.Dto
{
    public class AptitudeQuestionAnswerRepo
    {
        public int QuestionId { get; set; }
        public int QuestionMarks { get; set; }
        public int CorrectOptionId { get; set; }
    }
}

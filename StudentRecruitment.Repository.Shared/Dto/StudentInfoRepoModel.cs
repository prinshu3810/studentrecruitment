﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Repository.Shared.Dto
{
    public class StudentInfoRepoModel
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Course { get; set; }
        public string College { get; set; }
        public int AptiMarks  { get; set; } 
        public int CodingMarks { get; set; }
        public int? CollegeId { get; set; }
        public int? CourseId { get; set; }
        public string PhoneNumber { get; set; }
        public bool? AptiStatus { get; set; }
        public bool? CodingStatus { get; set; }
        public string Email { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Repository.Shared.Dto
{
    public class AptitudeQuestionRepo
    {
        public int QuestionId { get; set; }
        public string QuestionContent { get; set; }
        public int SectionId { get; set; }
        public List<AptitudeOptionRepo> Options { get; set; } = new List<AptitudeOptionRepo>();
    }
}

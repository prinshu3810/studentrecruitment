﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Repository.Shared.Dto
{
    public class AptitudeOptionRepo
    {
        public int OptionId { get; set; }
        public string Option { get; set; }
    }
}

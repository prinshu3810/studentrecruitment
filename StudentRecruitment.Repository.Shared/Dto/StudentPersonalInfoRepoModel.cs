﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Repository.Shared.Dto
{
    public class StudentPersonalInfoRepoModel
    {
        public string Name { get; set; }
        public string College { get; set; }
        public string Course { get; set; }
        public string PhoneNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }
    }
}

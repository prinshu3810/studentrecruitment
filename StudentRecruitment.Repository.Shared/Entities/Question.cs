﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Recruitment.Repository.Shared.Entities
{
    public partial class Question
    {
        public Question()
        {
            QuestionOption = new HashSet<QuestionOption>();
            UserQuestionAnswer = new HashSet<UserQuestionAnswer>();
        }

        public int Id { get; set; }
        public string QuestionContent { get; set; }
        public int Marks { get; set; }
        public bool IsActive { get; set; }
        public int SectionId { get; set; }

        public virtual ICollection<QuestionOption> QuestionOption { get; set; }
        public virtual ICollection<UserQuestionAnswer> UserQuestionAnswer { get; set; }
    }
}

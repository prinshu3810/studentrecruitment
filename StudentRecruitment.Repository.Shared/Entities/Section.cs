﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Recruitment.Repository.Shared.Entities
{
    public partial class Section
    {
        public int Id { get; set; }
        public string SectionName { get; set; }
        public int TestId { get; set; }
        public int QuestionCount { get; set; }
        public bool IsActive { get; set; }

        public virtual Test Test { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
    }
}

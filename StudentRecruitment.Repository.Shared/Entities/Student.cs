﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Recruitment.Repository.Shared.Entities
{
    public partial class Student
    {
        public string AlternatePhoneNo { get; set; }
        public string RollNo { get; set; }
        public int Semester { get; set; }
        public int CollegeId { get; set; }
        public int CourseId { get; set; }
        public Guid Id { get; set; }

        public virtual CollegeInfo College { get; set; }
        public virtual Course Course { get; set; }
        public virtual EndUser IdNavigation { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Recruitment.Repository.Shared.Entities
{
    public partial class EndUser
    {
        public EndUser()
        {
            UserTest = new HashSet<UserTest>();
        }

        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Otp { get; set; }
        public Guid Id { get; set; }
        public DateTime? OtpCreated { get; set; }

        public virtual Role Role { get; set; }
        public virtual Student Student { get; set; }
        public virtual ICollection<UserTest> UserTest { get; set; }
    }
}

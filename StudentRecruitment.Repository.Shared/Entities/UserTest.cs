﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Recruitment.Repository.Shared.Entities
{
    public partial class UserTest
    {
        public UserTest()
        {
            UserQuestionAnswer = new HashSet<UserQuestionAnswer>();
        }

        public int Id { get; set; }
        public Guid UserId { get; set; }
        public int TestId { get; set; }
        public int Marks { get; set; }
        public DateTime DateGiven { get; set; }
        public bool? Pass { get; set; }

        public virtual Test Test { get; set; }
        public virtual EndUser User { get; set; }
        public virtual ICollection<UserQuestionAnswer> UserQuestionAnswer { get; set; }
    }
}

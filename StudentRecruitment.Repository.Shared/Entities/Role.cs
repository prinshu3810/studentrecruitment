﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Recruitment.Repository.Shared.Entities
{
    public partial class Role
    {
        public Role()
        {
            EndUser = new HashSet<EndUser>();
        }

        public int Id { get; set; }
        public string RoleType { get; set; }

        public virtual ICollection<EndUser> EndUser { get; set; }
    }
}

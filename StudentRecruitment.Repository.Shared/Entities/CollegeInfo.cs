﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Recruitment.Repository.Shared.Entities
{
    public partial class CollegeInfo
    {
        public CollegeInfo()
        {
            Student = new HashSet<Student>();
        }

        public int Id { get; set; }
        public string CollegeName { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonPhoneNo { get; set; }
        public string CollegeAddress { get; set; }
        public string State { get; set; }
        public bool IsActive { get; set; }
        public int Cutoff { get; set; }

        public virtual ICollection<Student> Student { get; set; }
    }
}

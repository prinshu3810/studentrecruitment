﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Recruitment.Repository.Shared.Entities
{
    public partial class UserQuestionAnswer
    {
        public int Id { get; set; }
        public int UserTestId { get; set; }
        public int QuestionId { get; set; }
        public int? OptionId { get; set; }
        public string TextResponse { get; set; }

        public virtual QuestionOption Option { get; set; }
        public virtual Question Question { get; set; }
        public virtual UserTest UserTest { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Recruitment.Repository.Shared.Entities
{
    public partial class QuestionOption
    {
        public QuestionOption()
        {
            UserQuestionAnswer = new HashSet<UserQuestionAnswer>();
        }

        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string QuestionOption1 { get; set; }
        public bool IsActive { get; set; }
        public bool IsCorrect { get; set; }

        public virtual Question Question { get; set; }
        public virtual ICollection<UserQuestionAnswer> UserQuestionAnswer { get; set; }
    }
}

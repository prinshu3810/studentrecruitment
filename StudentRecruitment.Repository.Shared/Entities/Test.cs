﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Recruitment.Repository.Shared.Entities
{
    public partial class Test
    {
        public Test()
        {
            Section = new HashSet<Section>();
            UserTest = new HashSet<UserTest>();
        }

        public int Id { get; set; }
        public int Duration { get; set; }
        public string TestName { get; set; }
        public int MaxMarks { get; set; }

        public virtual ICollection<Section> Section { get; set; }
        public virtual ICollection<UserTest> UserTest { get; set; }
    }
}

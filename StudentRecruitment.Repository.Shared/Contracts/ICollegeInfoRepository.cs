﻿using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Repository.Shared.Contracts
{
    public interface ICollegeInfoRepository
    {
        List<CollegeInfo> GetAllColleges(bool flag);
        CollegeInfo GetById(int id);
        void Update(CollegeInfo college);
        CollegeInfo GetByName(string name);
        void Add(CollegeInfo college);

    }
}

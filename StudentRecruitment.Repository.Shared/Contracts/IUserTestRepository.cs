﻿using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Repository.Shared.Contracts
{
    public interface IUserTestRepository
    {
        UserTest Get(Guid id, int testId, DateTime date);
        void Add(UserTest userTest);
        void Update(UserTest userTest);
        void UpdateWithoutSave(UserTest userTest);
        List<UserTest> GetById(Guid userId);
    }
}

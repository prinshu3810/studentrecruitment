﻿using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Repository.Shared.Contracts
{
    public interface ISectionRepository
    {
        Section GetById(int Id);
        List<Section> GetByTestId(int testId);
        void Add(Section section);
        void Update(Section section);
        public List<Section> GetDetailsByTestId(int testId);
    }
}

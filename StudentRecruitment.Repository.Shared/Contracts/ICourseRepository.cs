﻿using Recruitment.Repository.Shared.Entities;
using System.Collections.Generic;

namespace IRepository.Shared.Contracts
{
    public interface ICourseRepository
    {
        List<Course> GetAllCourses();
    }
}

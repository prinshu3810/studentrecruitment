﻿using Recruitment.Repository.Shared.Dto;
using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Repository.Shared.Contracts
{
    public interface IStudentRepository
    {
        void Add(Student student);
        List<StudentInfoRepoModel> GetInfo();
        StudentPersonalInfoRepoModel GetPersonalDetails(Guid userId);
        Student GetById(Guid userId);
        Student GetDetailsById(Guid userId);
    }
}

﻿using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Repository.Shared.Contracts
{

    public interface IEndUserRepository
    {
        EndUser GetByPhonenumber(string phoneNumber);
        void Update(EndUser user);
        void AddUser(EndUser user);
        EndUser GetById(Guid id);
        EndUser GetDetailsById(Guid id);
        EndUser GetByEmail(string email);
        EndUser GetByUsername(string username);
        Task DeleteAsync(EndUser enduser);

    }
}

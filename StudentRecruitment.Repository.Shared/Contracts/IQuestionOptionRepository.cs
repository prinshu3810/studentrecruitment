﻿using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Repository.Shared.Contracts
{
    public interface IQuestionOptionRepository
    {
        List<QuestionOption> GetByQuestionId(int Id);
        void Add(QuestionOption option);
        void Update(QuestionOption option);
    }
}


﻿using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Repository.Shared.Contracts
{
    public interface ITestRepository
    {
        Test GetById(int id);
      
    }
}

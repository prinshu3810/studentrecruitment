﻿using Recruitment.Repository.Shared.Dto;
using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Repository.Shared.Contracts
{
    public interface IQuestionRepository
    {
        List<AptitudeQuestionAnswerRepo> GetAptiQuestionAnswer();
        List<Question> GetCodingQuestion(int sectionId);
        List<AptitudeQuestionRepo> GetAptitudeQuestion();
        Question GetById(int id);
        List<Question> GetBySectionId(int sectionId);
        void Add(Question question);
        Question GetByQuestionContent(string questionContent);
        void Update(Question question);
    }
}

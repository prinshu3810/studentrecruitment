﻿using Recruitment.Repository.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Recruitment.Repository.Shared.Contracts
{
    public interface IUserQuestionAnswerRepository
    {
        void Add(UserQuestionAnswer userQuestionAnswer);
        void AddMultiple(List<UserQuestionAnswer> userQuestionAnswer);
        List<UserQuestionAnswer> GetByQuestionId(int id);
        void Update(UserQuestionAnswer userQuestionAnswer);
        List<UserQuestionAnswer> GetByUserTestId(int userTestId);
    }
}
